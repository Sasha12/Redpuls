//
//  Marker.m
//  RedPulsePro
//
//  Created by MacMini on 12/16/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "Marker.h"

@implementation Marker
- (void)marker:(GMSMarker*)mark setPosition : (CGPoint) posValue andEnd :(CGPoint)andValue;
{
    NSLog(@"set position");
    
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
        
        animation.fromValue = [NSValue valueWithCGPoint:posValue];
        animation.toValue = [NSValue valueWithCGPoint:andValue];
        animation.duration = 1.0;
        //animation.delegate = self;
        animation.fillMode = kCAFillModeForwards;
        //[self.layer removeAllAnimations];
        [self.layer addAnimation:animation forKey:@"position"];
        
        //NSLog(@"setPosition ANIMATED %x from (%f, %f) to (%f, %f)", self, self.center.x, self.center.y, toPos.x, toPos.y);
    }
    
   // self.center = toPos;
    
   // previousPoint = mapPoint;
//}
@end
