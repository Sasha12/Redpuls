//
//  CustomerHistoryList.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/12/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "CustomerHistoryList.h"

@implementation CustomerHistoryList

- (id)initWithRestAPIResponse:(NSDictionary*) response andKey:(NSString*)key imageData:(NSData*)imageData
{
    self = [super init];
    if (self) {
        self.imageData = imageData;
        self.OrderId = [response valueForKey:@"id"];
        self.RepairerId = [response  valueForKey:@"repairer_id"];
        self.Status = [response valueForKey:@"status"];
        self.CreatedDate = [response valueForKey:@"created_date"];
        self.UpdatedDate = [response valueForKey:@"updated_date"];
        self.AcceptedDate = [response valueForKey:@"accepted_date"];
        self.RepairerName = [response valueForKey:@"repairer_name"];
        self.RepairerSurname = [response valueForKey:@"repairer_surname"];
        self.RepairerPhone= [response valueForKey:@"repairer_phone"];
        self.RepairerLatitute = [[response valueForKey:@"repairer_lat"] doubleValue];
        self.RepairerLongitute = [[response valueForKey:@"repairer_lng"] doubleValue];
        self.RepairerImage = [response valueForKey:@"repairer_image"];
        
        
        self.ServicePrice = [NSString stringWithFormat:@"%@",[[[response valueForKey:@"product_details"] objectAtIndex:0] valueForKey:@"service_price"]];
        self.ProductName = [[[response valueForKey:@"product_details"] objectAtIndex:0] valueForKey:@"product_name"];
        self.ServiceName = [[[response valueForKey:@"product_details"] objectAtIndex:0] valueForKey:@"service_name"];
        //self.ItemImagePath = [[[response valueForKey:@"product_details"] objectAtIndex:0] valueForKey:@"avatar"];
        self.primKey = key;
       
    }
    return self;
}

+ (NSString *)primaryKey {
    return @"primKey";
}

- (void) createOrUpdateCustomerAccountInStorage {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [CustomerHistoryList createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}



@end
/*
"message":{
 
 {"id":"619",
 "repairer_id":null,
 "status":"0",
 "created_date":"2016-10-30 08:41:59",
 "updated_date":"2016-10-30 08:41:59",
 "accepted_date":null,"problem":null,
 "payment_status":null,
 "repairer_name":"not value",
 "repairer_surname":"not value",
 "repairer_phone":"not value",
 "repairer_lat":"not value",
 "repairer_lng":"not value",
 "repairer_image":"http:\/\/admin-red.expandis.fr\/img\/avatars\/placeholder.png",
 "product_details":
                [
                    {"service_name":"Batterie HS",
                    "service_price":30,
                     "product_name":"Lumia 600",
                       "brand_name":"nokia"},
 
                    {"service_name":"D\u00e9soxydation",
                    "service_price":25,
                     "product_name":"Lumia 600",
                       "brand_name":"nokia"}
                ]
 }
 
 
 
 
 
 "0":{
 
 "id":"11",
 "repairer_id":"2",
 "status":"1",
 "created_date":"2016-09-08 21:29:28",
 "updated_date":"2016-09-08 21:29:28",
 "accepted_date":null,
 "problem":null,
 "repairer_name":"Sasha",
 "order_description":"Remplacement de l'ecran"
 },
 
 
 "1":{"id":"12","repairer_id":"2","status":"1","created_date":"2016-09-08 21:31:19","updated_date":"2016-09-08 21:31:19","accepted_date":null,"problem":null,"repairer_name":"Sasha","order_description":"Remplacement de l'ecran"},"2":{"id":"13","repairer_id":"2","status":"1","created_date":"2016-09-08 21:38:51","updated_date":"2016-09-08 21:38:51","accepted_date":null,"problem":null,"repairer_name":"Sasha","order_description":"Remplacement de l'ecran"},"3":{"id":"14","repairer_id":"2","status":"1","created_date":"2016-09-08 21:44:26","updated_date":"2016-09-08 21:44:26","accepted_date":null,"problem":null,"repairer_name":"Sasha","order_description":"Remplacement de l'ecran"},"4":{"id":"15","repairer_id":"2","status":"1","created_date":"2016-09-08 22:26:06","updated_date":"2016-09-08 22:26:06","accepted_date":null,"problem":null,"repairer_name":"Sasha","order_description":"Remplacement de l'ecran"},"5":{"id":"16","repairer_id":"2","status":"1","created_date":"2016-09-08 22:56:05","updated_date":"2016-09-08 22:56:05","accepted_date":null,"problem":null,"repairer_name":"Sasha","order_description":"Remplacement de l'ecran"},"6":{"id":"17","repairer_id":"2","status":"1","created_date":"2016-09-08 23:06:15","updated_date":"2016-09-08 23:06:15","accepted_date":null,"problem":null,"repairer_name":"Sasha","order_description":"Remplacement de l'ecran"},"7":{"id":"18","repairer_id":"2","status":"1","created_date":"2016-09-08 23:34:57","updated_date":"2016-09-08 23:34:57","accepted_date":null,"problem":null,"repairer_name":"Sasha","order_description":"Remplacement de l'ecran"},"8":{"id":"19","repairer_id":"2","status":"1","created_date":"2016-09-08 23:36:46","updated_date":"2016-09-08 23:36:46","accepted_date":null,"problem":null,"repairer_name":"Sasha","order_description":"Remplacement de l'ecran"},"9":{"id":"20","repairer_id":"2","status":"1","created_date":"2016-09-08 23:40:50","updated_date":"2016-09-08 23:40:50","accepted_date":null,"problem":null,"repairer_name":"Sasha","order_description":"Remplacement de l'ecran"},"10":{"id":"21","repairer_id":"2","status":"1","created_date":"2016-09-08 23:42:31","updated_date":"2016-09-08 23:42:31","accepted_date":null,"problem":null,"repairer_name":"Sasha","order_description":"Remplacement de l'ecran"},"11":{"id":"22","repairer_id":"2","status":"1","created_date":"2016-09-09 01:10:43","updated_date":"2016-09-09 01:10:43","accepted_date":null,"problem":null,"repairer_name":"Sasha","order_description":"Remplacement de l'ecran"},"avatar":"http:\/\/admin-red.expandis.fr\/img\/avatars\/avatar.png"}
*/
