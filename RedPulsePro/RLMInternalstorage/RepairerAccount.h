//
//  RepairerAccount.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/6/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import <Realm/Realm.h>

@interface RepairerAccount : RLMObject

//User Details
@property NSString* ApiKey;
@property NSString* Name;
@property NSString* Surname;
@property NSString* Email;
@property NSString* PhoneNumber;
@property NSInteger Role;
@property NSString* UserName;
@property NSString* Status; //Can recive order
@property NSString* UserId;
@property NSString* LaastIp;
@property NSString* UserCreatedDate;
@property NSString* UserUpdateDate;
@property NSString* Avatar;

//Defoult Address
@property NSString* City;
@property NSString* Country;
@property NSString* StreetAddress;
@property NSString* State;
@property NSString* UserLongitute;
@property NSString* UserLatitute;
@property NSString* Zip;
@property NSString* Percent;
@property NSString* WorkStatus;

@property BOOL isUserLogined;


- (id)initWithRestAPIResponse:(NSDictionary*) response isUserLogined:(BOOL)isLogined;
- (NSString*)getApiKey;
- (void) createOrUpdateCustomerAccountInStorage;
@end
RLM_ARRAY_TYPE(RepairerAccount)

