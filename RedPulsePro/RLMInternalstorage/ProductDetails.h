//
//  ProductDetails.h
//  RedPulsePro
//
//  Created by MacMini on 10/23/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <Realm/Realm.h>

@interface ProductDetails : RLMObject

//Service Details
@property NSString* Avatar;
@property NSString* Name;
@property NSString* Description;
@property NSInteger Type;
@property NSString* Id;
@property NSString* primKey;

- (id)initWithRestAPIResponse:(NSDictionary*) response  andKey:(NSInteger)key;

- (void) createOrUpdateCustomerAccountInStorage;

@end

RLM_ARRAY_TYPE(ProductDetails)
