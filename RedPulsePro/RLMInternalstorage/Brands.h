//
//  Brands.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/4/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import <Realm/Realm.h>

@interface Brands : RLMObject

//Brand Details
@property NSString* BrandId;
@property NSString* Name;
@property NSString* Logo;
@property NSString* Logo_Red;

- (void) createOrUpdateCustomerAccountInStorage;
- (id)initWithBrandId:(NSString *) brandId andBrandLogo:(NSString*)brandLogo andBrandName:(NSString*) brandName andRedLogo:(NSString*)brandRedLogo;

@end

RLM_ARRAY_TYPE(Brands)
