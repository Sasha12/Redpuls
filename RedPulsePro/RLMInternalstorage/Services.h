//
//  Services.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/3/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import <Realm/Realm.h>
#import "ServiceItems.h"

@interface Services : RLMObject

//Service Details
@property NSString* primKey;
@property NSString* Name;
@property RLMArray<ServiceItems*><ServiceItems>* ServiceItemsDetails;

- (id)initWithRestAPIResponse:(RLMArray*)array name:(NSString*)name;

- (void) createOrUpdateCustomerAccountInStorage;

@end

RLM_ARRAY_TYPE(Services)

