//
//  Devices.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/28/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import <Realm/Realm.h>

@interface Devices : RLMObject

@property NSString* DeviceId;
@property NSString* DeviceName;

- (id)initWithRestAPIResponse:(NSDictionary*) response;
- (void) createOrUpdateCustomerAccountInStorage;

@end
RLM_ARRAY_TYPE(Devices)
