//
//  Services.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/3/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "Services.h"

@implementation Services


- (id)initWithRestAPIResponse:(RLMArray*)array name:(NSString*)name {
    self = [super init];
    if (self) {
       
        self.ServiceItemsDetails = (id)array;
        self.Name = name;
        
    }
    return self;
}

- (void) createOrUpdateCustomerAccountInStorage {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [Services createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}

+ (NSString *)primaryKey {
    return @"Name";
}

@end
