//
//  RepairerHistoryList.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/6/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ProductDetails.h"

@class ProductDetails;

@interface RepairerHistoryList : RLMObject

//Order Details
@property NSString* OrderId;
@property NSString* RepairerId;
@property NSString* Status;
@property NSString* CreatedDate;
@property NSString* UpdatedDate;
@property NSString* AcceptedDate;
@property NSString* CustomerName;
@property NSString* CustomerSurname;
@property NSString* CustomerPhone;
@property double   CustomerLatitute;
@property double   CustomerLongitute;

@property NSString* ProductId;
@property NSString* ProductName;
@property NSString* OrderDescription;
@property NSString* ItemImagePath;

@property RLMArray<ProductDetails*><ProductDetails>* ProductDetails;

//@property NSArray* ProductDetails;

- (id)initWithRestAPIResponse:(NSDictionary*) response with:(RLMArray*)array;
- (void) createOrUpdateCustomerAccountInStorage;
@end
RLM_ARRAY_TYPE(RepairerHistoryList)



/*
"{\"success\":true,
 
 
 \"message\":
 
 {\"0\":
 
 {\"id\":\"22\",
 \"repairer_id\":\"24\",
 \"status\":\"0\",
 \"created_date\":\"2016-09-03 12:36:42\",
 \"updated_date\":\"2016-09-03 12:36:42\",
 \"accepted_date\":null,
 \"customer_name\":\"Narek Amirkhanyan\",
 \"customer_surname\":null,
 \"customer_phone\":\"94949494\",
 \"lat\":40.7866905,
 \"lng\":43.8379932,
 
 \"product_details\":
 
 {\"id\":\"2\",
 \"name\":\"galaxy s3\",
 \"description\":\"long description\",
 \"short_description\":\"short description\",
 \"price\":\"80\",
 \"avatar\":\"http:\\/\\/admin-red.expandis.fr\\/uploads\\/147221717557c04057ef6439.818239650.jpg\",
 \"type\":1}},
 
 \"1\":{\"id\":\"24\",\"repairer_id\":\"24\",\"status\":\"0\",\"created_date\":\"2016-09-03 22:54:48\",\"updated_date\":\"2016-09-03 22:54:48\",\"accepted_date\":null,\"customer_name\":\"fhjtgyh\",\"customer_surname\":\"ghfg\",\"customer_phone\":\"+33 4 64 67 56 75\",\"lat\":40.78902053833,\"lng\":43.820014190674,\"product_details\":{\"id\":\"2\",\"name\":\"screen\",\"description\":\"tuch screen\",\"price\":\"60\",\"in_stock\":\"10\",\"avatar\":\"http:\\/\\/admin-red.expandis.fr\\/uploads\\/147221717557c04057968882.259479930.jpg\",\"type\":0}},\"avatar\":\"http:\\/\\/admin-red.expandis.fr\\/img\\/avatars\\/avatar.png\"}}"
*/
