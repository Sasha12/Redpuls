//
//  CardDetails.m
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "CardDetails.h"

@implementation CardDetails

- (id)initWithRestAPIResponse:(NSDictionary*) response imageData:(NSData*)imageData{
    self = [super init];
    if (self) {
       
        self.cardLogoData           = imageData;
        self.Id                     = [response valueForKey:@"id"];
        self.Card_logo              = [response valueForKey:@"card_logo"];
        self.Default_card           = [response valueForKey:@"default_card"];
        self.Exp_date               = [response valueForKey:@"exp_date"];
        self.Brand                  = [response valueForKey:@"brand"];
        self.Last4                  = [response valueForKey:@"last4"];
        
    }
    return self;
}

- (void) createOrUpdateCustomerAccountInStorage {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [CardDetails createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}

+ (NSString *)primaryKey {
    return @"Id";
}



@end
//{"id":"9","last4":"1881","brand":"Visa","exp_date":"11\/2023","default_card":"1","card_logo":"http:\/\/red.expandis.fr\/img\/visa.png"}
