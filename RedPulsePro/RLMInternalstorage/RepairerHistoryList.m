//
//  RepairerHistoryList.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/6/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "RepairerHistoryList.h"

@implementation RepairerHistoryList

- (id)initWithRestAPIResponse:(NSDictionary*) response with:(RLMArray*)array
{
    self = [super init];
    if (self) {
        self.OrderId = [response valueForKey:@"id"];
        self.RepairerId = [response  valueForKey:@"repairer_id"];
        self.Status = [response valueForKey:@"status"];
        self.CreatedDate = [response valueForKey:@"created_date"];
        self.UpdatedDate = [response valueForKey:@"updated_date"];
        self.AcceptedDate = [response valueForKey:@"accepted_date"];
        self.CustomerName = [response valueForKey:@"customer_name"];
        self.CustomerSurname = [response valueForKey:@"customer_surname"];
        self.CustomerPhone= [response valueForKey:@"customer_phone"];
        self.CustomerLatitute = [[response valueForKey:@"lat"] doubleValue];
        self.CustomerLongitute = [[response valueForKey:@"lng"] doubleValue];
        
       // self.ProductDetails = [[response valueForKey:@"product_details"] valueForKey:@"product_details"];
        
//        self.ProductId = [[response valueForKey:@"product_details"] valueForKey:@"id"];
//        self.ProductName = [[response valueForKey:@"product_details"] valueForKey:@"name"];
//        self.OrderDescription = [[response valueForKey:@"product_details"] valueForKey:@"description"];
//        self.ItemImagePath = [[response valueForKey:@"product_details"] valueForKey:@"avatar"];
        self.ProductDetails = (id)array;
        
    }
    return self;
}

+ (NSString *)primaryKey {
    return @"OrderId";
}


- (void) createOrUpdateCustomerAccountInStorage {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [RepairerHistoryList createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}



@end
