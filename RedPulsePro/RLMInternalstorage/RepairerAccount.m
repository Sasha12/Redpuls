//
//  RepairerAccount.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/6/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "RepairerAccount.h"

@implementation RepairerAccount


- (id)initWithRestAPIResponse:(NSDictionary*) response isUserLogined:(BOOL)isLogined
{
    self = [super init];
    if (self) {
        self.Role = [[response valueForKey:@"role"] integerValue];
        self.ApiKey = [response valueForKey:@"api_key"];
        self.UserName = [response valueForKey:@"username"];
        self.Name = [[response valueForKey:@"data"] valueForKey:@"name"];
        self.Surname = [[response valueForKey:@"data"] valueForKey:@"surname"];
        self.Email = [[response valueForKey:@"data"] valueForKey:@"email"];
        self.PhoneNumber = [[response valueForKey:@"data"] valueForKey:@"phone"];
        self.UserId = [[response valueForKey:@"data"] valueForKey:@"id"];
        self.LaastIp = [[response valueForKey:@"data"] valueForKey:@"last_ip"];
        self.UserCreatedDate = [[response valueForKey:@"data"] valueForKey:@"created_date"];
        self.UserUpdateDate= [[response valueForKey:@"data"] valueForKey:@"updated_date"];
        self.Avatar = [[response valueForKey:@"data"] valueForKey:@"avatar"];
        self.City = [[response valueForKey:@"data"] valueForKey:@"city"];
        self.Country = [[response valueForKey:@"data"] valueForKey:@"country"];
        self.StreetAddress = [[response valueForKey:@"data"] valueForKey:@"address"];
        self.State = [[response valueForKey:@"data"] valueForKey:@"state"];
        self.UserLongitute = [[response valueForKey:@"data"] valueForKey:@"long"];
        self.UserLatitute = [[response valueForKey:@"data"] valueForKey:@"lat"];
        self.Zip = [[response valueForKey:@"data"] valueForKey:@"zip"];
        self.Percent = [[response valueForKey:@"data"] valueForKey:@"percent"];
         self.WorkStatus = [[response valueForKey:@"data"] valueForKey:@"work_status"];
        
        self.isUserLogined = isLogined;
        self.Status = [[response valueForKey:@"data"] valueForKey:@"status"];
        
    }
    return self;
}


+ (NSString *)primaryKey {
    return @"UserId";
}

//+(void) isUserLogined:(BOOL) mIsLogined {
//    isLogined = mIsLogined;
//}
- (NSString*)getApiKey {
    return self.ApiKey;
}
- (void) createOrUpdateCustomerAccountInStorage {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [RepairerAccount createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}


@end
/*
 
 {"username":"Sasha",
 "role":10,
 "api_key":"CAUNkJOfR5yq0.24793900\/1473116208",+
 
 "data":{"id":"2",
 "name":"Sasha",+
 "surname":"Stepanyan",+
 "email":"sash.stepanyan@bk.ru",+
 "phone":"+33 5 64 56 12 32",+
 "user_id":"4",+
 "country":"Armenia",+
 "city":"Gyumri",+
 "state":"Shirak",+
 "address":"Gay str",+
 "zip":"37456",+
 "long":"43.84649658203125",+
 "lat":"40.792903900146484",+
 "percent":"10",+
 "status":"1",+
 "created_date":"2016-08-24 13:26:00",+
 "updated_date":"2016-08-24 13:26:00",+
 "work_status":"0",+
 "avatar":"http:\/\/admin-red.expandis.fr\/img\/avatars\/avatar.png"},+
 "default_address":null}
 
 */