//
//  CustomerAccount.m
//
//
//  Created by Valodya Galstyan on 9/2/16.
//  Copyright © 2016 Valodya Galstyan. All rights reserved.
//

#import "CustomerAccount.h"

@implementation CustomerAccount

-(id)initWithOldAccount:(CustomerAccount*)account andIsLoginid:(BOOL) isLogined {
    self = [super init];
    if (self) {
 
        self.isUserLogined = isLogined;
       
        
    }
    return self;
}

- (id)initWithRestAPIResponse:(NSDictionary*) response isUserLogined:(BOOL)isLogined
{
    self = [super init];
    if (self) {
        self.Role = [[response valueForKey:@"role"] integerValue];
        self.ApiKey = [response valueForKey:@"api_key"];
        self.UserName = [response valueForKey:@"username"];
        self.Name = [[response valueForKey:@"data"] valueForKey:@"name"];
        self.Surname = [[response valueForKey:@"data"] valueForKey:@"surname"];
        self.Email = [[response valueForKey:@"data"] valueForKey:@"email"];
        self.PhoneNumber = [[response valueForKey:@"data"] valueForKey:@"phone"];
        self.UserId = [[response valueForKey:@"data"] valueForKey:@"id"];
        self.LaastIp = [[response valueForKey:@"data"] valueForKey:@"last_ip"];
        self.UserCreatedDate = [[response valueForKey:@"data"] valueForKey:@"created_date"];
        self.UserUpdateDate= [[response valueForKey:@"data"] valueForKey:@"updated_date"];
        self.Avatar = [[response valueForKey:@"data"] valueForKey:@"avatar"];
        self.City = [[response valueForKey:@"default_address"] valueForKey:@"city"];
        self.Country = [[response valueForKey:@"default_address"] valueForKey:@"country"];
        self.StreetAddress = [[response valueForKey:@"default_address"] valueForKey:@"address"];
        self.State = [[response valueForKey:@"default_address"] valueForKey:@"state"];
        self.UserLongitute = [[response valueForKey:@"default_address"] valueForKey:@"long"];
        self.UserLatitute = [[response valueForKey:@"default_address"] valueForKey:@"lat"];
        self.isUserLogined = isLogined;
        self.Status = [[response valueForKey:@"data"] valueForKey:@"status"];
        
    }
    return self;
}


+ (NSString *)primaryKey {
    return @"UserId";
}

//+(void) isUserLogined:(BOOL) mIsLogined {
//    isLogined = mIsLogined;
//}
- (NSString*)getApiKey {
    return self.ApiKey;
}
- (void) createOrUpdateCustomerAccountInStorage {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [CustomerAccount createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}
@end


/*
 @property NSString* ApiKey;
 @property NSString* Name;
 @property NSString* Surname;
 @property NSString* Email;
 @property NSString* PhoneNumber;
 @property NSString* Role;
 @property NSString* UserName;
 @property NSString* Status;
 @property NSString* UserId;
 @property NSString* LaastIp;
 @property NSString* UserCreatedDate;
 @property NSString* UserUpdateDate;
 @property NSString* Avatar;
 
 //Defoult Address
 @property NSString* City;
 @property NSString* Country;
 @property NSString* StreetAddress;
 @property NSString* State;
 @property NSString* UserLongitute;
 @property NSString* UserLatitute;

*/