//
//  CustomerHistoryList.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/12/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import <Realm/Realm.h>

@interface CustomerHistoryList : RLMObject

//Order Details
@property NSString* OrderId;
@property NSString* RepairerId;
@property NSString* Status;
@property NSString* CreatedDate;
@property NSString* UpdatedDate;
@property NSString* AcceptedDate;
@property NSString* RepairerName;
@property NSString* RepairerSurname;
@property NSString* RepairerPhone;
@property double   RepairerLatitute;
@property double   RepairerLongitute;
@property NSString* RepairerImage;
@property NSData* imageData;

@property NSString* primKey;
@property NSString* ServicePrice;
@property NSString* ProductName;
@property NSString* ServiceName;
@property NSString* ItemImagePath;

- (id)initWithRestAPIResponse:(NSDictionary*) response andKey:(NSString*)key imageData:(NSData*)imageData;
- (void) createOrUpdateCustomerAccountInStorage;


@end
RLM_ARRAY_TYPE(CustomerHistoryList)




/*
 "message":{
 
 "0":{
 
 "id":"11",
 "repairer_id":"2",
 "status":"1",
 "created_date":"2016-09-08 21:29:28",
 "updated_date":"2016-09-08 21:29:28",
 "accepted_date":null,
 "problem":null,
 "repairer_name":"Sasha",
 "order_description":"Remplacement de l'ecran"
 },
*/
