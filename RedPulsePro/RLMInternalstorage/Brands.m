//
//  Brands.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/4/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "Brands.h"

@implementation Brands


- (id)initWithBrandId:(NSString *) brandId andBrandLogo:(NSString*)brandLogo andBrandName:(NSString*) brandName andRedLogo:(NSString*)brandRedLogo {
    self = [super init];
    if (self) {
        self.BrandId          = brandId;
        self.Logo             = brandLogo;
        self.Name             = brandName;
        self.Logo_Red         = brandRedLogo;
    }
    return self;
}

- (void) createOrUpdateCustomerAccountInStorage {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [Brands createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}


+ (NSString *)primaryKey {
    return @"BrandId";
}

@end

