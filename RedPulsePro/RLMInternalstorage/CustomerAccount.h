//
//  UserAccount.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/2/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import <Realm/Realm.h>

@interface CustomerAccount : RLMObject

//User Details
@property NSString* ApiKey;
@property NSString* Name;
@property NSString* Surname;
@property NSString* Email;
@property NSString* PhoneNumber;
@property NSInteger Role;
@property NSString* UserName;
@property NSString* Status;
@property NSString* UserId;
@property NSString* LaastIp;
@property NSString* UserCreatedDate;
@property NSString* UserUpdateDate;
@property NSString* Avatar;

//Defoult Address
@property NSString* City;
@property NSString* Country;
@property NSString* StreetAddress;
@property NSString* State;
@property NSString* UserLongitute;
@property NSString* UserLatitute;


@property BOOL isUserLogined;

-(id)initWithOldAccount:(CustomerAccount*)account andIsLoginid:(BOOL) isLogined;
- (id)initWithRestAPIResponse:(NSDictionary*) response isUserLogined:(BOOL) isLogined;

- (void)createOrUpdateCustomerAccountInStorage ;
- (NSString*)getApiKey;
//-(void) isUserLogined:(BOOL) isLogined;
@end

RLM_ARRAY_TYPE(CustomerAccount)


