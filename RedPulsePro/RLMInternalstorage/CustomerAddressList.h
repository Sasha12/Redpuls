//
//  CustomerAddressList.h
//  RedPulsePro
//
//  Created by MacMini on 12/9/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <Realm/Realm.h>

@interface CustomerAddressList : RLMObject

//Service Details
@property NSString* AddressId;
@property NSString* City;
@property NSString* Country;
@property NSString* Address;
@property NSString* State;
@property NSString* Customer_id;
@property NSString* Long;
@property NSString* Lat;
@property NSString* Default_address;
@property NSString* Zip;


- (id)initWithRestAPIResponse:(NSDictionary*) response;

- (void) createOrUpdateCustomerAccountInStorage;

@end

RLM_ARRAY_TYPE(CustomerAddressList)
/*
response: "{\"success\":true,\"message\":
[{\"id\":\"165\",
 \"city\":\"Jivani Street\",
 \"country\":\"Shirak Province\",
 \"address\":\"74\",\
 "state\":\"Gyumri\",\
 "customer_id\":\"47\",
 \"long\":\"43.8379932\",
 \"lat\":\"40.7866905\",
 \"default_address\":null,
 \"zip\":\"0012\"},
 
 {\"id\":\"166\",\"city\":\"Malatia-Sebastia\",\"country\":\"Yerevan\",\"address\":\"Jivani Street\",\"state\":\"Yerevan\",\"customer_id\":\"47\",\"long\":\"44.4514265\",\"lat\":\"40.1706532\",\"default_address\":\"0\",\"zip\":null},.....
*/
