//
//  CustomerAddressList.m
//  RedPulsePro
//
//  Created by MacMini on 12/9/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "CustomerAddressList.h"

@implementation CustomerAddressList
- (id)initWithRestAPIResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        
        self.AddressId          = [response valueForKey:@"id"];
        self.City               = [response valueForKey:@"city"];
        self.Country            = [response valueForKey:@"country"];
        self.Address            = [response valueForKey:@"address"];
        self.State              = [response valueForKey:@"state"];
        self.Customer_id        = [response valueForKey:@"customer_id"];
        self.Long               = [response valueForKey:@"long"];
        self.Lat                = [response valueForKey:@"lat"];
        self.Default_address    = [NSString stringWithFormat:@"%@",(NSNumber*)[response valueForKey:@"default_address"]];
        self.Zip                = [response valueForKey:@"zip"];
    }
    return self;
}

- (void) createOrUpdateCustomerAccountInStorage {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [CustomerAddressList createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}

+ (NSString *)primaryKey {
    return @"AddressId";
}

@end

/*
 @property NSString* AddressId;
 @property NSString* City;
 @property NSString* Country;
 @property NSInteger Address;
 @property NSString* State;
 @property NSString* Customer_id;
 @property NSString* Long;
 @property NSString* Lat;
 @property NSString* Default_address;
 @property NSString* Zip;
 
 [{\"id\":\"165\",
 \"city\":\"Jivani Street\",
 \"country\":\"Shirak Province\",
 \"address\":\"74\",\
 "state\":\"Gyumri\",\
 "customer_id\":\"47\",
 \"long\":\"43.8379932\",
 \"lat\":\"40.7866905\",
 \"default_address\":null,
 \"zip\":\"0012\"},

*/
