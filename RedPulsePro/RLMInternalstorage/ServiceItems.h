//
//  ServiceItems.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/3/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import <Realm/Realm.h>

@interface ServiceItems : RLMObject

//Service Items Details
@property NSString* ItemId;
@property NSString* Name;
@property NSString* ShortDescription;
@property NSString* Price;
@property NSData* imageData;



- (id)initWithRestAPIResponse:(NSDictionary*) response andImageData:(NSData*)imageData;

- (void) createOrUpdateCustomerAccountInStorage;

@end

RLM_ARRAY_TYPE(ServiceItems)

/*
"{\"success\":true,
 \"message\":[
 
 
 {\"id\":\"2\",
 \"name\":\"screen\",
 \"description\":\"tuch screen\",
 \"price\":\"60\",
 \"in_stock\":\"10\",
 \"avatar\":\"http:\\/\\/admin-red.expandis.fr\\/uploads\\/147221717557c04057968882.259479930.jpg\"},
 
 
 
 
 {\"id\":\"5\",
 \"name\":\"galaxy s4\",
 \"description\":\"fgdgh\",
 \"short_description\":\"fgd\",
 \"price\":\"120\",
 \"avatar\":\"http:\\/\\/admin-red.expandis.fr\\/uploads\\/147291655357caec49cae5e2.905353670.jpg\"},
 
 
 
 
 {\"id\":\"10\",\"name\":\"ecran\",\"description\":\"fdgvdfvb dfgbdfghbfdgd fdgb\",\"price\":\"45\",\"in_stock\":\"10\",\"avatar\":\"http:\\/\\/admin-red.expandis.fr\\/uploads\\/147291737157caef7b3069f5.063868660.jpg\"},{\"id\":\"7\",\"name\":\"galaxy s6\",\"description\":\"dsjfhs shdfsj fsjfgshbdfv fsldfsdf fsdksdfhb bdiufghd hjfg ujhg\",\"short_description\":\"dfghdfghf\",\"price\":\"45\",\"avatar\":\"http:\\/\\/admin-red.expandis.fr\\/uploads\\/147291753857caf022574c59.270278780.jpg\"}]}"

*/
