//
//  ProductDetails.m
//  RedPulsePro
//
//  Created by MacMini on 10/23/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "ProductDetails.h"

@implementation ProductDetails

- (id)initWithRestAPIResponse:(NSDictionary*) response  andKey:(NSInteger)key {
    self = [super init];
    if (self) {
        
        self.primKey = [NSString stringWithFormat:@"%ld",key];
        self.Id        = [response valueForKey:@"id"];
        self.Name             = [response valueForKey:@"name"];
        self.Description      = [response valueForKey:@"description"];
        self.Avatar = [response valueForKey:@"avatar"];
        self.Type            = [[response valueForKey:@"type"] integerValue];
    }
    return self;
}

- (void) createOrUpdateCustomerAccountInStorage {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [ProductDetails createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}

+ (NSString *)primaryKey {
    return @"primKey";
}

@end
