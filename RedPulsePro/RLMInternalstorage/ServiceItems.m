//
//  ServiceItems.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/3/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "ServiceItems.h"

@implementation ServiceItems


- (id)initWithRestAPIResponse:(NSDictionary*) response andImageData:(NSData*)imageData{
    self = [super init];
    if (self) {
        self.ItemId           = [response valueForKey:@"id"];
        self.Name             = [response valueForKey:@"name"];
        self.Price            = [response valueForKey:@"price"];
        self.imageData        = imageData;
        self.ShortDescription = [response valueForKey:@"short_description"];
    }
    return self;
}

- (void) createOrUpdateCustomerAccountInStorage {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [ServiceItems createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}

+ (NSString *)primaryKey {
    return @"ItemId";
}

@end
