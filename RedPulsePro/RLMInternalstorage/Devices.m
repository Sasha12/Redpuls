//
//  Devices.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/28/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "Devices.h"

@implementation Devices

- (id)initWithRestAPIResponse:(NSDictionary*) response
{
    self = [super init];
    if (self) {
        self.DeviceId = [response valueForKey:@"id"];
        self.DeviceName = [response  valueForKey:@"name"];
        
    }
    return self;
}

+ (NSString *)primaryKey {
    return @"DeviceId";
}

- (void) createOrUpdateCustomerAccountInStorage {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [Devices createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}


@end
