//
//  CardDetails.h
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <Realm/Realm.h>

@interface CardDetails : RLMObject

//Service Details
@property NSString* Card_logo;
@property NSString* Default_card;
@property NSString* Exp_date;
@property NSString* Brand;
@property NSString* Id;
@property NSString* Last4;
@property NSData * cardLogoData;
- (id)initWithRestAPIResponse:(NSDictionary*) response imageData:(NSData*)imageData;

- (void) createOrUpdateCustomerAccountInStorage;

@end

RLM_ARRAY_TYPE(CardDetails)


//{"id":"9","last4":"1881","brand":"Visa","exp_date":"11\/2023","default_card":"1","card_logo":"http:\/\/red.expandis.fr\/img\/visa.png"}
