//
//  LeftViewController.m
//  LGSideMenuControllerDemo
//
//  Created by Grigory Lutkov on 18.02.15.
//  Copyright (c) 2015 Grigory Lutkov. All rights reserved.
//

#import "LeftViewController.h"
#import "AppDelegate.h"
#import "LeftViewCell.h"
#import "RootViewController.h"
#import "MainViewController.h"
#import "NavigationController.h"
#import "UIViewController+LGSideMenuController.h"
#import "UserAccount.h"
#import "CustomerAccount.h"
#import "RepairerAccount.h"
#import <Realm.h>
#import "LogOutRequest.h"
#import "LoginViewController.h"
#import "HistoryViewController.h"
#import <ProgressHUD.h>

@interface LeftViewController () {
    AppDelegate * appObj;
}

@property (strong, nonatomic) NSArray *titlesArray;
@property () UserAccount *userAccount;
@end

@implementation LeftViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
   appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    self.mUserImage.backgroundColor = [UIColor whiteColor];
    
    RLMResults<UserAccount *> *userAccounts = [UserAccount allObjects];
    self.userAccount = [userAccounts firstObject];
    self.mUserImage.layer.masksToBounds = YES;
    self.mUserImage.layer.cornerRadius = 40;
    self.mUserImage.layer.borderWidth = 1.0f;
    self.mUserImage.layer.borderColor = [UIColor colorWithRed:71/255.0 green:67/255.0 blue:65/255.0 alpha:1.0].CGColor;
    if (self.userAccount.role == 20) {
//        self.mUserIage.hidden = NO;
//        self.mUserNameLbl.hidden = NO;
//        self.mLineImageView.hidden = NO;
//        self.mLogOutBtn.hidden = NO;
//        self.mBackgroundImgView.hidden = NO;
        
        RLMResults<CustomerAccount *> *mUserAccounts = [CustomerAccount allObjects];
        CustomerAccount * mUserAccount = [mUserAccounts firstObject];
        
        [self.mUserImage setImage:[UIImage imageWithData:
                                  [NSData dataWithContentsOfURL:
                                   [NSURL URLWithString:[mUserAccount valueForKey:@"Avatar"]]]]];
        self.mUserName.text = [NSString stringWithFormat:@"%@  %@",[mUserAccount valueForKey:@"Name"],[mUserAccount valueForKey:@"Surname"]];
    } else {
        RLMResults<RepairerAccount *> *mRepairerAccounts = [RepairerAccount allObjects];
        CustomerAccount * mRepairerAccount = [mRepairerAccounts firstObject];
        
        [self.mUserImage setImage:[UIImage imageWithData:
                                   [NSData dataWithContentsOfURL:
                                    [NSURL URLWithString:[mRepairerAccount valueForKey:@"Avatar"]]]]];
        self.mUserName.text = [NSString stringWithFormat:@"%@  %@",[mRepairerAccount valueForKey:@"Name"],[mRepairerAccount valueForKey:@"Surname"]];
        
//        self.mUserIage.hidden = YES;
//        self.mUserNameLbl.hidden = YES;
//        self.mLineImageView.hidden = YES;
//        self.mLogOutBtn.hidden = YES;
//        self.mBackgroundImgView.hidden = YES;
    }

    // -----

    _titlesArray = @[@"Open Right View",
                     @"",
                     @"Profile",
                     @"News",
                     @"Articles",
                     @"Video",
                     @"Music"];

    // -----

  //  self.view.contentInset = UIEdgeInsetsMake(44.f, 0.f, 44.f, 0.f);
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _titlesArray.count;
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LeftViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.textLabel.text = _titlesArray[indexPath.row];
    cell.separatorView.hidden = indexPath.row <= 1 || indexPath.row == _titlesArray.count - 1;
    cell.userInteractionEnabled = indexPath.row != 1;
    cell.tintColor = _tintColor;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row == 1 ? 22.f : 44.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row == 0) {
//        if (![[self sideMenuController] isLeftViewAlwaysVisible]) {
//            [[self sideMenuController] hideLeftViewAnimated:YES completionHandler:^(void) {
//                [[self sideMenuController] showRightViewAnimated:YES completionHandler:nil];
//            }];
//        } else {
//            [[self sideMenuController] showRightViewAnimated:YES completionHandler:nil];
//        }
//    } else {
//        UIViewController *viewController = [UIViewController new];
//        viewController.view.backgroundColor = [UIColor whiteColor];
//        viewController.title = _titlesArray[indexPath.row];
//        [(UINavigationController *)[self sideMenuController].rootViewController pushViewController:viewController animated:YES];
//
//        [[self sideMenuController] hideLeftViewAnimated:YES completionHandler:nil];
//    }
    
    
}

- (IBAction)PaimantAction:(id)sender {
   // PaymentCardsListViewController
    if (self.userAccount.role == 20) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PaymentButtonPressForBlackView" object:nil];
        
        [[self sideMenuController] hideLeftViewAnimated:YES completionHandler:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PaymentButtonPress" object:nil];
        }];
    }else {
        
    }
}

- (IBAction)HalpAction:(id)sender {
}

- (IBAction)NotificationAction:(id)sender {
}

- (IBAction)SettingsAction:(id)sender {
}

- (IBAction)LogOutAction:(id)sender {
    [ProgressHUD show:@"Please wait..."];
    NSLog(@"Log Out!!");
   __block NSString * apiKey;
    
    if (self.userAccount.role == 20) {
        RLMResults<CustomerAccount *> *mCustomerAccount = [CustomerAccount allObjects];
        CustomerAccount * customerOldAccount = [mCustomerAccount firstObject];
        apiKey = [[mCustomerAccount firstObject] valueForKey:@"ApiKey"];
        
        
        //    UserAccount * userAccount = [[UserAccount alloc] initWithUserName:@"" andToken:appObj.gDeviceToken andApiKey:@"" andRole:nil andIsUserLogined:NO];
        //    [userAccount createOrUpdateCustomerAccountInStorage];
        
           }else {
        RLMResults<RepairerAccount *> *mRepairerAccount = [RepairerAccount allObjects];
        RepairerAccount * repairerAccount = [mRepairerAccount firstObject];
        apiKey = [repairerAccount valueForKey:@"ApiKey"];
        
        
        //    UserAccount * userAccount = [[UserAccount alloc] initWithUserName:@"" andToken:appObj.gDeviceToken andApiKey:@"" andRole:nil andIsUserLogined:NO];
        //    [userAccount createOrUpdateCustomerAccountInStorage];
        
       
        
    }

    
    
     LogOutRequest * logOutRequest = [[LogOutRequest alloc] initWithApiKey:apiKey];
    [logOutRequest postWithSuccess:^(NSDictionary *response) {
        NSLog(@"Log out response: = %@",response);
        // CustomerAccount * customerAccount = [[CustomerAccount alloc] initWithOldAccount:customerOldAccount andIsLoginid:NO];
        //[CustomerAccount isU];
        //[[mCustomerAccount firstObject] createOrUpdateCustomerAccountInStorage];
       
        if (self.userAccount.role == 20) {
             [[NSNotificationCenter defaultCenter] postNotificationName:@"LogOutButtonPress" object:nil];
            RLMResults<CustomerAccount *> *mCustomerAccount = [CustomerAccount allObjects];
            CustomerAccount * customerOldAccount = [mCustomerAccount firstObject];
            apiKey = [[mCustomerAccount firstObject] valueForKey:@"ApiKey"];
            
            
            //    UserAccount * userAccount = [[UserAccount alloc] initWithUserName:@"" andToken:appObj.gDeviceToken andApiKey:@"" andRole:nil andIsUserLogined:NO];
            //    [userAccount createOrUpdateCustomerAccountInStorage];
            
            RLMRealm *realm = [RLMRealm defaultRealm];
            RLMResults<UserAccount *> *userAccounts = [UserAccount allObjects];
            UserAccount *userAccount = [userAccounts firstObject];
            
            //    [realm beginWriteTransaction];
            //    userAccount.usernName = @"";
            //    userAccount.role = 0;
            //    userAccount.isUserLogined = NO;
            //    [realm commitWriteTransaction];
            [realm beginWriteTransaction];
            [realm deleteObject:customerOldAccount];
            [realm deleteObject:userAccount];
            [realm commitWriteTransaction];
        }else {
            RLMResults<RepairerAccount *> *mRepairerAccount = [RepairerAccount allObjects];
            RepairerAccount * repairerAccount = [mRepairerAccount firstObject];
            apiKey = [[mRepairerAccount firstObject] valueForKey:@"ApiKey"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"LogOutButtonPressFromRepaier" object:nil];
            
            //    UserAccount * userAccount = [[UserAccount alloc] initWithUserName:@"" andToken:appObj.gDeviceToken andApiKey:@"" andRole:nil andIsUserLogined:NO];
            //    [userAccount createOrUpdateCustomerAccountInStorage];
            
            RLMRealm *realm = [RLMRealm defaultRealm];
            RLMResults<UserAccount *> *userAccounts = [UserAccount allObjects];
            UserAccount *userAccount = [userAccounts firstObject];
            
            //    [realm beginWriteTransaction];
            //    userAccount.usernName = @"";
            //    userAccount.role = 0;
            //    userAccount.isUserLogined = NO;
            //    [realm commitWriteTransaction];
            [realm beginWriteTransaction];
            [realm deleteObject:userAccount];
            [realm deleteObject:repairerAccount];
            [realm commitWriteTransaction];
            
        }

        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLogin"];
        [appObj.myTimer invalidate];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        LoginViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        
        UIWindow *window = [UIApplication sharedApplication].delegate.window;
        
        window.rootViewController = mainViewController;
        
        [UIView transitionWithView:window
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:nil
                        completion:nil];
        [ProgressHUD dismiss];
        //[self dismissViewControllerAnimated:YES completion:nil];
    } failure:^(NSError *error, NSInteger responseCode) {
        [self showAlertWithMessage:@"Error" andText:[NSString stringWithFormat:@"Error N:%ld",(long)responseCode]];
        NSLog(@"Error = %@",error.description);
        [ProgressHUD dismiss];
    }];
    
}

- (IBAction)HistoryAction:(id)sender {
    
   // [ProgressHUD show:@"Please wait..."];
//    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController* viewController = [storyboard instantiateViewControllerWithIdentifier:@"HistoryViewController"];
//    [(UINavigationController *)[self sideMenuController].rootViewController presentViewController:viewController animated:YES completion:nil];
//
//    
//    [[self sideMenuController] hideLeftViewAnimated:YES completionHandler:nil];

    if (self.userAccount.role == 20) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"HystoryButtonPressForBlackView" object:nil];
        
        [[self sideMenuController] hideLeftViewAnimated:YES completionHandler:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HystoryButtonPress" object:nil];
        }];
    }else {
        
    }
    
    
}

- (IBAction)PromotionsAction:(id)sender {

   // [ProgressHUD show:@"Please wait..."];
//    if (self.userAccount.role == 20) {
//        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        UIViewController* viewController = [storyboard instantiateViewControllerWithIdentifier:@"PromotionsViewController"];
//        [(UINavigationController *)[self sideMenuController].rootViewController pushViewController:viewController animated:YES];
//        
//        
//        [[self sideMenuController] hideLeftViewAnimated:YES completionHandler:nil];
//    }else {
//        
//    }
    if (self.userAccount.role == 20) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PromotionButtonPressForBlackView" object:nil];
        
        [[self sideMenuController] hideLeftViewAnimated:YES completionHandler:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PromotionButtonPress" object:nil];
        }];
    }else {
        
    }

    
}


-(void) showAlertWithMessage:(NSString *) message andText:(NSString *) text {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:message
                                  message:text
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [super presentViewController:alert animated:YES completion:nil];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                             
                         }];
    [alert addAction:ok];
    
}

@end
