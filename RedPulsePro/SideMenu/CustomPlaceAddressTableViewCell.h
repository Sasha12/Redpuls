//
//  CustomPlaceAddressTableViewCell.h
//  RedPulsePro
//
//  Created by MacMini on 12/20/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPlaceAddressTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mPrimaryAddress;
@property (weak, nonatomic) IBOutlet UILabel *mSecondaryAddress;

@end
