//
//  LeftViewController.h
//  LGSideMenuControllerDemo
//
//  Created by Grigory Lutkov on 18.02.15.
//  Copyright (c) 2015 Grigory Lutkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftViewController : UIViewController

@property (strong, nonatomic) UIColor *tintColor;

@property (weak, nonatomic) IBOutlet UIImageView *mUserImage;
@property (weak, nonatomic) IBOutlet UILabel *mUserName;
- (IBAction)PaimantAction:(id)sender;
- (IBAction)HalpAction:(id)sender;
- (IBAction)SettingsAction:(id)sender;
- (IBAction)LogOutAction:(id)sender;
- (IBAction)HistoryAction:(id)sender;
- (IBAction)PromotionsAction:(id)sender;
@end
