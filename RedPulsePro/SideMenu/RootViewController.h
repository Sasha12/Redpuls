//
//  ViewController.h
//  LGSideMenuControllerDemo
//
//  Created by Grigory Lutkov on 18.02.15.
//  Copyright (c) 2015 Grigory Lutkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "LGSideMenuController.h"

@interface RootViewController : LGSideMenuController <UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate> {
    MKPointAnnotation *selectedPlaceAnnotation;
}
@property (weak, nonatomic) IBOutlet UICollectionView *repairCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *mDeviceTypeCollectionView;
@property (weak, nonatomic) IBOutlet UIImageView *mAjGic;
@property (weak, nonatomic) IBOutlet UITableView *mTableView;
@property (weak, nonatomic) IBOutlet UITextField *mserchTextFild;
@property (weak, nonatomic) IBOutlet UIButton *mButton;
@property (weak, nonatomic) IBOutlet UISearchBar *mSearchBar;
@property (weak, nonatomic) IBOutlet MKMapView *mMapView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mSearchBarTopConstraint;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *mVisualEffectView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mButtomViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *mOrderBtn;
@property (weak, nonatomic) IBOutlet UIImageView *infoVindowImageView;
@property (weak, nonatomic) IBOutlet UIImageView *mPinImageView;


@property (weak, nonatomic) IBOutlet UILabel *mTotalLbl;
@property (weak, nonatomic) IBOutlet UIView *mDevicesView;
@property (weak, nonatomic) IBOutlet UIView *pointView;
@property (weak, nonatomic) IBOutlet UIButton *mTotalBtn;

@property (weak, nonatomic) IBOutlet UITextField *mSearchTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pinImageView;
@property (weak, nonatomic) IBOutlet UIImageView *mSquarPoint;

@property (weak, nonatomic) IBOutlet UIView *mRepaierDetailsView;
@property (weak, nonatomic) IBOutlet UILabel *mRepaierNameLbl;
@property (weak, nonatomic) IBOutlet UIButton *mRepaierPhoneBtn;
@property (weak, nonatomic) IBOutlet UIView *mButtomView;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *mSwipeGesture;
@property (weak, nonatomic) IBOutlet UIView *testview;
@property (weak, nonatomic) IBOutlet UIView *mAllButtomViews;
@property (weak, nonatomic) IBOutlet UIImageView *menuBtnImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mMapViewBottomConstraint;

@property (weak, nonatomic) IBOutlet UILabel *mCustomerTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mMapButtomConstraint;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *mSw;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *testSW;
@property (weak, nonatomic) IBOutlet UIView *mTestView1;
@property (weak, nonatomic) IBOutlet UIButton *showServicesListBtn;
@property (weak, nonatomic) IBOutlet UIImageView *showServicesListImage;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewForMapButtomConstrain;
@property (weak, nonatomic) IBOutlet UIView *mViewForMap;
- (IBAction)PhoneCallAction:(id)sender;
- (IBAction)curentLocationAction:(id)sender;
- (IBAction)openServicesListBtnAction:(id)sender;

- (IBAction)orderBtnAction:(id)sender;
@end

//AIzaSyBAIHrhGK5JHJmQk6lTJWcP3aGGhLNzc7Q
