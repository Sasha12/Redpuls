//
//  PlaceObject.h
//  RedPulsePro
//
//  Created by MacMini on 12/20/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlaceObject : NSObject


@property() NSString * primaryAddress;
@property() NSString * secondaryAddress;
@end
