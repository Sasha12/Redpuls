//
//  CustomAdressTableViewCell.h
//  RedPulsePro
//
//  Created by MacMini on 12/9/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAdressTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mAddressImage;
@property (weak, nonatomic) IBOutlet UILabel *mAddressLabel;

@end
