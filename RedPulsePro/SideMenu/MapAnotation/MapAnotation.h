//
//  MapAnotation.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/4/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>

@interface MapAnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic,  copy) NSString *title;
@property (nonatomic,  copy) NSString *subtitle;
@property () NSInteger ann_tag;
@end
