//
//  ViewController.m
//  LGSideMenuControllerDemo
//
//  Created by Grigory Lutkov on 18.02.15.
//  Copyright (c) 2015 Grigory Lutkov. All rights reserved.
//

#import "RootViewController.h"
#import "AppDelegate.h"
#import "MainViewController.h"
#import "MapAnotation.h"
#import <CoreLocation/CoreLocation.h>
#import "CustomDeviceTypeCollectionCell.h"
#import "CustomBrandCollectionViewCell.h"
#import "OrderListTableViewCell.h"
#import "Brands.h"
#import "GetBrandFiltr.h"
#import "GetFiltrProductRequest.h"
#import "UserAccount.h"
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import "GetDistanceTimeRequest.h"
#import "GetnearestsRequest.h"
#import "CreateOrderRequest.h"
#import "GetRepairerLocationsRequest.h"
#import "CustomerAccount.h"
#import "DropDownView-1.h"
#import <DXAnnotationView.h>
#import <DXAnnotationSettings.h>
#import "GetCustomerHistoryRequest.h"
#import "CustomerHistoryList.h"
#import "ProductDetails.h"
#import "Services.h"
#import "ServiceItems.h"
#import "GetCustomerAddressRequest.h"
#import "CustomerAddressList.h"
#import "CustomAdressTableViewCell.h"
#import "GetPaymantCardsRequest.h"
#import "CardDetails.h"
#import <Realm.h>
#import <ProgressHUD.h>
#import "Reachability.h"
#import "SMCalloutView.h"
#import "MapStyle.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Marker.h"
#import <GooglePlaces/GooglePlaces.h>
#import "PlaceObject.h"
#import "CustomPlaceAddressTableViewCell.h"
#import "RepairCollectionViewCell.h"


@import GoogleMaps;
#define DEVICE_TYPE_CELL_WIDTH_IPHONE 80
static const CGFloat CalloutYOffset = 40.0f;




@interface TAMapCircle : GMSCircle
{
    CLLocationDistance _from;
    CLLocationDistance _to;
    NSTimeInterval _duration;
}
@property (nonatomic, copy) void(^handler)();
@property (nonatomic, strong) NSDate * begin;
@end

@implementation TAMapCircle
// just call this
-(void)beginRadiusAnimationFrom:(CLLocationDistance)from
                             to:(CLLocationDistance)to
                       duration:(NSTimeInterval)duration
                completeHandler:(void(^)())completeHandler {
    
    self.handler = completeHandler;
    self.begin = [NSDate date];
    _from = from;
    _to = to;
    _duration = duration;
    
    [self performSelectorOnMainThread:@selector(updateSelf) withObject:nil waitUntilDone:NO];
}

// internal update
-(void)updateSelf {
    
    NSTimeInterval i = [[NSDate date] timeIntervalSinceDate:_begin];
    if (i >= _duration) {
        self.radius = _to;
        self.handler();
        return;
    } else {
        CLLocationDistance d = (_to - _from) * i / _duration + _from;
        self.radius = d;
        // do it again at next run loop
        [self performSelectorOnMainThread:@selector(updateSelf) withObject:nil waitUntilDone:NO];
    }
}
@end




@interface RootViewController ()<UITableViewDelegate,UITableViewDataSource,NIDropDownDelegate,UISearchBarDelegate,MKMapViewDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,UIScrollViewDelegate,GMSMapViewDelegate,CAAnimationDelegate,GMSAutocompleteViewControllerDelegate> {
    
    NSMutableArray * mBrandsArray;
    NSMutableArray * mDeviceArray;
    NSMutableArray * mServiceArray;
    NSMutableArray * mServiceArrayForTableView;
    NSMutableArray * mArrayWithNearRepaiers;
    NSMutableArray * mArrayWithDistanc;
    BOOL mSelectedTableView;
    NSString* nearTimeStr;
    UICollectionView *brandCollectionView;
     AppDelegate* appObj;
    
    NSString* mLongitute;
    NSString* mLatitude;
    CLLocationManager *locationManager;
    
    NSMutableDictionary * addressDictionary;
    NSMutableArray * addressArray;
    NSString * address1;
    NSInteger selectedDeviceIndex;
    MapAnotation* CustomerAnnotation;
    MapAnotation* RepaierAnnotation;
    
    NSMutableArray* arrayWithServicesId;
    NSMutableDictionary* arrayWithServicesPrices;
    
    NSTimer* myTimer;
    NSError * error;
    NSData* mData;
    NSDictionary *mDictionary;
    NSDictionary* mMessageDictionary;
    double mNewLat;
    double mNewLong;
    
    UISwipeGestureRecognizer * mSwip;
    UIView * testview;

    CLLocationCoordinate2D selectedCoordinate;
    BOOL showCallout;
    UILabel* lbl;
    NSUInteger countRepair;
    
    CGFloat startX;
    CGFloat startY;
    
    CGFloat endX;
    CGFloat endY;
    BOOL isDone;
    BOOL isOpened;
    NSInteger secectedIndex;
    NSMutableArray * arrayWithImagesRed;
    NSMutableArray * arrayWithImagesBleck;
    UIImageView * brandImage;
    BOOL isFinishedRenderMap;
    BOOL isAccepted;
    BOOL isFirstTime;
    NSInteger repaiersCount;
    NSInteger CustCount;
    NSInteger cout;
    NSMutableDictionary * repaiersAnnotations;
    NSMutableDictionary * customersAnnotations;
    BOOL isLoadMap;
    NSArray* arrayWithServicesList;
    
    NSMutableArray* newServicesArray;
    CGFloat topViewhaigthConstant;
    CGFloat bottomTableViewhaightConstrain;
    
    NSMutableArray * addressListArray;
    BOOL mIsConnected;
    NSMutableArray* markersArray;
    UIImageView * clockArrow;
    BOOL isLoadPlaceAddress;
    UILabel * timeLbl;
    NSTimer * repaierListUpdateTimer;
    __block CLLocationCoordinate2D oldLocation;
    BOOL isFirst;
    BOOL isOpenView;
    BOOL isOpenedBottomView;
    BOOL isMapLoaded;;
    
}
@property (weak, nonatomic) IBOutlet UITableView *mBottomTableView;
@property (nonatomic) UIBackgroundTaskIdentifier backgroundTask;
@property () UserAccount * userAccount;
@property ()NSMutableArray* searchResultPlaces;
@property()SPGooglePlacesAutocompleteQuery* query;
@property()DropDownView* dropDawn;
@property()NSMutableArray* placeNames;
@property() CustomerAccount * customerAccount;
@property() CLLocationCoordinate2D mLocation;
@property ()NSMutableArray * allRepaiersArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mTopViewTopConstrain;
@property (weak, nonatomic) IBOutlet UIButton *mCurrentLocationBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mBottomTableViewTopConstrain;
@property (weak, nonatomic) IBOutlet UIView *mTopView;
@property() UISwipeGestureRecognizer *gestureRecognizer;
@property (weak, nonatomic) IBOutlet UIButton *mSearchBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mSearchBarHorizontalConstraint;
@property (weak, nonatomic) IBOutlet UITextField *mSearchText;
@property() GMSMapView *mapView;
@property()NSMutableArray* nearestRepairesLocationsArray;
@property (strong, nonatomic) SMCalloutView *calloutView;
@property (strong, nonatomic) UIView *emptyCalloutView;
@property()UIImageView * clockImage;
@property()NSArray *animationFrames;
@property() GMSPlacesClient * placeClient;
@property() UILabel * timeLabel;
@property() NSString * repAddress;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mBottomViewTopConstraint;

@property (weak, nonatomic) IBOutlet UIButton *closeRepaierListBnt;

@property (weak, nonatomic) IBOutlet UIButton *showRepaierListBtn;


@property() GMSMarker* repaierMarker;
@property() GMSMarker * customerMarker;
@property() MapStyle * mapStyle;
@property() GMSAutocompleteTableDataSource * acTableView ;

@property() CLLocationCoordinate2D customerLocation;
@property() GMSMarker * mCustomerMarker;
@property()  GMSMarker* currentLocationMarker;
@property() TAMapCircle * circ;

@property() TAMapCircle * circCertr;
@property()float radius;
@end

@implementation RootViewController

#pragma mark - IBActions
- (IBAction)searchBtnAction:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
        self.mSearchBarHorizontalConstraint.constant = 0.2;
        self.mBottomTableViewTopConstrain.constant = 0;
        self.mTopViewTopConstrain.constant = -22;
        CGFloat viewweight = self.view.frame.size.width;
        self.mTopView.frame = CGRectMake(self.mTopView.frame.origin.x, self.mTopView.frame.origin.y, viewweight, self.mTopView.frame.size.height);

        [self.mTopView layoutIfNeeded];
        [self.mBottomTableView layoutIfNeeded];
        [self.mSearchBar becomeFirstResponder ];
        [self.mSearchBtn layoutIfNeeded];
        self.mCurrentLocationBtn.hidden = NO;
        
    }];
    
}

- (IBAction)backButtonPressed:(id)sender {
    self.mapView.frame = CGRectMake(0, 0, self.mViewForMap.frame.size.width, self.mViewForMap.frame.size.height);
    [self.mSearchTextField resignFirstResponder];
    
    
    [UIView animateWithDuration:0.2 animations:^{
        self.mSearchBarHorizontalConstraint.constant = 0.405;
        self.mBottomTableViewTopConstrain.constant = self.mBottomTableView.frame.size.height;
        self.mTopViewTopConstrain.constant = self.mTopView.frame.size.height;
        CGFloat viewweight = self.mSearchText.frame.size.width;
        self.mTopView.frame = CGRectMake(self.mTopView.frame.origin.x, self.mTopView.frame.origin.y, viewweight, self.mTopView.frame.size.height);
        [self.mTopView layoutIfNeeded];
        [self.mBottomTableView layoutIfNeeded];
        [self.mSearchBar resignFirstResponder ];
        self.mCurrentLocationBtn.hidden = YES;
        [self.mSearchBtn layoutIfNeeded];
        

    } completion:^(BOOL finished) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            CGRect newFrame = self.mAllButtomViews.frame;
//            newFrame.origin.y = (self.view.frame.size.height  - self.mAllButtomViews.frame.size.height);
//            self.mAllButtomViews.frame = newFrame;
//            CGRect newFrameForMap = self.mViewForMap.frame;
//            CGRect newFrameForMap1 = self.mViewForMap.frame;
//            newFrameForMap.size.height = self.mAllButtomViews.frame.origin.y;
//            newFrameForMap1.size.height = self.mAllButtomViews.frame.origin.y;
//            self.mViewForMap.frame = newFrameForMap;
//            self.mapView.frame = newFrameForMap1;
//            [_mapView layoutIfNeeded];
//            self.mPinImageView.frame = CGRectMake(self.mPinImageView.frame.origin.x, self.mapView.frame.size.height/2 - self.mPinImageView.frame.size.height, self.mPinImageView.frame.size.width, self.mPinImageView.frame.size.height);
//            self.infoVindowImageView.frame = CGRectMake(self.infoVindowImageView.frame.origin.x, self.mPinImageView.frame.origin.y - self.infoVindowImageView.frame.size.height, self.infoVindowImageView.frame.size.width, self.infoVindowImageView.frame.size.height);
//            [self.mPinImageView layoutIfNeeded];
//            [self.infoVindowImageView layoutIfNeeded];
//            CGRect newFrameForPointView = self.pointView.frame;
//            newFrameForPointView.origin.y = self.mViewForMap.frame.size.height/2;
//            self.pointView.frame = newFrameForPointView;
//            [self.pointView layoutIfNeeded];
//            [self.infoVindowImageView layoutIfNeeded];
//            [self.mAllButtomViews layoutIfNeeded];
//            // [self.view layoutIfNeeded];
//            self.mSearchBtn.hidden = YES;
//            self.mSquarPoint.hidden = YES;
//            
//            [self performSelector:@selector(bottomViewUp) withObject:nil afterDelay:0.5];
//        });
            }];
    
    //[self performSelector:@selector(bottomViewUp) withObject:nil afterDelay:0.5];
   
    
}


-(void)panGesture:(UIPanGestureRecognizer *)gestureRecognizer {
//    CGPoint location = [gestureRecognizer locationInView:self.view];
//    CGPoint velocity = [gestureRecognizer velocityInView:self.view];
//    
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"]) {
//    }else{
//        
//    NSLog(@"locationX %f  locationY %f============velocityX  %f   velocityY  %f",location.x,location.y,velocity.x,velocity.y);
//    
//    if (location.y > (self.view.frame.size.height  - self.mAllButtomViews.frame.size.height)) {
//        
//        if ((velocity.y > 0 && (self.mAllButtomViews.frame.origin.y == self.view.frame.size.height - self.mDevicesView.frame.size.height)) || (velocity.y < 0 && (self.mAllButtomViews.frame.origin.y == (self.view.frame.size.height  - self.mAllButtomViews.frame.size.height)))) {
////            NSLog(@"");
////            self.mSearchBtn.hidden = NO;
////            self.mSquarPoint.hidden = NO;
//        }else {
//            CGRect newFrame = self.mAllButtomViews.frame;
//            newFrame.origin.y = location.y;
//            self.mAllButtomViews.frame = newFrame;
//            CGRect newFrameForMap = self.mViewForMap.frame;
//            CGRect newFrameForMap1 = self.mapView.frame;
//            newFrameForMap.size.height = self.mAllButtomViews.frame.origin.y;
//            newFrameForMap1.size.height = self.mAllButtomViews.frame.origin.y;
//            self.mViewForMap.frame = newFrameForMap;
//            self.mapView.frame = newFrameForMap1;
//            [self.mViewForMap layoutIfNeeded];
//            [_mapView layoutIfNeeded];
//            self.mPinImageView.frame = CGRectMake(self.mPinImageView.frame.origin.x, self.mapView.frame.size.height/2, self.mPinImageView.frame.size.width, self.mPinImageView.frame.size.height);
//             self.infoVindowImageView.frame = CGRectMake(self.infoVindowImageView.frame.origin.x, self.mPinImageView.frame.origin.y - self.infoVindowImageView.frame.size.height, self.infoVindowImageView.frame.size.width, self.infoVindowImageView.frame.size.height);
//            [self.mPinImageView layoutIfNeeded];
//            [self.infoVindowImageView layoutIfNeeded];
//            CGRect newFrameForPointView = self.pointView.frame;
//            newFrameForPointView.origin.y = self.mViewForMap.frame.size.height/2;
//            self.pointView.frame = newFrameForPointView;
//            [self.pointView layoutIfNeeded];
//            self.mSearchBtn.hidden = YES;
//            self.mSquarPoint.hidden = YES;
//            [UIView animateWithDuration:7 animations:^{
//                [self.view layoutIfNeeded];
//            }];
//            
//            if(gestureRecognizer.state == UIGestureRecognizerStateEnded)
//            {
//                if (velocity.y > 0) {
//                    //down
//                    [UIView animateWithDuration:0.5 animations:^{
//                        CGRect newFrame = self.mAllButtomViews.frame;
//                        newFrame.origin.y = self.view.frame.size.height - self.mDevicesView.frame.size.height;
//                        self.mAllButtomViews.frame = newFrame;
//                        
//                        CGRect newFrameForMap = self.mViewForMap.frame;
//                        CGRect newFrameForMap1 = self.mapView.frame;
//                        newFrameForMap.size.height = self.mAllButtomViews.frame.origin.y;
//                        newFrameForMap1.size.height = self.mAllButtomViews.frame.origin.y;
//                        self.mViewForMap.frame = newFrameForMap;
//                        self.mapView.frame = newFrameForMap1;
//                        [self.mViewForMap layoutIfNeeded];
//                         [_mapView layoutIfNeeded];
//                         [self.infoVindowImageView layoutIfNeeded];
//                        CGRect newFrameForPointView = self.pointView.frame;
//                        newFrameForPointView.origin.y = self.mViewForMap.frame.size.height/2;
//                        self.pointView.frame = newFrameForPointView;
//                        [self.pointView layoutIfNeeded];
//                        self.mPinImageView.frame = CGRectMake(self.mPinImageView.frame.origin.x, self.mapView.frame.size.height/2 - self.mPinImageView.frame.size.height, self.mPinImageView.frame.size.width, self.mPinImageView.frame.size.height);
//                         self.infoVindowImageView.frame = CGRectMake(self.infoVindowImageView.frame.origin.x, self.mPinImageView.frame.origin.y - self.infoVindowImageView.frame.size.height, self.infoVindowImageView.frame.size.width, self.infoVindowImageView.frame.size.height);
//                        [self.mPinImageView layoutIfNeeded];
//                        [self.infoVindowImageView layoutIfNeeded];
//                        [self.view layoutIfNeeded];
//                        self.mSearchBtn.hidden = NO;
//                        self.mSquarPoint.hidden = NO;
//                        isOpenedBottomView = NO;
//                    }];
//                }else{
//                    //up
//                    [UIView animateWithDuration:0.5 animations:^{
//                        CGRect newFrame = self.mAllButtomViews.frame;
//                        newFrame.origin.y = (self.view.frame.size.height  - self.mAllButtomViews.frame.size.height);
//                        self.mAllButtomViews.frame = newFrame;
//                        CGRect newFrameForMap = self.mViewForMap.frame;
//                        CGRect newFrameForMap1 = self.mViewForMap.frame;
//                        newFrameForMap.size.height = self.mAllButtomViews.frame.origin.y;
//                        newFrameForMap1.size.height = self.mAllButtomViews.frame.origin.y;
//                        self.mViewForMap.frame = newFrameForMap;
//                        self.mapView.frame = newFrameForMap1;
//                        [self.mapView layoutIfNeeded];
//                         [_mapView layoutIfNeeded];
//                        self.mPinImageView.frame = CGRectMake(self.mPinImageView.frame.origin.x, self.mapView.frame.size.height/2 - self.mPinImageView.frame.size.height, self.mPinImageView.frame.size.width, self.mPinImageView.frame.size.height);
//                         self.infoVindowImageView.frame = CGRectMake(self.infoVindowImageView.frame.origin.x, self.mPinImageView.frame.origin.y - self.infoVindowImageView.frame.size.height, self.infoVindowImageView.frame.size.width, self.infoVindowImageView.frame.size.height);
//                        [self.mPinImageView layoutIfNeeded];
//                        [self.infoVindowImageView layoutIfNeeded];
//                        CGRect newFrameForPointView = self.pointView.frame;
//                        newFrameForPointView.origin.y = self.mViewForMap.frame.size.height/2;
//                        self.pointView.frame = newFrameForPointView;
//                        [self.pointView layoutIfNeeded];
//                        [self.infoVindowImageView layoutIfNeeded];
//                        [self.view layoutIfNeeded];
//                        self.mSearchBtn.hidden = YES;
//                        self.mSquarPoint.hidden = YES;
//                        isOpenedBottomView = YES;
//                    }];
////                    selectedDeviceIndex = 0;
////                    [self.mDeviceTypeCollectionView reloadData];
////                    [self getServicesWithIndex:0];
//                }
//                
//                
//                NSLog(@"END:::::::");
//            }
// 
//        }
//     
//    }
//    }
}
- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"]) {
        
    }else {
       
            [self getNearRepaierTime];
           [self getAdressFromCordinate:[self.mapView.projection coordinateForPoint:self.mapView.center]];
        
   
    
    [clockArrow startAnimating];
    
    
    }
}



#pragma mark Geacoder
-(NSString*)getAdressFromCordinate :(CLLocationCoordinate2D)coordinates {
    __block NSString* adress;
    CLGeocoder * ceo = [[CLGeocoder alloc]init];
    CLLocation* loc = [[CLLocation alloc]initWithLatitude:coordinates.latitude longitude:coordinates.longitude];
    [ceo reverseGeocodeLocation:loc completionHandler:^(NSArray * placemarks, NSError*  error){
        CLPlacemark* placemark = [placemarks objectAtIndex:0];
        if (placemark){
            addressDictionary = (NSMutableDictionary*)[placemark addressDictionary];
            adress = [addressDictionary objectForKey:@"Name"];
            _repAddress = adress;
            BOOL isAceppt = [[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"];
            if (isAceppt) {
                 _mapView.selectedMarker = _repaierMarker;
            }else{
                _mapView.selectedMarker = nil;
            }
            
            if (_timeLabel) {
                _timeLabel.text = adress;
               
            }
            if(mSelectedTableView){
            [self.mSearchBtn setTitle:[NSString stringWithFormat:@"    %@",adress] forState:UIControlStateNormal];
                mSelectedTableView = NO;
            }
             NSLog(@"adress = %@", adress);
        }else{
            NSLog(@"Could not locate");
        }
    }
     ];

    return adress;
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker* )marker
{
    
    BOOL isExist = [[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"];
    if (isExist) {
        
         mapView.selectedMarker = marker;
    }else {
        mapView.selectedMarker = nil;
    }
   
    return YES;
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture {
    
}


- (void)mapView:(GMSMapView *)mapView didDragMarker:(GMSMarker *)marker {
    
}
- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker {

    
}

- (void)mapViewDidStartTileRendering:(GMSMapView *)mapView {
    
}

- (void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay {
    
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    BOOL isExist = [[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"];
    if (isExist) {

     mapView.selectedMarker = _repaierMarker;
    }else {
        mapView.selectedMarker = nil;
    }
}
-(UIView* )mapView:(GMSMapView* )mapView markerInfoWindow:(GMSMarker *)marker {
    
   // if (marker == _customerMarker) {
        /*
        marker.infoWindowAnchor = CGPointMake(0.65,1);
        CLLocationCoordinate2D anchor = marker.position;
        CGPoint point = [mapView.projection pointForCoordinate:anchor];
        self.calloutView.title = marker.title;
        self.calloutView.calloutOffset = CGPointMake(0, CalloutYOffset);
        self.calloutView.hidden = NO;
        [self.calloutView becomeFirstResponder];
        CGRect calloutRect = CGRectMake(40, 40, 140, 40);
        calloutRect.origin = point;
        self.calloutView.hidden = NO;
        
        calloutRect.size = CGSizeMake(140, 40);
        
        */
    marker.infoWindowAnchor = CGPointMake(0.5,0);
        self.calloutView.calloutOffset = CGPointMake(-10, -60);
        UIView * infoView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 120, 50)];
        infoView.backgroundColor = [UIColor colorWithRed:202/255.f green:37/255.f blue:44/255.f alpha:1];
        infoView.layer.cornerRadius = 10;
       
        UIImageView * bgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, infoView.frame.size.width, infoView.frame.size.height)];
        bgImageView.image = [UIImage imageNamed:@"pin_view"];
        [infoView addSubview:bgImageView];
        
        self.clockImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 9, 18, 18)];
        _clockImage.image = [UIImage imageNamed:@"clock"];
        _clockImage.backgroundColor = [UIColor clearColor];
        
        clockArrow = [[UIImageView alloc] initWithFrame:CGRectMake(10, 9, 18, 18)];
        clockArrow.image = [UIImage imageNamed:@""];
        clockArrow.backgroundColor = [UIColor clearColor];
        
    
       /*
        _animationFrames = [NSArray arrayWithObjects:[UIImage imageNamed:@"1.png"],[UIImage imageNamed: @"2.png"],[UIImage imageNamed:@"3.png"],[UIImage imageNamed: @"4.png"],[UIImage imageNamed:@"5.png"],[UIImage imageNamed: @"6.png"],[UIImage imageNamed:@"7.png"],[UIImage imageNamed: @"8.png"] ,[UIImage imageNamed: @"9.png"],[UIImage imageNamed: @"10.png"],[UIImage imageNamed: @"11.png"],[UIImage imageNamed: @"12.png"],nil];
        clockArrow.animationImages = _animationFrames;
        [clockArrow startAnimating];
        */
        
        _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 2, 90, 40)];
    if (![_repAddress isEqualToString:@""]) {
         _timeLabel.text = _repAddress;
    }else{
         _timeLabel.text = @"";
    }
    
        _timeLabel.textColor = [UIColor whiteColor];
        _timeLabel.numberOfLines = 3;
        _timeLabel.minimumScaleFactor = (7.0/([UIFont labelFontSize]));
        _timeLabel.adjustsFontSizeToFitWidth = YES;
        [infoView addSubview:_timeLabel];
        //[infoView addSubview:_clockImage];
      //  [infoView addSubview:clockArrow];
        
        


        
        
        //return self.emptyCalloutView;
        return infoView;
         
   
    
}


#pragma Mark MapView Move



- (void)timerAnimating {
    clockArrow.transform = CGAffineTransformMakeRotation(M_PI * 180);
    [clockArrow startAnimating];
}


- (void)mapView:(GMSMapView* )pMapView didChangeCameraPosition:(GMSCameraPosition* )position {
    
    BOOL isExist = [[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"];
    if (isExist) {
        _circCertr.map = nil;
        _circCertr = nil;
        _circ.map = nil;
        _circ = nil;
    }else {
    self.radius = 100000;
    if(position.zoom > 15){
        self.radius = self.radius/(position.zoom* 1000);
    }
    if(position.zoom < 15 && position.zoom > 10){
        self.radius = self.radius/(position.zoom* 80);
    }
    if(position.zoom < 10 && position.zoom > 5){
        self.radius = self.radius/(position.zoom* 8);
    }
    if(position.zoom < 5 && position.zoom > 1){
        self.radius = self.radius/(position.zoom* 5);
    }else{
        self.radius = self.radius/(position.zoom);
    }
    
    NSLog(@"====================self.radius = %f",self.radius);
    _circCertr.map = nil;
    _circCertr = nil;
    _circ.map = nil;
    _circ = nil;
    
    if (_circ && _circCertr) {
        [_circ beginRadiusAnimationFrom:_radius to:_radius *100 duration:0.1 completeHandler:^{
            
        }];
    }else{
        
        CLLocationCoordinate2D circleCenter = CLLocationCoordinate2DMake([mLatitude doubleValue], [mLongitute doubleValue]);
        
        _circCertr = [TAMapCircle circleWithPosition:circleCenter radius:_radius + 5];
        _circCertr.fillColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:1];
        _circCertr.strokeColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:1];
        _circCertr.strokeWidth = 1.5f;
        
        _circCertr.map = _mapView;
        
        
        _circ = [TAMapCircle circleWithPosition:circleCenter radius:_radius * 1000];
        _circ.fillColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:0.1f];
        _circ.strokeColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:0.5f];
        _circ.strokeWidth = 1.5f;
        
        _circ.map = _mapView;
        [_circ beginRadiusAnimationFrom:_radius to:_radius * 100 duration:0.1 completeHandler:^{
            
        }];
    }

    
    if(_customerMarker){
    self.infoVindowImageView.hidden = YES;
    }
    if (pMapView.selectedMarker != nil && !self.calloutView.hidden) {
        CLLocationCoordinate2D anchor = [pMapView.selectedMarker position];
        
        CGPoint arrowPt = self.calloutView.backgroundView.arrowPoint;
        
        CGPoint pt = [pMapView.projection pointForCoordinate:anchor];
        pt.x -= arrowPt.x;
        pt.y -= arrowPt.y + CalloutYOffset;
        
        //self.calloutView.frame = (CGRect) {.origin = pt, .size = self.calloutView.frame.size };
    } else {
       // self.calloutView.hidden = YES;
    }
    }
    
    BOOL isAceppt = [[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"];
    if (isAceppt) {
        _mapView.selectedMarker = nil;
    }
}
-(void)viewDidAppear:(BOOL)animated {
    
    
   [self.clockImage startAnimating];
    
    self.mSearchBtn.layer.shadowColor = [UIColor grayColor].CGColor;
    self.mSearchBtn.layer.shadowOffset = CGSizeMake(0, 15.0);
    self.mSearchBtn.layer.shadowOpacity = 0.3;
    self.mSearchBtn.layer.shadowRadius = 10.0;
    [locationManager startUpdatingLocation];
    
    self.mButtomViewHeight.constant = -(self.mAllButtomViews.frame.size.height);
    self.mCurrentLocationBtn.hidden = YES;
    [self performSelector:@selector(hideVisualEffectView) withObject:nil afterDelay:0.5];
    self.mSearchBar.delegate = self;
   // self.navigationController.navigationBarHidden = YES;
    [self setStatusBarBackgroundColor:[UIColor clearColor]];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
//    self.navigationController.navigationBar.shadowImage = nil;////UIImageNamed:@"transparent.png"
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.view.backgroundColor = [UIColor clearColor];
//    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
//    [UIView animateWithDuration:0.5 animations:^{
//        CGRect newFrame = self.mAllButtomViews.frame;
//        newFrame.origin.y = self.view.frame.size.height - self.mDevicesView.frame.size.height;
//        self.mAllButtomViews.frame = newFrame;
//        
//        CGRect newFrameForMap = self.mViewForMap.frame;
//        newFrameForMap.size.height = self.mAllButtomViews.frame.origin.y;
//        self.mViewForMap.frame = newFrameForMap;
//        [self.mViewForMap layoutIfNeeded];
//        [self.view layoutIfNeeded];
//    }];
//
    
      
//    RLMResults<Brands*>* mBrands = [Brands allObjects];
//    mBrandsArray = (NSMutableArray*)mBrands;
    
    RLMResults<CustomerAccount*>* mCustomerAccount = [CustomerAccount allObjects];
    _customerAccount = [mCustomerAccount firstObject];
    arrayWithImagesRed = [[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"nokia_red"],[UIImage imageNamed:@"apple_red"],[UIImage imageNamed:@"samsung_red"], nil];
    arrayWithImagesBleck = [[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"nokia_sev"],[UIImage imageNamed:@"apple_sev"],[UIImage imageNamed:@"samsung_sev"], nil];
    if([self checkInternetConnection]){
        [self getDevicesWithBrandId:[[mBrandsArray objectAtIndex:1] valueForKey:@"BrandId"]];
    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }
    
    
    

   
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        brandCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, (mBrandsArray.count*150), 80) collectionViewLayout:layout];
        //brandCollectionView.collectionViewLayout = layout;
        brandCollectionView.backgroundColor = [UIColor clearColor];
        self.navigationItem.titleView = brandCollectionView;
        [brandCollectionView registerClass:[CustomBrandCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        brandCollectionView.delegate = self;
        brandCollectionView.dataSource = self;
        brandCollectionView.showsHorizontalScrollIndicator = YES;
        self.navigationItem.titleView = brandCollectionView;
        
    }else{
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        brandCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, (mBrandsArray.count*90), 40) collectionViewLayout:layout];
//        layout.minimumInteritemSpacing = 10;
//        layout.minimumLineSpacing = 10;
        brandCollectionView.backgroundColor = [UIColor clearColor];
        self.navigationItem.titleView = brandCollectionView;
        
        [brandCollectionView registerClass:[CustomBrandCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        brandCollectionView.delegate = self;
        brandCollectionView.dataSource = self;
        brandCollectionView.backgroundColor = [UIColor clearColor];
        brandCollectionView.showsHorizontalScrollIndicator = YES;
        self.navigationItem.titleView = brandCollectionView;
    }
    
    self.mDeviceTypeCollectionView.delegate = self;
    self.mDeviceTypeCollectionView.dataSource = self;
    [self initSPGooglePlacesAutocompleteQuery];
    
  
       
 

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handelAcceptNotification:)
                                                 name:ShowAcceptNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handelDoneNotification:)
                                                 name:ShowDoneNotification
                                               object:nil];

    BOOL isExist = [[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"];
    if (isExist) {
         [locationManager stopUpdatingLocation];
        self.mSearchBtn.hidden = YES;
        self.mSquarPoint.hidden = YES;
        self.infoVindowImageView.hidden = YES;
        self.mPinImageView.hidden = YES;
        [self.view layoutIfNeeded];
        
        [self addRepairAnnotationWithLatitude:[[[NSUserDefaults standardUserDefaults] objectForKey:@"repaierLat"] doubleValue] andLongitude:[[[NSUserDefaults standardUserDefaults] objectForKey:@"repaierLong"] doubleValue]];
        //_mapView.selectedMarker = _repaierMarker;
        // _customerLocation
        CLLocationCoordinate2D repCoordinates = CLLocationCoordinate2DMake([[[NSUserDefaults standardUserDefaults] objectForKey:@"repaierLat"] doubleValue], [[[NSUserDefaults standardUserDefaults] objectForKey:@"repaierLong"] doubleValue]);
        [self getAdressFromCordinate:repCoordinates];
        
        
    }
   //[self showAllMarkers:[NSMutableArray arrayWithObjects:_repaierMarker,_mCustomerMarker, nil]];
    
//    [UIView animateWithDuration:0.5 animations:^{
//        CGRect newFrame = self.mAllButtomViews.frame;
//        newFrame.origin.y = self.view.frame.size.height - self.mDevicesView.frame.size.height;
//        self.mAllButtomViews.frame = newFrame;
//        
//        CGRect newFrameForMap = self.mViewForMap.frame;
//        CGRect newFrameForMap1 = self.mapView.frame;
//        newFrameForMap.size.height = self.mAllButtomViews.frame.origin.y;
//        newFrameForMap1.size.height = self.mAllButtomViews.frame.origin.y;
//        self.mViewForMap.frame = newFrameForMap;
//        self.mapView.frame = newFrameForMap1;
//        [self.mViewForMap layoutIfNeeded];
//        [_mapView layoutIfNeeded];
//        [self.infoVindowImageView layoutIfNeeded];
//        CGRect newFrameForPointView = self.pointView.frame;
//        newFrameForPointView.origin.y = self.mViewForMap.frame.size.height/2;
//        self.pointView.frame = newFrameForPointView;
//        [self.pointView layoutIfNeeded];
//        self.mPinImageView.frame = CGRectMake(self.mPinImageView.frame.origin.x, self.mapView.frame.size.height/2 - self.mPinImageView.frame.size.height, self.mPinImageView.frame.size.width, self.mPinImageView.frame.size.height);
//        self.infoVindowImageView.frame = CGRectMake(self.infoVindowImageView.frame.origin.x, self.mPinImageView.frame.origin.y - self.infoVindowImageView.frame.size.height, self.infoVindowImageView.frame.size.width, self.infoVindowImageView.frame.size.height);
//        [self.mPinImageView layoutIfNeeded];
//        [self.infoVindowImageView layoutIfNeeded];
//        [self.view layoutIfNeeded];
//        
//        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"]) {
//        self.mSearchBtn.hidden = YES;
//        self.mSquarPoint.hidden = YES;
//        }else{
//            self.mSearchBtn.hidden = NO;
//            self.mSquarPoint.hidden = NO;
//        }
//    }];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"]) {
                self.mSearchBtn.hidden = YES;
                self.mSquarPoint.hidden = YES;
                }else{
                    self.mSearchBtn.hidden = NO;
                    self.mSquarPoint.hidden = NO;
                }
    self.mTableView.hidden = YES;
    self.nearestRepairesLocationsArray = [[NSMutableArray alloc]init];
    GetnearestsRequest * mGetnearestsRequest = [[GetnearestsRequest alloc] initWithApiKey:_userAccount.apiKey];
    
    [mGetnearestsRequest postWithSuccess:^(NSDictionary *response) {
        NSLog(@"response : %@",response);
        NSError * myError;
        NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&myError];
        self.nearestRepairesLocationsArray = (NSMutableArray*)[dictionary objectForKey:@"message"];
        //NSLog(@"nearestRepairesLocationsArray : %@",self.nearestRepairesLocationsArray);
        repaiersCount = self.nearestRepairesLocationsArray.count;
        
        
        
        if(markersArray){
            
            for (GMSMarker *m in markersArray){
                m.map = nil;
                
            }
            
        }
        
        if (self.nearestRepairesLocationsArray.count > 0) {
            for (int i = 0; i < self.nearestRepairesLocationsArray.count; i++) {
                //[self performSelectorInBackground:@selector(addRepaiersWithIndex:) withObject:[NSNumber numberWithInt:i]];
                //[self.mMapView showAnnotations:self.mMapView.annotations animated:YES];
                //[self recenterMapToPlacemarkForRepaiers:CLLocationCoordinate2DMake([[[mServiceArray objectAtIndex:i] valueForKey:@"lat"] doubleValue], [[[mServiceArray objectAtIndex:i] valueForKey:@"long"] doubleValue])];
                
                
                [self addRepairAnnotationWithLatitude:[[[self.nearestRepairesLocationsArray objectAtIndex:i] valueForKey:@"lat"] doubleValue]
                                         andLongitude:[[[self.nearestRepairesLocationsArray objectAtIndex:i] valueForKey:@"long"] doubleValue]];
            }
            //            if(isFirst){
            //            [self showAllMarkers:markersArray];
            //                isFirst = NO;
            //            }
            // [self.mMapView showAnnotations:self.mMapView.annotations animated:YES];
            
            //[self getNearRepaierTime];
        }
        countRepair = 0;
        [self getNearRepaierTime];
        // [self addTime];
        
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"error : %@",error.description);
        
    }];
   
    
    if (isOpenedBottomView) {
        self.mSearchBtn.hidden = YES;
        self.mSquarPoint.hidden = YES;
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"]) {
        self.mSearchBtn.hidden = YES;
        self.mSquarPoint.hidden = YES;
        self.infoVindowImageView.hidden = YES;
        self.mPinImageView.hidden = YES;
        [repaierListUpdateTimer invalidate];
        _mapView.myLocationEnabled = NO;
    }else {
        _mapView.myLocationEnabled = NO;
        repaierListUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:25 target:self
                                                 selector:@selector(updateGoogleMap) userInfo:nil repeats:YES];
    }
    
    [arrayWithServicesId removeAllObjects];
    selectedDeviceIndex = -1;
    [self.mDeviceTypeCollectionView reloadData];
    [self getServicesWithIndex:0];
    
    self.showRepaierListBtn.hidden = NO;
    self.closeRepaierListBnt.hidden = YES;
    self.menuBtnImageView.image = [UIImage imageNamed:@"ic_menu"];
    self.mMapViewBottomConstraint.constant = 0;
    self.mButtomViewHeight.constant = -(234);
    [self.mAllButtomViews layoutIfNeeded];
    self.mSearchBtn.hidden = NO;
    self.mSquarPoint.hidden = NO;
    
    CGRect newFrameForMap1 = self.mapView.frame;
    newFrameForMap1.size.height = self.view.frame.size.height;
    self.mViewForMap.frame = newFrameForMap1;
    self.mapView.frame = newFrameForMap1;
    [self.mViewForMap layoutIfNeeded];
    [self.mapView layoutIfNeeded];
    [self.view layoutIfNeeded];

//    [self.mDeviceTypeCollectionView
//     selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]
//     animated:YES
//     scrollPosition:UICollectionViewScrollPositionCenteredVertically];
//    
//    selectedDeviceIndex = 0;
//    [self.mDeviceTypeCollectionView reloadData];
//    [self getServicesWithIndex:0];
//    
//    [self.mDeviceTypeCollectionView reloadData];
//    [self.mTableView reloadData];
}

#pragma mark INFO VIEW


-(void)addInfoView{
    NSLog(@"time");
    if (!self.clockImage && !clockArrow  && !timeLbl) {
        self.clockImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 9, 18, 18)];
        clockArrow = [[UIImageView alloc] initWithFrame:CGRectMake(10, 9, 18, 18)];
        timeLbl = [[UILabel alloc] initWithFrame:CGRectMake(31, 9, 85, 18)];
    }
    
    _clockImage.image = [UIImage imageNamed:@"clock"];
    _clockImage.backgroundColor = [UIColor clearColor];
    
    
    clockArrow.image = [UIImage imageNamed:@""];
    clockArrow.backgroundColor = [UIColor clearColor];
    [self.infoVindowImageView addSubview:_clockImage];
    [self.infoVindowImageView addSubview:clockArrow];
    
   
    timeLbl.adjustsFontSizeToFitWidth=YES;
    timeLbl.minimumScaleFactor=0.5;
    
    timeLbl.text = @"00:00";
    timeLbl.textColor = [UIColor whiteColor];
    
    [self.infoVindowImageView addSubview:timeLbl];
    if(_customerMarker){
        self.infoVindowImageView.hidden = NO;
    }
    
    
    _animationFrames = [NSArray arrayWithObjects:[UIImage imageNamed:@"1.png"],[UIImage imageNamed:@"2.png"],[UIImage imageNamed:@"3.png"],[UIImage imageNamed:@"4.png"],[UIImage imageNamed:@"5.png"],[UIImage imageNamed:@"6.png"],[UIImage imageNamed:@"7.png"],[UIImage imageNamed:@"8.png"],[UIImage imageNamed: @"9.png"],[UIImage imageNamed:@"10.png"],[UIImage imageNamed: @"11.png"],[UIImage imageNamed:@"12.png"],nil];
    
    clockArrow.animationImages = _animationFrames;
    //clockArrow.animationDuration = 720;
    [clockArrow startAnimating];

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"]) {
        self.mSearchBtn.hidden = YES;
        self.mSquarPoint.hidden = YES;
        self.infoVindowImageView.hidden = YES;
        self.mPinImageView.hidden = YES;
       
        CGRect newFrameForMap = self.mViewForMap.frame;
        CGRect newFrameForMap1 = self.mapView.frame;
        newFrameForMap.size.height = self.view.frame.size.height - self.mRepaierDetailsView.frame.size.height;
        newFrameForMap1.size.height = self.view.frame.size.height - self.mRepaierDetailsView.frame.size.height;
        self.mViewForMap.frame = newFrameForMap;
        self.mapView.frame = newFrameForMap1;
        [self.mViewForMap layoutIfNeeded];
        [_mapView layoutIfNeeded];

    }
}


-(void)chackIsMapLoadid {
    if (isMapLoaded) {
        NSLog(@"Inet ka");
        [ProgressHUD dismiss];
        //self.mVisualEffectView.hidden = YES;
    }else {
        [self showAlertWithMessage:@"Your internet speed is slow.Please restart the app." andText:@"Internet error!"];
    }
}
-(void)viewWillAppear:(BOOL)animated {
    mSelectedTableView = NO;
    isMapLoaded = NO;
    self.repairCollectionView.delegate = self;
    self.repairCollectionView.dataSource = self;
    [ProgressHUD show:@"Please wait..."];
    //self.mVisualEffectView.hidden = NO;
     self.mButtomViewHeight.constant = -(self.mAllButtomViews.frame.size.height);
   
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(searchTextDidChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
    isOpenView = NO;
    //self.mVisualEffectView.hidden = NO;
    [ProgressHUD show:@"Please wait..."];
    self.mButtomViewHeight.constant = 0;
    //[self.navigationController.navigationBar setTintColor:[UIColor clearColor]];
    //self.navigationController.navigationBar.appearance
    newServicesArray = [[NSMutableArray alloc] init];
    RLMResults<Services *> *mServices = [Services allObjects];
    newServicesArray = (NSMutableArray*)mServices;
    
    if([self checkInternetConnection]){
        [self getServicesWithIndex:0];
    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }

    
    [self.mapView addObserver:self forKeyPath:@"myLocation" options:0 context:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handelHystoryButtonPress)
                                                 name:@"HystoryButtonPress"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handelHystoryButtonPressForBlackView)
                                                 name:@"HystoryButtonPressForBlackView"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handelPromotionButtonPress)
                                                 name:@"PromotionButtonPress"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handelPromotionButtonPressForBlackView)
                                                 name:@"PromotionButtonPressForBlackView"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handelPaymentButtonPress)
                                                 name:@"PaymentButtonPress"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handelPaymentButtonPressForBlackView)
                                                 name:@"PaymentButtonPressForBlackView"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handelLogOutButtonPress)
                                                 name:@"LogOutButtonPress"
                                               object:nil];
    
    RLMResults<UserAccount*>* mUserAccount = [UserAccount allObjects];
    _userAccount = [mUserAccount firstObject];
    
   
    
    [self performSelector:@selector(chackIsMapLoadid) withObject:nil afterDelay:5];
    
    
}



-(void)viewDidLoad {
    
    [ProgressHUD show:@"Please wait..."];
    //[self addCustomCollautView];
    //self.mVisualEffectView.hidden = NO;
    super.panGesture.delegate = self;
    isOpened = NO;
    isLoadPlaceAddress = NO;
    isAccepted = NO;
    isFirstTime = NO;
    isDone = NO;
    isLoadMap = NO;
    isOpenedBottomView = NO;
    isFinishedRenderMap = NO;
    secectedIndex = 1;
    repaiersCount = 0;
    CustCount = 0;
    cout = 0;
    isFirst = YES;
    markersArray = [[NSMutableArray alloc] init];
    self.mSearchText.delegate = self;
    self.mBottomTableView.delegate = self;
    self.mBottomTableView.dataSource = self;
    repaiersAnnotations = [[NSMutableDictionary alloc] init];
    customersAnnotations = [[NSMutableDictionary alloc] init];
    mServiceArrayForTableView = [[NSMutableArray alloc] init];
    _placeClient = [[GMSPlacesClient alloc] init];
    
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(30, 10, 70, 20)];
    _allRepaiersArray = [[NSMutableArray alloc] init];
    
    [self.mSearchBar setImage:[UIImage imageNamed:@"icon_images"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    
    selectedDeviceIndex = -1;
    self.mMapView.delegate = self;
    self.mTotalLbl.text = [NSString stringWithFormat:@"$0"];
    arrayWithServicesId = [[NSMutableArray alloc] init];
    arrayWithServicesPrices = [[NSMutableDictionary alloc] init];
    mArrayWithNearRepaiers = [[NSMutableArray alloc] init];
    mArrayWithDistanc = [[NSMutableArray alloc] init];
    
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager requestWhenInUseAuthorization];
    //[locationManager requestAlwaysAuthorization];
    
    
    self.mButtomViewHeight.constant = 0;
    self.mSearchTextField.delegate = self;
    self.searchResultPlaces = [[NSMutableArray alloc]init];
    RLMResults<UserAccount*>* mUserAccount = [UserAccount allObjects];
    _userAccount = [mUserAccount firstObject];
    
    RLMResults<CustomerAddressList*>* mCustomerAddressList = [CustomerAddressList allObjects];
    
    addressListArray = (NSMutableArray*)mCustomerAddressList;
    

    RLMResults<CustomerAddressList *> *mCardDetailsSorted = [mCustomerAddressList sortedResultsUsingProperty:@"Default_address" ascending:NO];
    addressListArray = (NSMutableArray*)mCardDetailsSorted;
    
    
    self.mBottomTableViewTopConstrain.constant = self.mBottomTableView.frame.size.height;
    self.mTopViewTopConstrain.constant = self.mTopView.frame.size.height;
    
     
    
    //===========================
  
    /*
    // Polyline
   
    GMSMutablePath *path = [GMSMutablePath path];
    [path addCoordinate:CLLocationCoordinate2DMake(@(40.793421).doubleValue,@(43.844524).doubleValue)];//40.793421, 43.844524
    [path addCoordinate:CLLocationCoordinate2DMake(@(40.809970).doubleValue,@(43.840190).doubleValue)];//40.809970, 43.840190
    
    GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
    rectangle.strokeWidth = 2.f;
    rectangle.map = _mapView;
    self.view=_mapView;
    rectangle.geodesic = true;
    */
      // self.mViewForMap = mapView;
    //==========================
    
   
}

-(void)mapViewDidFinishTileRendering:(GMSMapView *)mapView {
    isMapLoaded = YES;
    NSLog(@"========================= mapViewDidFinishTileRendering ========================");
}
- (void)viewWillDisappear:(BOOL)animated
{
    [repaierListUpdateTimer invalidate];
    
    [locationManager stopUpdatingLocation];
    
    [super viewWillDisappear:animated];
    self.mTotalLbl.text = @"$0";
    [arrayWithServicesPrices removeAllObjects];
    // unregister for keyboard notifications while not visible.
  
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"HystoryButtonPress"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"HystoryButtonPressForBlackView"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"PromotionButtonPress"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"PromotionButtonPressForBlackView"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"PaymentButtonPress"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"PaymentButtonPressForBlackView"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"LogOutButtonPress"
                                                  object:nil];
    
    
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary* )change context:(void *)context {
    if([keyPath isEqualToString:@"myLocation"]) {
        CLLocation *location = [object myLocation];
        //...
        NSLog(@"Location, %@,", location);
        
        CLLocationCoordinate2D target =
        CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        
        [self.mapView animateToLocation:target];
        [self.mapView animateToZoom:17];
        
    }
    
    [self.mapView removeObserver:self forKeyPath:@"myLocation"];
}


-(void)handelHystoryButtonPress {
    NSLog(@"handelHystoryButtonPress!!!!");
    [self performSegueWithIdentifier:@"goToHistory" sender:nil];
}
-(void)handelHystoryButtonPressForBlackView {
    NSLog(@"handelHystoryButtonPressForBlackView!!!!!!!!!");
    //self.mVisualEffectView.hidden = NO;
    [ProgressHUD show:@"Please wait..."];
}
-(void)handelPromotionButtonPress {
    NSLog(@"handelPromotionButtonPress!!!!");
    [self performSegueWithIdentifier:@"goToPromotion" sender:nil];
}
-(void)handelPromotionButtonPressForBlackView {
    NSLog(@"handelPromotionButtonPressForBlackView!!!!!!!!!");
    //self.mVisualEffectView.hidden = NO;
    [ProgressHUD show:@"Please wait..."];
}

-(void)getAllRepaiers {
    self.nearestRepairesLocationsArray = [[NSMutableArray alloc]init];
    GetnearestsRequest * mGetnearestsRequest = [[GetnearestsRequest alloc] initWithApiKey:_userAccount.apiKey];
    
    [mGetnearestsRequest postWithSuccess:^(NSDictionary *response) {
        NSLog(@"response : %@",response);
        NSError * myError;
        NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&myError];
        self.nearestRepairesLocationsArray = (NSMutableArray*)[dictionary objectForKey:@"message"];
        NSLog(@"nearestRepairesLocationsArray : %@",self.nearestRepairesLocationsArray);
        repaiersCount = self.nearestRepairesLocationsArray.count;
        
        [_mapView clear];
        
        if (self.nearestRepairesLocationsArray.count > 0) {
            for (int i = 0; i < self.nearestRepairesLocationsArray.count; i++) {
                //[self performSelectorInBackground:@selector(addRepaiersWithIndex:) withObject:[NSNumber numberWithInt:i]];
                //[self.mMapView showAnnotations:self.mMapView.annotations animated:YES];
                //[self recenterMapToPlacemarkForRepaiers:CLLocationCoordinate2DMake([[[mServiceArray objectAtIndex:i] valueForKey:@"lat"] doubleValue], [[[mServiceArray objectAtIndex:i] valueForKey:@"long"] doubleValue])];
                
                
                [self addRepairAnnotationWithLatitude:[[[self.nearestRepairesLocationsArray objectAtIndex:i] valueForKey:@"lat"] doubleValue]
                                         andLongitude:[[[self.nearestRepairesLocationsArray objectAtIndex:i] valueForKey:@"long"] doubleValue]];
            }
            //[self showAllMarkers:markersArray];
            
            // [self.mMapView showAnnotations:self.mMapView.annotations animated:YES];
            
        }
        countRepair = 0;
        // [self addTime];
        [self showAllMarkers:markersArray];
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"error : %@",error.description);
        
    }];

}

-(void)handelLogOutButtonPress {
    [locationManager stopUpdatingLocation];
    [repaierListUpdateTimer invalidate];
}
-(void)handelPaymentButtonPress {
    NSLog(@"handelPromotionButtonPress!!!!");
    [repaierListUpdateTimer invalidate];
    
    [locationManager stopUpdatingLocation];
    
    if([self checkInternetConnection]){
        GetPaymantCardsRequest * mGetPaymantCardsRequest = [[GetPaymantCardsRequest alloc] initWithApiKey:_userAccount.apiKey withCustomerId:_customerAccount.UserId];
        
        [mGetPaymantCardsRequest postWithSuccess:^(NSDictionary *response) {
            NSLog(@"response = %@",response);
            NSError * mError;
            NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *addressListDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&mError];
            NSMutableArray* cardListArray = (NSMutableArray*)[addressListDictionary objectForKey:@"message"];
            
            
            RLMRealm *realm = [RLMRealm defaultRealm];
            RLMResults<CardDetails *> *mCardDetails = [CardDetails allObjects];
            NSArray * mCardDetailsArray = (NSArray*)mCardDetails;
            
            if (mCardDetailsArray.count > 0) {
                for (int i = 0; i < [mCardDetailsArray count]; i++) {
                    [realm beginWriteTransaction];
                    [realm deleteObject:[mCardDetailsArray objectAtIndex:i]];
                    [realm commitWriteTransaction];
                }
            }
            
            
            for (int i = 0; i < cardListArray.count; i++) {
                
                NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[cardListArray objectAtIndex:i] valueForKey:@"card_logo"]]];
                
                CardDetails * mCardDetails = [[CardDetails alloc] initWithRestAPIResponse:[cardListArray objectAtIndex:i] imageData:imageData];
                [mCardDetails createOrUpdateCustomerAccountInStorage];
            }
            
            
            [self performSegueWithIdentifier:@"goToPaymant" sender:nil];
            
        } failure:^(NSError *error, NSInteger responseCode) {
            NSLog(@"error = %@",error.description);
        }];
        

    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }

    
    
}
-(void)handelPaymentButtonPressForBlackView {
    NSLog(@"handelPromotionButtonPressForBlackView!!!!!!!!!");
    //self.mVisualEffectView.hidden = NO;
    [ProgressHUD show:@"Please wait..."];
}


-(void)handelAcceptNotification:(NSNotification*) notification {
    NSLog(@"accept!!!!!!1");
     [repaierListUpdateTimer invalidate];
    
    [locationManager stopUpdatingLocation];
    
   
    
    isAccepted = YES;
    isDone = NO;
    CustCount = 0;
    cout = 0;
   
    
    
    self.mSearchBar.userInteractionEnabled = NO;
    //[self.mMapView removeAnnotations:self.mMapView.annotations];
    self.mRepaierDetailsView.hidden = NO;
    self.mRepaierNameLbl.hidden = NO;
    self.mRepaierPhoneBtn.hidden = NO;
    self.mAllButtomViews.hidden = YES;
    [repaiersAnnotations removeAllObjects];
    [customersAnnotations removeAllObjects];
    [self recenterMapToPlacemark:CLLocationCoordinate2DMake([mLatitude doubleValue], [mLongitute doubleValue])]; //new
    
    appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [self showAlertWithMessage:@"Info!" andText:appObj.alertMessage];
    [self.mRepaierPhoneBtn setTitle:[appObj.AcceptNotData valueForKey:@"repairer_phone"] forState:UIControlStateNormal];
    self.mRepaierNameLbl.text = [NSString stringWithFormat:@"%@ %@",[appObj.AcceptNotData valueForKey:@"repairer_name"],[appObj.AcceptNotData valueForKey:@"repairer_surname"]];
    [self.view layoutIfNeeded];
    NSString * phone = [appObj.AcceptNotData valueForKey:@"repairer_phone"];
    NSString * name = [NSString stringWithFormat:@"%@ %@",[appObj.AcceptNotData valueForKey:@"repairer_name"],[appObj.AcceptNotData valueForKey:@"repairer_surname"]];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAccepted"];
    [[NSUserDefaults standardUserDefaults] setObject:phone forKey:@"repaierPhone"];
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"repaierName"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"validOrder"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if([self checkInternetConnection]){
        [self moveRepairer:NO];
    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[appObj.AcceptNotData valueForKey:@"repairer_lat"] forKey:@"repaierLat"];
    [[NSUserDefaults standardUserDefaults] setObject:[appObj.AcceptNotData valueForKey:@"repairer_lng"] forKey:@"repaierLong"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%f",_customerLocation.latitude] forKey:@"customerLat"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%f",_customerLocation.longitude] forKey:@"customerLong"];
    
    
    [_mapView clear];
    oldLocation = CLLocationCoordinate2DMake([[appObj.AcceptNotData valueForKey:@"repairer_lat"] doubleValue], [[appObj.AcceptNotData valueForKey:@"repairer_lng"] doubleValue]);
    [self addRepairAnnotationWithLatitude:[[appObj.AcceptNotData valueForKey:@"repairer_lat"] doubleValue] andLongitude:[[appObj.AcceptNotData valueForKey:@"repairer_lng"] doubleValue]];
    //_mapView.selectedMarker = _repaierMarker;
   // _customerLocation
    [self getAdressFromCordinate:oldLocation];
    [self addCustomerWithLatitute:_customerLocation.latitude andLongitute:_customerLocation.longitude];
    
    [self showAllMarkers:[NSMutableArray arrayWithObjects:_repaierMarker,_mCustomerMarker, nil]];
    
    
    _mapView.myLocationEnabled = NO;
    myTimer = [NSTimer scheduledTimerWithTimeInterval:7.0 target:self
                                             selector:@selector(move) userInfo:nil repeats:YES];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mSearchBtn.hidden = YES;
        self.mSquarPoint.hidden = YES;
        self.infoVindowImageView.hidden = YES;
        self.mPinImageView.hidden = YES;
        //self.mVisualEffectView.hidden = YES;
        [ProgressHUD dismiss];
         //_mapView.selectedMarker = _repaierMarker;
        
});
  
    self.showRepaierListBtn.hidden = NO;
    self.closeRepaierListBnt.hidden = YES;
    self.menuBtnImageView.image = [UIImage imageNamed:@"ic_menu"];
    self.mMapViewBottomConstraint.constant = 0;
    self.mButtomViewHeight.constant = -(234);
    [self.mAllButtomViews layoutIfNeeded];
    self.mSearchBtn.hidden = NO;
    self.mSquarPoint.hidden = NO;

    CGRect newFrameForMap1 = self.mapView.frame;
    newFrameForMap1.size.height = self.view.frame.size.height - self.mRepaierDetailsView.frame.size.height;
    self.mViewForMap.frame = newFrameForMap1;
    self.mapView.frame = newFrameForMap1;
    [self.mViewForMap layoutIfNeeded];
    [self.mapView layoutIfNeeded];
    [self.view layoutIfNeeded];
 
    self.mSearchBtn.hidden = YES;
    self.mSquarPoint.hidden = YES;
    
    [self performSelector:@selector(closeCurentCircle) withObject:nil afterDelay:1];

}

-(void)closeCurentCircle {
    _circCertr.map = nil;
    _circCertr = nil;
    _circ.map = nil;
    _circ = nil;
    
     //_mapView.selectedMarker = _repaierMarker;
}
-(void)move {
    if([self checkInternetConnection]){
    
         [self moveRepairer:YES];
    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }
    

   
}

-(void)handelDoneNotification:(NSNotification*) notification {
    NSLog(@"DONE!!!!!!!");
    [myTimer invalidate];
    myTimer = nil;
    isDone = YES;
    isAccepted = NO;
    CustCount = 0;
    cout = 0;
    isFirstTime = NO;
    
    isOpened = NO;
    isFirstTime = NO;
    isFinishedRenderMap = NO;
    secectedIndex = 1;
    repaiersCount = 0;

    [repaiersAnnotations removeAllObjects];
    [customersAnnotations removeAllObjects];
    repaiersAnnotations = [[NSMutableDictionary alloc] init];
    customersAnnotations = [[NSMutableDictionary alloc] init];
    
    [UIView setAnimationsEnabled:NO];
    [self.view.layer removeAllAnimations];
    
    self.mSearchBar.userInteractionEnabled = YES;
    appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [self showAlertWithMessage:@"Info!" andText:appObj.alertMessage];
    self.mRepaierDetailsView.hidden = YES;
    self.mRepaierNameLbl.hidden = YES;
    self.mRepaierPhoneBtn.hidden = YES;
    self.mAllButtomViews.hidden = NO;
    //[self.mMapView showAnnotations:self.mMapView.annotations animated:YES];
    [self.mMapView removeAnnotations:self.mMapView.annotations];
     [self.view layoutIfNeeded];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isAccepted"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"validOrder"];
    [[NSUserDefaults standardUserDefaults] synchronize];
 
    BOOL isassfd = [[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"];
     //[self updateMap];
    [self performSelector:@selector(updateMap) withObject:nil afterDelay:5];
    self.mTotalLbl.text = @"$0";
    [self.view layoutIfNeeded];
   
    [_mapView clear];
    [self updateGoogleMap];
    [locationManager startUpdatingLocation];
    
    //_mapView.myLocationEnabled = YES;
    if (_circ && _circCertr) {
        [_circ beginRadiusAnimationFrom:_radius to:_radius *100 duration:1 completeHandler:^{
            
        }];
    }else{
        
        CLLocationCoordinate2D circleCenter = CLLocationCoordinate2DMake([mLatitude doubleValue], [mLongitute doubleValue]);
        
        _circCertr = [TAMapCircle circleWithPosition:circleCenter radius:_radius];
        _circCertr.fillColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:1];
        _circCertr.strokeColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:1];
        _circCertr.strokeWidth = 1.5f;
        
        _circCertr.map = _mapView;
        
        
        _circ = [TAMapCircle circleWithPosition:circleCenter radius:_radius * 1000];
        _circ.fillColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:0.1f];
        _circ.strokeColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:0.5f];
        _circ.strokeWidth = 1.5f;
        
        _circ.map = _mapView;
        [_circ beginRadiusAnimationFrom:_radius to:_radius * 100 duration:1 completeHandler:^{
            
        }];
    }

    
    self.mSearchBtn.hidden = YES;
    self.mSquarPoint.hidden = YES;
    self.infoVindowImageView.hidden = NO;
    self.mPinImageView.hidden = NO;
    [self.view layoutIfNeeded];
    //self.mVisualEffectView.hidden = YES;
    [ProgressHUD dismiss];
    self.showRepaierListBtn.hidden = NO;
    self.closeRepaierListBnt.hidden = YES;
    self.menuBtnImageView.image = [UIImage imageNamed:@"ic_menu"];
    self.mMapViewBottomConstraint.constant = 0;
    self.mButtomViewHeight.constant = -(234);
    [self.mAllButtomViews layoutIfNeeded];
    self.mSearchBtn.hidden = NO;
    self.mSquarPoint.hidden = NO;
    
    CGRect newFrameForMap1 = self.mapView.frame;
    newFrameForMap1.size.height = self.view.frame.size.height;
    self.mViewForMap.frame = newFrameForMap1;
    self.mapView.frame = newFrameForMap1;
    [self.mViewForMap layoutIfNeeded];
    [self.mapView layoutIfNeeded];
    [self.view layoutIfNeeded];
    
    [_mapView.settings setAllGesturesEnabled:YES];

    _mapView.selectedMarker = nil;
    [myTimer invalidate];
    myTimer = nil;
}

-(void)updateGoogleMap {
    
    [self Getnearests];
    [_mapView reloadInputViews];
}

#pragma mark --UITextFieldDelegate


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    NSLog(@"textFieldShouldBeginEditing");
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    
    //self.mBottomTableView.hidden = YES;
//    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
//    acController.delegate = self;
//[self presentViewController:acController animated:YES completion:nil];
//    _acTableView = [[GMSAutocompleteTableDataSource alloc] init];
//    _acTableView.delegate = self;
    
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason {
    
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
      return YES;
}

#pragma mark SearchTextDidChanged

-(void)searchTextDidChanged :(NSNotification*) notification{
    
    if (!isLoadPlaceAddress) {
        isLoadPlaceAddress = YES;
        NSString* searchText = _mSearchTextField.text;
        if([searchText isEqualToString:@""]){
            _placeNames = [[NSMutableArray alloc]init];
            [self.mBottomTableView reloadData];
            isLoadPlaceAddress = NO;
        }else{
            _placeNames = [[NSMutableArray alloc]init];
            GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
            filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter;
            
            [_placeClient autocompleteQuery:searchText
                                     bounds:nil
                                     filter:filter
                                   callback:^(NSArray *results, NSError *error) {
                                       if (error != nil) {
                                           NSLog(@"Autocomplete error %@", [error localizedDescription]);
                                           return;
                                       }else {
                                           NSLog(@"resulte = %@",results);
                                       }
                                       
                                       for (GMSAutocompletePrediction* result in results) {
                                           PlaceObject * mPlaceObject = [[PlaceObject alloc] init];
                                           mPlaceObject.primaryAddress = result.attributedPrimaryText.string;
                                           mPlaceObject.secondaryAddress = result.attributedSecondaryText.string;
                                           NSLog(@"PrimaryText = %@ SecondaryText = %@", result.attributedPrimaryText.string,result.attributedSecondaryText.string);
                                           
                                           [_placeNames addObject:mPlaceObject];
                                       }
                                       [self.mBottomTableView reloadData];
                                       isLoadPlaceAddress = NO;
                                   }];
            
        }

    }
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    
    return NO;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSLog(@"================== textFieldShouldReturn ================");
    
    
    [self.mSearchTextField resignFirstResponder];
    [self.mSearchText resignFirstResponder];
    return YES;
}
-(void)updateMap {
    [myTimer invalidate];
    myTimer = nil;
    NSLog(@"=======================================================   TIMER = %@",myTimer);
    isOpened = NO;
    isAccepted = NO;
    isFirstTime = NO;
    isFinishedRenderMap = NO;
    secectedIndex = 1;
    repaiersCount = 0;
    CustCount = 0;
    cout = 0;
    [repaiersAnnotations removeAllObjects];
    [customersAnnotations removeAllObjects];
    repaiersAnnotations = [[NSMutableDictionary alloc] init];
    customersAnnotations = [[NSMutableDictionary alloc] init];
    
    [UIView setAnimationsEnabled:NO];
    [self.view.layer removeAllAnimations];
    [self.mMapView removeAnnotations:self.mMapView.annotations];

    if([self checkInternetConnection]){
        [self Getnearests];
    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }

    
}


-(void)initSPGooglePlacesAutocompleteQuery {
    _query = [SPGooglePlacesAutocompleteQuery query];
    //_query.input = @"Armenia  gyumri Te";
    //_query.radius = 1000.0;
    _query.language = @"en";
    _query.types = SPPlaceTypeGeocode;
   // _query.location = CLLocationCoordinate2DMake(37.76999, -122.44696);
    
}
-(void)rel {
    self.dropDawn = nil;
    self.mSearchBarTopConstraint.constant = 55;
    [self.navigationController setNavigationBarHidden: NO animated:YES];
    [self.mSearchBar resignFirstResponder];
}
- (void) dropDownDelegateMethod: (DropDownView *) sender{
    self.mSearchBarTopConstraint.constant = 55;
    [self.navigationController setNavigationBarHidden: NO animated:YES];
    appObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.mSearchBar.text = [_placeNames objectAtIndex:appObj.selectedIndex];
    if([self checkInternetConnection]){
        [self changeMapToSelectedDropDownAddress:appObj.selectedIndex];
    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }

    
    [self rel];
}

-(void)changeMapToSelectedDropDownAddress:(NSInteger)index {
SPGooglePlacesAutocompletePlace *place = [_searchResultPlaces objectAtIndex:index];
[place resolveToPlacemark:^(CLPlacemark *placemark, NSString *addressString, NSError *error) {
    CustCount = 0;
    isFirstTime = NO;
    [self geocoderMethod:placemark.location.coordinate.latitude longitude:placemark.location.coordinate.longitude];
    [self changeMapCentrerCoordinatesBy:CLLocationCoordinate2DMake(placemark.location.coordinate.latitude, placemark.location.coordinate.longitude)];
    if (error) {
        SPPresentAlertViewWithErrorAndTitle(error, @"Could not map selected Place");
    } else if (placemark) {
        if ([self.mMapView annotations].count > 0) {
             [self.mMapView removeAnnotation:CustomerAnnotation];
        }
        
        CLLocation *customerLocation = [[CLLocation alloc] initWithLatitude:placemark.location.coordinate.latitude longitude:placemark.location.coordinate.longitude];
        CLLocation *repaierLocation;
        CLLocationDistance distance;
        double rLatitute;
        double rLongitute;
        
        mArrayWithDistanc = [[NSMutableArray alloc] init];
        mArrayWithNearRepaiers = (NSMutableArray*)mServiceArray;
        NSLog(@"mServiceArray : %@",mServiceArray);
        if (mArrayWithNearRepaiers.count > 0) {
            for (int i = 0; i < mArrayWithNearRepaiers.count; i++) {
                rLatitute = [[[mArrayWithNearRepaiers objectAtIndex:i] valueForKey:@"lat"] doubleValue];
                rLongitute = [[[mArrayWithNearRepaiers objectAtIndex:i] valueForKey:@"long"] doubleValue];
                repaierLocation = [[CLLocation alloc] initWithLatitude:rLatitute longitude:rLongitute];
                distance = [customerLocation distanceFromLocation:repaierLocation];
                [mArrayWithDistanc addObject:[NSNumber numberWithDouble:distance]];
                
            }
            
            NSNumber* min = [mArrayWithDistanc valueForKeyPath:@"@min.self"];
            NSLog(@"mArrayWithDistanc = %@",mArrayWithDistanc);
            NSLog(@"MIN = %f",[min doubleValue]);
            
            for (int j = 0; j < mArrayWithNearRepaiers.count; j++) {
                if ([[mArrayWithDistanc objectAtIndex:j] doubleValue] == [min doubleValue]) {
                    rLatitute = [[[mArrayWithNearRepaiers objectAtIndex:j] valueForKey:@"lat"] doubleValue];
                    rLongitute = [[[mArrayWithNearRepaiers objectAtIndex:j] valueForKey:@"long"] doubleValue];
                    mLatitude = [NSString stringWithFormat:@"%f",placemark.location.coordinate.latitude];
                    mLongitute =[NSString stringWithFormat:@"%f", placemark.location.coordinate.longitude];
                    GetDistanceTimeRequest * mGetDistanceTimeRequest = [[GetDistanceTimeRequest alloc] initWithCustomerLocation:CLLocationCoordinate2DMake([self.mMapView centerCoordinate].latitude, [self.mMapView centerCoordinate].longitude) andRepaierLocation:CLLocationCoordinate2DMake(rLatitute,rLongitute )];
                    
                    [mGetDistanceTimeRequest getWithSuccess:^(NSObject *response) {
                        NSLog(@"response : %@",response);
                        
                        NSDictionary * dictionary = (NSDictionary*)response;
                        
                        nearTimeStr = [[[[[[dictionary valueForKey:@"rows"] objectAtIndex:0] valueForKey:@"elements"] objectAtIndex:0] valueForKey:@"duration"] valueForKey:@"text"];
                        NSLog(@"nearTimeStr = %@",nearTimeStr);
                        [ProgressHUD dismiss];
                        //self.mVisualEffectView.hidden = YES;
                        self.mCustomerTime.text = nearTimeStr;
                        [self recenterMapToPlacemark:placemark.location.coordinate];
                        [self geocoderMethod:placemark.location.coordinate.latitude longitude:placemark.location.coordinate.longitude];
                        [self changeMapCentrerCoordinatesBy:CLLocationCoordinate2DMake(placemark.location.coordinate.latitude, placemark.location.coordinate.longitude)];
                    } failure:^(NSError *error, NSInteger responseCode) {
                        //self.mVisualEffectView.hidden = YES;
                        [ProgressHUD dismiss];
                        NSLog(@"error : %@",error.description);
                    }];
                    

                    
                }
            }
            
            
            
        }
        

       
        

//        [self addPlacemarkAnnotationToMap:placemark addressString:addressString];
//        [self recenterMapToPlacemark:placemark];
//        [self dismissSearchControllerWhileStayingActive];
//        [self.searchDisplayController.searchResultsTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}];
}
- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSInteger)indexPath {
    return [_searchResultPlaces objectAtIndex:indexPath];
}



-(void)getNearRepaierTime{
    
    
    CLLocationCoordinate2D cord = [_mapView.projection coordinateForPoint:_mapView.center];
     CLLocation *customerLocation = [[CLLocation alloc] initWithLatitude:cord.latitude longitude:cord.longitude];
     CLLocation *repaierLocation;
     CLLocationDistance distance;
     double rLatitute;
     double rLongitute;
    
    mArrayWithDistanc = [[NSMutableArray alloc] init];
        mArrayWithNearRepaiers = (NSMutableArray*)self.nearestRepairesLocationsArray;
        NSLog(@"mServiceArray : %@",mServiceArray);
                if (mArrayWithNearRepaiers.count > 0) {
                    for (int i = 0; i < mArrayWithNearRepaiers.count; i++) {
                        rLatitute = [[[mArrayWithNearRepaiers objectAtIndex:i] valueForKey:@"lat"] doubleValue];
                        rLongitute = [[[mArrayWithNearRepaiers objectAtIndex:i] valueForKey:@"long"] doubleValue];
                        repaierLocation = [[CLLocation alloc] initWithLatitude:rLatitute longitude:rLongitute];
                        distance = [customerLocation distanceFromLocation:repaierLocation];
                        [mArrayWithDistanc addObject:[NSNumber numberWithDouble:distance]];
                        
                    }
        
                      NSNumber* min = [mArrayWithDistanc valueForKeyPath:@"@min.self"];
                    NSLog(@"mArrayWithDistanc = %@",mArrayWithDistanc);
                    NSLog(@"MIN = %f",[min doubleValue]);

                    for (int j = 0; j < mArrayWithNearRepaiers.count; j++) {
                        if ([[mArrayWithDistanc objectAtIndex:j] doubleValue] == [min doubleValue]) {
                            rLatitute = [[[mArrayWithNearRepaiers objectAtIndex:j] valueForKey:@"lat"] doubleValue];
                            rLongitute = [[[mArrayWithNearRepaiers objectAtIndex:j] valueForKey:@"long"] doubleValue];
                            
                            [self getTimeWithLat:rLatitute andWithLong:rLongitute];
                            
                        }
                    }
                    
                    
                    
                }
       
        
        
  
    
//    CLLocation *customerLocation = [[CLLocation alloc] initWithLatitude:[mLatitude doubleValue] longitude:[mLongitute doubleValue]];
//    CLLocation *locB = [[CLLocation alloc] initWithLatitude:lat2 longitude:long2];
//    CLLocationDistance distance = [customerLocation distanceFromLocation:locB];
}

-(void)getTimeWithLat:(double)lat andLong:(double)lng {
    GetDistanceTimeRequest * mGetDistanceTimeRequest = [[GetDistanceTimeRequest alloc] initWithCustomerLocation:CLLocationCoordinate2DMake([self.mMapView centerCoordinate].latitude, [self.mMapView centerCoordinate].longitude) andRepaierLocation:CLLocationCoordinate2DMake(lat, lng)];
    
    [mGetDistanceTimeRequest getWithSuccess:^(NSObject *response) {
        NSLog(@"response : %@",response);
        
        NSDictionary * dictionary = (NSDictionary*)response;
        
        nearTimeStr = [[[[[[dictionary valueForKey:@"rows"] objectAtIndex:0] valueForKey:@"elements"] objectAtIndex:0] valueForKey:@"duration"] valueForKey:@"text"];
        timeLbl.text = nearTimeStr;
        
        NSLog(@"nearTimeStr = %@",nearTimeStr);
        [ProgressHUD dismiss];
        //self.mVisualEffectView.hidden = YES;
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"error : %@",error.description);
        //self.mVisualEffectView.hidden = YES;
        [ProgressHUD dismiss];
    }];
    

}
-(void)getTimeWithLat:(double)latitute andWithLong:(double)longitute{
    CLLocationCoordinate2D cord = [_mapView.projection coordinateForPoint:_mapView.center];
    GetDistanceTimeRequest * mGetDistanceTimeRequest = [[GetDistanceTimeRequest alloc] initWithCustomerLocation:cord andRepaierLocation:CLLocationCoordinate2DMake(latitute, longitute)];
    [self addInfoView];
    [mGetDistanceTimeRequest getWithSuccess:^(NSObject *response) {
        NSLog(@"response : %@",response);
        
        NSDictionary * dictionary = (NSDictionary*)response;
        
        nearTimeStr = [[[[[[dictionary valueForKey:@"rows"] objectAtIndex:0] valueForKey:@"elements"] objectAtIndex:0] valueForKey:@"duration"] valueForKey:@"text"];
        
        NSLog(@"nearTimeStr = %@",nearTimeStr);
        if (nearTimeStr) {
            timeLbl.text = nearTimeStr;
        }else{
            timeLbl.text = @"Unavailable!";
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [clockArrow stopAnimating];
        });
        
       // self.mVisualEffectView.hidden = YES;
        self.mCustomerTime.text = nearTimeStr;
        [self.view setNeedsDisplay];
        
//        if (isOpenedBottomView) {
//            self.mButtomViewHeight.constant = 0;
//        }else {
//            self.mButtomViewHeight.constant = -172;
//        }
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"]) {
        }else{

        }
        
    } failure:^(NSError *error, NSInteger responseCode) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [clockArrow stopAnimating];
        });

        [ProgressHUD dismiss];
        //self.mVisualEffectView.hidden = YES;
        NSLog(@"error : %@",error.description);
    }];

}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
   // self.mSearchBarTopConstraint.constant = 15;
    //self.mButtomViewHeight.constant = 0;
   // [self.mSearchBar layoutIfNeeded];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    return YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if([searchText isEqualToString:@""]){
        _placeNames = [[NSMutableArray alloc]init];
        [self.mBottomTableView reloadData];
    }else{
    _query.input = searchText;
    [_query fetchPlaces:^(NSArray *places, NSError *error) {
        NSLog(@"Places returned %@", places);
        _searchResultPlaces = places.mutableCopy;
        _placeNames = [[NSMutableArray alloc]init];
        NSString* addressString = @"";
        if(places.count != 0){
        for(int i = 0; i< places.count; i++){
            SPGooglePlacesAutocompletePlace* s =  [self placeAtIndexPath:i];
            NSArray * array = [s.name componentsSeparatedByString:@","];
            if (array.count >=3) {
                for (int i = 0; i < array.count -2; i++) {
                   addressString =  [addressString stringByAppendingString:[array objectAtIndex:i]];
                }
                [_placeNames addObject:addressString];
            }else {
            [_placeNames addObject:s.name];
            }
        }
            [self.mBottomTableView reloadData];
        }
    
        //  NSIndexPath* pat = [NSIndexPath indexPathForRow:0 inSection:1];
        if(places.count != 0 || searchBar.text.length > 0){
            [self.mBottomTableView reloadData];
            if(_dropDawn == nil){
                CGFloat f ;
                if(_placeNames.count > 5){
                    f = 5 * 40.f;
                }else{
                    f = _placeNames.count * 40.f;
                }
                
                self.dropDawn = [[DropDownView alloc]showDropDown:self.mButton
                                                           height:&f
                                                            array:_placeNames
                                                        direction:@"down"];
                self.dropDawn.delegate = self;
            }else{
                [self.dropDawn reloadData: _placeNames];
            }
            // SPGooglePlacesAutocompletePlace* s =  [self placeAtIndexPath:pat];
            // NSLog(@"Places returned %@", s.name);
            
        }
        else{
            self.mSearchBarTopConstraint.constant = 55;
            [self.view layoutIfNeeded];
            [self.dropDawn hideDropDown:self.mButton];
            [self rel];
        }
    }];
    
    }
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        if (_placeNames.count > 0 && self.dropDawn != nil) {
             [self.mSearchBar resignFirstResponder];
        }else{
            self.mSearchBarTopConstraint.constant = 55;
            //[self.navigationController setNavigationBarHidden: NO animated:YES];
             [self.mSearchBar resignFirstResponder];
        }
       
       
    }
    if(self.mSearchBar.text.length == 0 && [text isEqualToString:@""]) {
        self.mSearchBarTopConstraint.constant = 55;
        //[self.navigationController setNavigationBarHidden: NO animated:YES];
        [self.mSearchBar resignFirstResponder];
    }else {
        
    }
    return YES;
}





-(void)getDevicesWithBrandId:(NSString*)brandId {
//    RLMResults<UserAccount*>* mUserAccount = [UserAccount allObjects];
//    _userAccount = [mUserAccount firstObject];
    
    GetBrandFiltr * mGetBrandFiltr = [[GetBrandFiltr alloc] initWithApiKey:[_userAccount valueForKey:@"apiKey"] andBrandId:brandId];
    
    [mGetBrandFiltr postWithSuccess:^(NSDictionary *response) {
        NSLog(@"response : %@",response);
        NSError * error;
        NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        mDeviceArray = (NSMutableArray*)[dictionary objectForKey:@"message"];
        [_mDeviceTypeCollectionView reloadData];
        
    } failure:^(NSError *error, NSInteger responseCode) {
         NSLog(@"error :%@",error.description);
    }];
    //ServiceItemsDetails
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    if (collectionView == self.repairCollectionView) {
        return 10;
    }else {
    return 0;
    }
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == _mDeviceTypeCollectionView) {
        return newServicesArray.count;
    }
    if(collectionView == _repairCollectionView){
        return mServiceArrayForTableView.count;
    }
    
    else {
        return mBrandsArray.count;
    }
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView == _repairCollectionView){
        RepairCollectionViewCell* cell = (RepairCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"repairCell" forIndexPath:indexPath];
        cell.rep_nameLabel.text = [[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.rep_priceLabel.text = [NSString stringWithFormat:@"$%@",[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"Price"]];
        cell.isSelected = NO;
        cell.imageBackgroundView.layer.cornerRadius = cell.imageBackgroundView.frame.size.height/2;
        cell.imageBackgroundView.layer.masksToBounds = YES;
        
        cell.rep_Image.image = [UIImage imageWithData:[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"imageData"]];
        
        if (arrayWithServicesId.count > 0) {
            if ([arrayWithServicesId containsObject:[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"ItemId"]]) {
                cell.imageBackgroundView.backgroundColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:1.f];
                cell.isSelected = YES;
            }else {
                cell.imageBackgroundView.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1.f];
                cell.isSelected = NO;
            }
        }else{
            cell.isSelected = NO;
            cell.imageBackgroundView.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1.f];
        }

        
        
        return cell;
        
           }
    
    if (collectionView == _mDeviceTypeCollectionView) {
        CustomDeviceTypeCollectionCell *cell1 = (CustomDeviceTypeCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell1" forIndexPath:indexPath];
        [cell1.mCircleImageView setImage:[UIImage imageNamed:@"klor"]];
        if (mDeviceArray.count == 1) {
            [cell1.mLineImageView setImage:[UIImage imageNamed:@""]];
            [cell1.mAjGic setImage:[UIImage imageNamed:@""]];
        }else {
        if (indexPath.row == 0) {
            [cell1.mLineImageView setImage:[UIImage imageNamed:@""]];
            [cell1.mAjGic setImage:[UIImage imageNamed:@"gic"]];
        }else if (indexPath.row == mDeviceArray.count - 1) {
            [cell1.mLineImageView setImage:[UIImage imageNamed:@"gic"]];
            [cell1.mAjGic setImage:[UIImage imageNamed:@""]];

        }else {
            [cell1.mLineImageView setImage:[UIImage imageNamed:@"gic"]];
            [cell1.mAjGic setImage:[UIImage imageNamed:@"gic"]];
        }
        
        }
        
        cell1.mDeviceName.text = [[newServicesArray objectAtIndex:indexPath.row] valueForKey:@"Name"];
        if (selectedDeviceIndex == indexPath.row) {
            cell1.mCircleImageView.image = [UIImage imageNamed:@"klorKarmir"];
        }else{
            cell1.mCircleImageView.image = [UIImage imageNamed:@"klor"];
        }
        return cell1;

    }else {
        
        static int i = 0;
    CustomBrandCollectionViewCell *cell = (CustomBrandCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        [cell addImage];
       
        NSLog(@"mBrandsArray = %@",mBrandsArray);
        
        
        //brandImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
        NSInteger f=indexPath.row;
        if (indexPath.row == secectedIndex) {
            cell.mBrandImageView.image = nil;
           // [cell.mBrandImageView setImage:[arrayWithImagesRed objectAtIndex:indexPath.row]];
            [cell.mBrandImageView setImage:[UIImage imageWithData:
                                            [NSData dataWithContentsOfURL:
                                             [NSURL URLWithString:[[mBrandsArray objectAtIndex:indexPath.row] valueForKey:@"Logo_Red"]]]]];
            
        }else
        {
            cell.mBrandImageView.image = nil;
           // [cell.mBrandImageView setImage:[arrayWithImagesBleck objectAtIndex:indexPath.row]];
            [cell.mBrandImageView setImage:[UIImage imageWithData:
                                  [NSData dataWithContentsOfURL:
                                   [NSURL URLWithString:[[mBrandsArray objectAtIndex:indexPath.row] valueForKey:@"Logo"]]]]];

        }
        //cell.backgroundColor = [UIColor redColor];
         //[cell addSubview:brandImage];
        i++;
    return cell;
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    CGFloat whieght;
    //[ProgressHUD dismiss];
    if (collectionView == _mDeviceTypeCollectionView) {
        if (mDeviceArray.count == 1) {
            return UIEdgeInsetsMake(0, _mDeviceTypeCollectionView.frame.size.width/2 - DEVICE_TYPE_CELL_WIDTH_IPHONE/2, 0, 0);
        }else if (mDeviceArray.count == 2) {
            return UIEdgeInsetsMake(0, _mDeviceTypeCollectionView.frame.size.width/2 - DEVICE_TYPE_CELL_WIDTH_IPHONE, 0, 0);
        }else if (mDeviceArray.count == 3) {
            return UIEdgeInsetsMake(0, _mDeviceTypeCollectionView.frame.size.width/2 - (DEVICE_TYPE_CELL_WIDTH_IPHONE + DEVICE_TYPE_CELL_WIDTH_IPHONE/2), 0, 0);
        }else if (mDeviceArray.count == 4) {
            return UIEdgeInsetsMake(0, _mDeviceTypeCollectionView.frame.size.width/2 - (2*DEVICE_TYPE_CELL_WIDTH_IPHONE), 0, 0);
        }else {
        return UIEdgeInsetsMake(0, 0, 0, 0);
        }
    }else if(collectionView == brandCollectionView){
        CGFloat totalCellWidth;
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
              totalCellWidth = mBrandsArray.count * 80;
        }else{
              totalCellWidth = mBrandsArray.count * 40;
        }
       
        CGFloat totalSpacingWidth = 5 * (mBrandsArray.count - 1);
        CGFloat leftInset = (brandCollectionView.frame.size.width - (CGFloat)(totalCellWidth + totalSpacingWidth))/2;
        CGFloat rightInset = leftInset;
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }else if (collectionView == self.repairCollectionView) {
        if (mServiceArrayForTableView.count == 1) {
            return UIEdgeInsetsMake(0, _repairCollectionView.frame.size.width/2 - self.repairCollectionView.frame.size.height/2, 0, 0);
        }else if (mServiceArrayForTableView.count == 2) {
            return UIEdgeInsetsMake(0, _repairCollectionView.frame.size.width/2 - self.repairCollectionView.frame.size.height, 0, 0);
        }else if (mServiceArrayForTableView.count == 3) {
            return UIEdgeInsetsMake(0, _repairCollectionView.frame.size.width/2 - (self.repairCollectionView.frame.size.height + self.repairCollectionView.frame.size.height/2), 0, 0);
        }else if (mServiceArrayForTableView.count == 4) {
            return UIEdgeInsetsMake(0, _repairCollectionView.frame.size.width/2 - (2*self.repairCollectionView.frame.size.height), 0, 0);
        }else {
            return UIEdgeInsetsMake(0, 0, 0, 0);
        }

    }
    
        return UIEdgeInsetsMake(0, 0, 0, 0);
   
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == self.repairCollectionView) {
        return CGSizeMake(self.repairCollectionView.frame.size.height, self.repairCollectionView.frame.size.height);
    }
    return CGSizeMake(85, 40);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath; {
    
    if (collectionView == _mDeviceTypeCollectionView) {
        self.mTableView.hidden = NO;
//        [UIView animateWithDuration:0.5 animations:^{
//            CGRect newFrame = self.mAllButtomViews.frame;
//            newFrame.origin.y = (self.view.frame.size.height  - self.mAllButtomViews.frame.size.height);
//            self.mAllButtomViews.frame = newFrame;
//            CGRect newFrameForMap = self.mMapView.frame;
//            CGRect newFrameForMap1 = self.mapView.frame;
//            newFrameForMap.size.height = self.mAllButtomViews.frame.origin.y;
//            newFrameForMap1.size.height = self.mAllButtomViews.frame.origin.y;
//            self.mViewForMap.frame = newFrameForMap1;
//            self.mMapView.frame = newFrameForMap;
//            self.mapView.frame = newFrameForMap1;
//            [self.mMapView layoutIfNeeded];
//            [self.mViewForMap layoutIfNeeded];
//            self.mPinImageView.frame = CGRectMake(self.mPinImageView.frame.origin.x, self.mapView.frame.size.height/2 - self.mPinImageView.frame.size.height, self.mPinImageView.frame.size.width, self.mPinImageView.frame.size.height);
//            self.infoVindowImageView.frame = CGRectMake(self.infoVindowImageView.frame.origin.x, self.mPinImageView.frame.origin.y - self.infoVindowImageView.frame.size.height, self.infoVindowImageView.frame.size.width, self.infoVindowImageView.frame.size.height);
//            
//            [self.mPinImageView layoutIfNeeded];
//            [self.infoVindowImageView layoutIfNeeded];
//            
//            self.mSearchBtn.hidden = YES;
//            self.mSquarPoint.hidden = YES;
//            CGRect newFrameForPointView = self.pointView.frame;
//            newFrameForPointView.origin.y = self.mMapView.frame.size.height/2;
//            self.pointView.frame = newFrameForPointView;
//            [self.pointView layoutIfNeeded];
//            [self.view layoutIfNeeded];
//        }];
        
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            self.mButtomViewHeight.constant = 0;
        }else{
            self.mButtomViewHeight.constant = 0;
        }
        CustomDeviceTypeCollectionCell *datasetCell =(CustomDeviceTypeCollectionCell*)[collectionView cellForItemAtIndexPath:indexPath];
        
//        if (datasetCell.mCircleImageView.tag == 100) {
//            datasetCell.tag = indexPath.row;
//        }else{
//        datasetCell.mCircleImageView.image = [UIImage imageNamed:@"klorKarmir"];
//        datasetCell.tag = 100;
//        }
        isOpenedBottomView = YES;
        selectedDeviceIndex = indexPath.row;
        [self.mDeviceTypeCollectionView reloadData];
        [self getServicesWithIndex:indexPath.row];
    }else {
        
        [self updateCollectionView:collectionView withindex:indexPath];
        
//        selectedDeviceIndex = -1;
//        secectedIndex = indexPath.row;
//        
//        if([self checkInternetConnection]){
//           [self getDevicesWithBrandId:[[mBrandsArray objectAtIndex:indexPath.row] valueForKey:@"BrandId"]];
//        }else{
//            [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
//        }
//
//        
//        [UIView setAnimationDuration:10];
//        //[brandCollectionView reloadData];
//        for (int i = 0; i < mBrandsArray.count ; i++){
//            NSIndexPath* ip= [[NSIndexPath alloc]initWithIndex:i];
//            NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:0];
//            CustomBrandCollectionViewCell * cell2 =(CustomBrandCollectionViewCell*) [collectionView cellForItemAtIndexPath:path];
//
//            if(i == indexPath.row){
//                //cell2.mBrandImageView.image = [arrayWithImagesRed objectAtIndex:indexPath.row];
//                [cell2.mBrandImageView setImage:[UIImage imageWithData:
//                                                [NSData dataWithContentsOfURL:
//                                                 [NSURL URLWithString:[[mBrandsArray objectAtIndex:indexPath.row] valueForKey:@"Logo_Red"]]]]];
//            }else{
//                 //cell2.mBrandImageView.image = [arrayWithImagesBleck objectAtIndex:i];
//                [cell2.mBrandImageView setImage:[UIImage imageWithData:
//                                                [NSData dataWithContentsOfURL:
//                                                 [NSURL URLWithString:[[mBrandsArray objectAtIndex:i] valueForKey:@"Logo"]]]]];
//            }
//        //[brandCollectionView reloadData];
//        self.mButtomViewHeight.constant = 0;
//        }
//        [UIView commitAnimations];    }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.mTableView) {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        return 30;
    }else {
        return 60;
    }
    }else{
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
            return 50;
        }else {
            return 100;
        }
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.mTableView) {
        if (mServiceArrayForTableView.count > 0 ) {
            return mServiceArrayForTableView.count;
        }else {
            return 1;
        }
    }else{
        if (addressListArray.count > 0 && [_mSearchTextField.text  isEqualToString: @""]) {
            return addressListArray.count;
        }else {
            return _placeNames.count;
        }
        
    }
    
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.mTableView){
    OrderListTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[OrderListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    if (arrayWithServicesId.count > 0) {
        if ([arrayWithServicesId containsObject:[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"ItemId"]]) {
            cell.mChackbox.image = [UIImage imageNamed:@"ic_red"];
        }else {
            cell.mChackbox.image = [UIImage imageNamed:@"ic_square"];
        }
    }else{
         cell.mChackbox.image = [UIImage imageNamed:@"ic_square"];
    }
    
    
//    if ([arrayWithServicesId containsObject:[[mServiceArray objectAtIndex:indexPath.row] valueForKey:@"id"]]) {
//        cell.mChackbox.image = [UIImage imageNamed:@"ic_red"];
//    }else {
//        cell.mChackbox.image = [UIImage imageNamed:@"ic_square"];
//    }
    if (mServiceArrayForTableView.count > 0) {
        cell.mItemNameLbl.text = [[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"Name"];
        cell.mPriceLbl.text = [NSString stringWithFormat:@"$%@",[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"Price"]];
    }

    return cell;
        
    }
//    else if(tableView == self.mBottomTableView){
//        
//        CustomAdressTableViewCell* addressCell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell"];
//        if(!addressCell){
//            addressCell = [[CustomAdressTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddressCell"];
//          }
//        addressCell.textLabel.text = [_placeNames objectAtIndex:indexPath.row];
//        return addressCell;
//
//    }
    else{
        if(_placeNames.count == 0){
        CustomAdressTableViewCell* addressCell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell"];
       if(!addressCell){
            addressCell = [[CustomAdressTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddressCell"];
        }
        if ([[[addressListArray objectAtIndex:indexPath.row] valueForKey:@"Default_address"] isEqualToString:@"1"]) {
            [addressCell.mAddressImage setImage:[UIImage imageNamed:@"home"]];
        }else {
            [addressCell.mAddressImage setImage:[UIImage imageNamed:@"address_pin"]];
        }
        addressCell.mAddressLabel.text = [NSString stringWithFormat:@"%@,%@",[[addressListArray objectAtIndex:indexPath.row] valueForKey:@"Address"],[[addressListArray objectAtIndex:indexPath.row] valueForKey:@"City"]];
       
        return addressCell;
        }
        else{
            CustomPlaceAddressTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
            if(!cell){
                cell = [[CustomPlaceAddressTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            }
            //if (indexPath.row < _placeNames.count) {
                cell.mPrimaryAddress.text = [(PlaceObject*)[_placeNames objectAtIndex:indexPath.row] primaryAddress];
                cell.mSecondaryAddress.text = [(PlaceObject*)[_placeNames objectAtIndex:indexPath.row] secondaryAddress];
            //}
            
            return cell;

        }
    }
}

-(void)updateCollectionView :(UICollectionView*)collectionView withindex :(NSIndexPath*)indexPath{
 
    RepairCollectionViewCell* cell  = (RepairCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    if (cell.isSelected) {
        cell.isSelected = NO;
        cell.imageBackgroundView.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1.f];
        
        [arrayWithServicesId removeObject:[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"ItemId"]];
        [arrayWithServicesPrices removeObjectForKey:[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"ItemId"]];
        // [arrayWithServicesPrices removeObject:[[mServiceArray objectAtIndex:indexPath.row] valueForKey:@"price"]];
    }else {
        cell.isSelected = YES;
        if (![arrayWithServicesId containsObject:[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"ItemId"]]) {
            [arrayWithServicesId removeObject:[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"ItemId"]];
            //[arrayWithServicesPrices removeObject:[[mServiceArray objectAtIndex:indexPath.row] valueForKey:@"Price"]];
        }
        if (![[arrayWithServicesPrices allKeys] containsObject:[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"ItemId"]]) {
            [arrayWithServicesPrices removeObjectForKey:[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"ItemId"]];
        }
        [arrayWithServicesId addObject:[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"ItemId"]];
        [arrayWithServicesPrices setObject:[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"Price"] forKey:[[mServiceArrayForTableView objectAtIndex:indexPath.row] valueForKey:@"ItemId"]];//addObject:[[mServiceArray objectAtIndex:indexPath.row] valueForKey:@"Price"]];
        //
        cell.imageBackgroundView.backgroundColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:1.f];
    }
    //
    //    NSLog(@"arrayWithServicesId = %@",arrayWithServicesId);
    //    NSLog(@"arrayWithServicesPrices = %@",arrayWithServicesPrices);
   // [self.mTotalLbl setText:;
    self.mTotalLbl.text = [NSString stringWithFormat:@"$%@",[self totallPrices:[arrayWithServicesPrices allValues]]];
    
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.mTableView) {

       // [self updateTableView:tableView withindex:indexPath];
    }else{
        mSelectedTableView = YES;
        //self.mVisualEffectView.hidden = NO;
        [ProgressHUD show:@"Please wait..."];
        NSLog(@"Defoult Addres N%ld",(long)indexPath.row);
        double  latitute;
        double longitute;
        if(_placeNames != nil && _placeNames.count > 0){
        CustomPlaceAddressTableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
        NSString* adress3 = cell.mPrimaryAddress.text;
        
        CLLocationCoordinate2D cord =  [self cordinatFromAdress:adress3];
            latitute = cord.latitude;
            longitute = cord.longitude;
        }else{
            
              latitute = [(NSString*)[[addressListArray objectAtIndex:indexPath.row] valueForKey:@"Lat"] doubleValue];
              longitute = [(NSString*)[[addressListArray objectAtIndex:indexPath.row] valueForKey:@"Long"] doubleValue];
        
        
        if (latitute && longitute) {
           
                         [self addMarkerWithLatitute:latitute andLongitute:longitute];
        }else{
               NSLog(@"Latitute = %f   \n  Longitute = %f",latitute,longitute);
             }
        }
    }
    [self performSelector:@selector(hideVisualEffectView) withObject:nil afterDelay:1];
    //[self performSelector:@selector(bottomViewPosition) withObject:nil afterDelay:0.5];
    
}

-(void)bottomViewPosition {
    if (isOpenedBottomView) {
        self.mButtomViewHeight.constant = 0;
    }else {
        self.mButtomViewHeight.constant = 0;
    }
    
   
}

-(void)hideVisualEffectView {
    //self.mVisualEffectView.hidden = YES;
    [ProgressHUD dismiss];
}
-(CLLocationCoordinate2D )cordinatFromAdress :(NSString*)adress{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
   __block NSString *latDest1;
   __block NSString *lngDest1;
    [geocoder geocodeAddressString:adress completionHandler:^(NSArray* placemarks, NSError* error){
        for (CLPlacemark* aPlacemark in placemarks)
        {
            // Process the placemark.
           latDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.latitude];
           lngDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.longitude];
            
        }
        [self addMarkerWithLatitute:latDest1.doubleValue andLongitute:lngDest1.doubleValue];
    }];
    CLLocationCoordinate2D cord = CLLocationCoordinate2DMake(latDest1.doubleValue, lngDest1.doubleValue);
    return cord;
}

-(void)showAllMarkers:(NSMutableArray*)mMarkersArray {
    NSMutableArray *myMarkers = mMarkersArray;   // array of marker which sets in Mapview
    GMSMutablePath *path = [GMSMutablePath path];
    
    for (GMSMarker *marker in myMarkers) {
        [path addCoordinate: marker.position];
    }
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:path];
    
    [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    
}

-(void)openBottomAllView {
   
        CGRect newFrame = self.mAllButtomViews.frame;
        newFrame.origin.y = (self.view.frame.size.height  - self.mAllButtomViews.frame.size.height);
        self.mAllButtomViews.frame = newFrame;
        CGRect newFrameForMap = self.mViewForMap.frame;
        CGRect newFrameForMap1 = self.mViewForMap.frame;
        newFrameForMap.size.height = self.mAllButtomViews.frame.origin.y;
        newFrameForMap1.size.height = self.mAllButtomViews.frame.origin.y;
        self.mViewForMap.frame = newFrameForMap;
        self.mapView.frame = newFrameForMap1;
        [_mapView layoutIfNeeded];
        self.mPinImageView.frame = CGRectMake(self.mPinImageView.frame.origin.x, self.mapView.frame.size.height/2 - self.mPinImageView.frame.size.height, self.mPinImageView.frame.size.width, self.mPinImageView.frame.size.height);
        self.infoVindowImageView.frame = CGRectMake(self.infoVindowImageView.frame.origin.x, self.mPinImageView.frame.origin.y - self.infoVindowImageView.frame.size.height, self.infoVindowImageView.frame.size.width, self.infoVindowImageView.frame.size.height);
        [self.mPinImageView layoutIfNeeded];
        [self.infoVindowImageView layoutIfNeeded];
        CGRect newFrameForPointView = self.pointView.frame;
        newFrameForPointView.origin.y = self.mViewForMap.frame.size.height/2;
        self.pointView.frame = newFrameForPointView;
        [self.pointView layoutIfNeeded];
        [self.infoVindowImageView layoutIfNeeded];
        
        [self.mAllButtomViews layoutIfNeeded];
        //[self.view layoutIfNeeded];
        self.mSearchBtn.hidden = YES;
        self.mSquarPoint.hidden = YES;
   

}

-(void)addCustomerWithLatitute:(double)latitute  andLongitute:(double) longitute {
   
    
   _mCustomerMarker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(latitute, longitute)];
    
    _mCustomerMarker.map = _mapView;
    _mCustomerMarker.icon = [UIImage imageNamed:@"ph_icon"];
    // [_mapView setSelectedMarker:_customerMarker];
}
-(void)addMarkerWithLatitute:(double) latitute andLongitute:(double) longitute {
    //[self openBottomAllView];
    [self getNearRepaierTime];
    self.infoVindowImageView.hidden = NO;
    self.mPinImageView.hidden = NO;
    //[_mapView clear];
    //_customerMarker.map = nil;
    _customerMarker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(latitute, longitute)];
    
    //_customerMarker.map = _mapView;
    //_customerMarker.icon = [UIImage imageNamed:@"chop"];
   // [_mapView setSelectedMarker:_customerMarker];
   // _customerMarker.groundAnchor = CGPointMake(0, 0);
    CLLocationCoordinate2D target = CLLocationCoordinate2DMake(latitute, longitute);
    self.mapView.center =  [_mapView.projection pointForCoordinate:target];
    //[markersArray addObject:_customerMarker];
    
   
    [self.mapView animateToLocation:target];
    [self.mapView animateToZoom:17];
//    self.mSearchBtn.hidden = YES;
//    self.mSquarPoint.hidden = YES;
    [self backButtonPressed:self.mCurrentLocationBtn];
   // [self performSelector:@selector(animateClockArrow) withObject:nil afterDelay:5];
    //[self performSelectorInBackground:@selector(animateClockArrow) withObject:nil];
    
    /*
    CABasicAnimation *blink = [CABasicAnimation animationWithKeyPath:@"opacity"];
    blink.fromValue = [NSNumber numberWithFloat:1.0];
    blink.toValue = [NSNumber numberWithFloat:0.0];
    blink.duration = 1.5;
    [blink setDelegate:self];
    blink.cumulative = YES;
    blink.repeatCount = 10000;
    [clockArrow.layer addAnimation:blink forKey:@"blinkmarker"];
    */
       //[self marker:_customerMarker setPosition:CLLocationCoordinate2DMake(40.234245, 43.2342345)];
    //[self showAllMarkers:markersArray];
    
//    if (isOpenedBottomView) {
//        self.mButtomViewHeight.constant = 0;
//    }else {
//        self.mButtomViewHeight.constant = -172;
//    }
//    isOpenedBottomView = YES;
}

-(void)animateClockArrow {
    [self runSpinAnimationOnView:clockArrow
                        duration:0.5
                       rotations:0.7
                          repeat:100000];
    

}
-(NSString*)totallPrices:(NSArray*)priceArray {
    CGFloat totalPrice = 0;
    if (priceArray.count > 0) {
        for (int i = 0; i < priceArray.count; i++) {
            totalPrice += [[priceArray objectAtIndex:i] floatValue];
        }

    }
    
        return [NSString stringWithFormat:@"%.2f",totalPrice];
}
- (BOOL)image:(UIImage *)image1 isEqualTo:(UIImage *)image2
{
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    
    return [data1 isEqual:data2];
}

-(void)getServicesWithIndex:(NSInteger)index {
    
//    GetFiltrProductRequest * mGetFiltrProductRequest = [[GetFiltrProductRequest alloc] initWithApiKey:[_userAccount valueForKey:@"apiKey"] andProductId:[[mDeviceArray objectAtIndex:index] valueForKey:@"id"]];
//    
//    [mGetFiltrProductRequest postWithSuccess:^(NSDictionary *response) {
//        NSLog(@"response : %@",response);
//        NSError * error;
//        NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
//        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        mServiceArrayForTableView = [[newServicesArray objectAtIndex:index] valueForKey:@"ServiceItemsDetails"];

    [self.repairCollectionView reloadData];
    //self.mVisualEffectView.hidden = YES;
        //[ProgressHUD dismiss];
//    } failure:^(NSError *error, NSInteger responseCode) {
//        NSLog(@"ERROR : %@",error.description);
//    }];
}

#pragma mark --MKMap

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    NSLog(@"region Will Change Animated");
    
    isLoadMap = YES;
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
     NSLog(@"region Did Change Animated");
    //[self Getnearests];
    if (isLoadMap) {
        if([self checkInternetConnection]){
            [self geocoderMethod:[self.mMapView centerCoordinate].latitude longitude:[self.mMapView centerCoordinate].longitude];
            //[self Getnearests];
            //[self getNearRepaierTime];
        }else{
           [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
        }
       

    }

    

}
//
//- (void)mapViewWillStartRenderingMap:(MKMapView *)mapView {
//     NSLog(@"map View Will Start Rendering Map");
//}
//- (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView fullyRendered:(BOOL)fullyRendered {
//     NSLog(@"map View Did Finish Rendering Map");
//    self.mVisualEffectView.hidden = YES;
//    //[ProgressHUD dismiss];
////    if (isFinishedRenderMap) {
////        [self.mMapView removeAnnotations:self.mMapView.annotations];
////        [self Getnearests];
////
////    }
////    isFinishedRenderMap = YES;
//    if([self checkInternetConnection]){
//        //[self Getnearests];
//        //[self getNearRepaierTime];
//    }else{
//        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
//    }
//    
//
//    
//}
//


- (void)addPlacemarkAnnotationToMap:(CLPlacemark *)placemark addressString:(NSString *)address {
    [self.mMapView removeAnnotation:selectedPlaceAnnotation];
    //[selectedPlaceAnnotation release];
    
    selectedPlaceAnnotation = [[MKPointAnnotation alloc] init];
    selectedPlaceAnnotation.coordinate = placemark.location.coordinate;
    selectedPlaceAnnotation.title = address;
    [self.mMapView addAnnotation:selectedPlaceAnnotation];
}

#pragma mark Current Location
-(void)setCurrentLocationWithLocation:(CLLocationCoordinate2D)location {
    //if(!self.currentLocationMarker){
    //let circleCenter : CLLocationCoordinate2D  = CLLocationCoordinate2DMake(centerLattitude, centerLongitude);
    if (_circ && _circCertr) {
        [_circ beginRadiusAnimationFrom:_radius to:_radius *100 duration:1 completeHandler:^{
            
        }];
    }else{
   
    CLLocationCoordinate2D circleCenter = location;
        
        _circCertr = [TAMapCircle circleWithPosition:circleCenter radius:_radius];
        _circCertr.fillColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:1];
        _circCertr.strokeColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:1];
        _circCertr.strokeWidth = 1.5f;
        
        _circCertr.map = _mapView;
        
        
    _circ = [TAMapCircle circleWithPosition:circleCenter radius:_radius * 1000];
    _circ.fillColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:0.1f];
    _circ.strokeColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:0.5f];
    _circ.strokeWidth = 1.5f;
    
    _circ.map = _mapView;
    [_circ beginRadiusAnimationFrom:_radius to:_radius * 100 duration:1 completeHandler:^{
        
    }];
    }
   
    
   
   
//        self.currentLocationMarker.map = nil;
//        self.currentLocationMarker = [GMSMarker markerWithPosition:location];
//        self.currentLocationMarker.map = _mapView;
//        self.currentLocationMarker.groundAnchor = CGPointMake(0.9,0.9);
//        self.currentLocationMarker.icon = [UIImage imageNamed:@"klor_full"];
    
//    self.currentLocationMarker.icon = [UIImage animatedImageWithImages:[NSArray arrayWithObjects:[UIImage imageNamed:@"circle_20"],
//                                                                                                 [UIImage imageNamed:@"circle_33"],
//                                                                                                 [UIImage imageNamed:@"circle_46"],
//                                                                                                 [UIImage imageNamed:@"circle_59"],
//                                                                                                 [UIImage imageNamed:@"circle_72"],
//                                                                                                 [UIImage imageNamed:@"circle_85"],
//                                                                                                 [UIImage imageNamed:@"circle_98"],
//                                                                                                 [UIImage imageNamed:@"circle_111"],
//                                                                                                 [UIImage imageNamed:@"circle_124"],
//                                                                                                 [UIImage imageNamed:@"circle_137"],
//                                                                                                 [UIImage imageNamed:@"circle_150"],nil]
//                                                              duration:1.5];
//    
   // }
    
    //[self animateCurentLocationMarker];
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.5];
    //self.currentLocationMarker.position = location;
    
    [CATransaction commit];
    
    
}

-(void)animateCurentLocationMarker {
    [UIView animateKeyframesWithDuration:0.7 delay:0.0 options:UIViewKeyframeAnimationOptionAutoreverse | UIViewKeyframeAnimationOptionRepeat animations:^{
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.1 animations:^{
            self.currentLocationMarker.iconView.transform = CGAffineTransformMakeScale(0.5, 0.5);
        }];
        [UIView addKeyframeWithRelativeStartTime:0.7 relativeDuration:0.7 animations:^{
            self.currentLocationMarker.iconView.transform = CGAffineTransformMakeScale(1, 1);
            
        }];
    } completion:nil];
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    mLongitute = [NSString stringWithFormat:@"%.10f", newLocation.coordinate.longitude];
    mLatitude = [NSString stringWithFormat:@"%.10f", newLocation.coordinate.latitude];
    
    if (!isOpenView) {
        isOpenView = YES;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[mLatitude doubleValue]
                                                                longitude:[mLongitute doubleValue]
                                                                     zoom:1];
        _mapView = [GMSMapView mapWithFrame:self.view.frame camera:camera];
        
        
        GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:[[NSBundle mainBundle] URLForResource:@"mapstyle" withExtension:@"json"] error:nil];
        
        
        [_mapView animateToZoom:17.f];
        _mapView.myLocationEnabled = NO;
        _mapView.settings.myLocationButton = NO;
        
        // [_mapView setMinZoom:1.f maxZoom:20.f];
        _mapView.mapStyle = style;
        
        
        
        //[CATransaction begin];
        //[CATransaction setValue:[NSNumber numberWithFloat: 1.0f] forKey:kCATransactionAnimationDuration];
        // change the camera, set the zoom, whatever.  Just make sure to call the animate* method.
        // [self.mapView animateToCameraPosition:camera];
        //[CATransaction commit];
        
        
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"isAccepted"]) {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isAccepted"];
            [[NSUserDefaults standardUserDefaults] synchronize ];
            
            // [self.mViewForMap addSubview:_mapView];
            //[self getAllRepaiers];
            
            
            
        }else{
            //[self getAllRepaiers];
            BOOL isExist = [[NSUserDefaults standardUserDefaults] boolForKey:@"isAccepted"];
            if (isExist) {
                self.mSearchBtn.hidden = YES;
                self.mSquarPoint.hidden = YES;
                
                [_mapView clear];
                //[self.mViewForMap addSubview:_mapView];
                [self addRepairAnnotationWithLatitude:[[[NSUserDefaults standardUserDefaults] valueForKey:@"repaierLat"] doubleValue] andLongitude:[[[NSUserDefaults standardUserDefaults] valueForKey:@"repaierLong"] doubleValue]];
                
                // _customerLocation
                
                [self addCustomerWithLatitute:[[[NSUserDefaults standardUserDefaults] valueForKey:@"customerLat"] doubleValue] andLongitute:[[[NSUserDefaults standardUserDefaults] valueForKey:@"customerLong"] doubleValue]];
                _mapView.myLocationEnabled = NO;
                
                
                
                [self showAllMarkers:[NSMutableArray arrayWithObjects:_repaierMarker,_mCustomerMarker, nil]];
                myTimer = [NSTimer scheduledTimerWithTimeInterval:7.0 target:self
                                                         selector:@selector(move) userInfo:nil repeats:YES];
            }else{
                
                
                //[self.mViewForMap addSubview:_mapView];
                
            }
            
        }
        [self.mViewForMap addSubview:_mapView];
        
        self.mapView.delegate = self;
        

        [self.mViewForMap addSubview:_mapView];
        [_mapView animateToLocation:newLocation.coordinate];
        
        _mapView.center = [_mapView.projection pointForCoordinate:newLocation.coordinate];
        self.mPinImageView.hidden = NO;
        self.infoVindowImageView.hidden = NO;
        
        
    }
   
   
    //if (localCount == 2) {
         [self setCurrentLocationWithLocation:newLocation.coordinate];
      //  localCount = 0;
  //  }
   
   static BOOL isCall = YES;
    
       if (isCall && !isAccepted) {
      
       

        [self geocoderMethod:[mLatitude doubleValue] longitude: [mLongitute doubleValue]];
           [self changeMapCentrerCoordinatesBy:CLLocationCoordinate2DMake([mLatitude doubleValue], [mLongitute doubleValue])];
        isCall = NO;
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"validOrder"]) {
           
            self.mRepaierDetailsView.hidden = NO;
            self.mRepaierNameLbl.hidden = NO;
            self.mRepaierPhoneBtn.hidden = NO;
            self.mAllButtomViews.hidden = NO;
            [self.mRepaierPhoneBtn setTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"repaierPhone"] forState:UIControlStateNormal];
            self.mRepaierNameLbl.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"repaierName"];
            [self.view layoutIfNeeded];
            if([self checkInternetConnection]){
               [self moveRepairer:NO];
            }else{
               [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
            }
            

            NSLog(@"timer%%%%%%%%%%%%%%%%%");
            myTimer = [NSTimer scheduledTimerWithTimeInterval:7.0 target:self
                                                     selector:@selector(move) userInfo:nil repeats:YES];
            
        }else{
//            MKCoordinateRegion region = self.mMapView.region;
//             MKCoordinateSpan span = self.mMapView.region.span;
//            span = MKCoordinateSpanMake(50, 50);
//            region.span = span;
//            self.mMapView.region = region;
            static BOOL isCalled = YES;
           
            if([self checkInternetConnection] && isCalled){
               [self Getnearests];
                isCalled = NO;
                //[self performSelectorInBackground:@selector(getNearRepaierTime) withObject:nil];
            }else{
                [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
            }
            

            
            
        }

    }
    

}

- (void)recenterMapToPlacemark:(CLLocationCoordinate2D)locations {
    
    
    
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    
    span.latitudeDelta = 0.02;
    span.longitudeDelta = 0.02;
    
    region.span = span;
    region.center = locations;
    CustomerAnnotation = [[MapAnotation alloc] init];
    CustomerAnnotation.coordinate = locations;
    
    CustomerAnnotation.ann_tag = 1;
    CustomerAnnotation.title = nearTimeStr;
    
    [self.mMapView addAnnotation:CustomerAnnotation];
    
    if (!isFirstTime) {
        [self.mMapView setRegion:region];
        isFirstTime = YES;
    }
    
    
    mLatitude = [NSString stringWithFormat:@"%f",locations.latitude];
    mLongitute = [NSString stringWithFormat:@"%f",locations.longitude];
   isLoadMap = YES;
}

- (void)recenterMapToPlacemarkForRepaiers:(CLLocationCoordinate2D)locations {
    
    
//    MKCoordinateRegion region;
//    MKCoordinateSpan span;
//    
//    span.latitudeDelta = 0.02;
//    span.longitudeDelta = 0.02;
//    
//    region.span = span;
//    region.center = locations;
  
    
    
       RepaierAnnotation = [[MapAnotation alloc] init];
        RepaierAnnotation.coordinate = locations;
        
    RepaierAnnotation.ann_tag = 2;
        [_allRepaiersArray addObject:RepaierAnnotation];
        
        [self.mMapView addAnnotation:RepaierAnnotation];
        
    [self.mMapView showAnnotations:self.mMapView.annotations animated:YES];
        countRepair++;
       // [self addTime];
   

    
}


-(void)addRepairAnnotationWithLatitude:(double)latitude andLongitude:(double)longitude {
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    _repaierMarker = [GMSMarker markerWithPosition:position];
    _repaierMarker.infoWindowAnchor = CGPointMake(0.5, 0.5);
    _repaierMarker.icon = [UIImage imageNamed:@"small-car"];
    _repaierMarker.map = self.mapView;
    [markersArray addObject:_repaierMarker];
    //[self showAllMarkers:markersArray];
}

-(void)Getnearests {
    NSLog(@"ffdgfgfdfgdfgdgfdgfdgfdgfdg");
    //
    self.nearestRepairesLocationsArray = [[NSMutableArray alloc]init];
    GetnearestsRequest * mGetnearestsRequest = [[GetnearestsRequest alloc] initWithApiKey:_userAccount.apiKey];
  
    [mGetnearestsRequest postWithSuccess:^(NSDictionary *response) {
        NSLog(@"response : %@",response);
        NSError * myError;
        NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&myError];
        self.nearestRepairesLocationsArray = (NSMutableArray*)[dictionary objectForKey:@"message"];
        NSLog(@"nearestRepairesLocationsArray : %@",self.nearestRepairesLocationsArray);
        repaiersCount = self.nearestRepairesLocationsArray.count;
        
       
        
        if(markersArray){
            
            for (GMSMarker *m in markersArray){
                m.map = nil;
                
            }
            
        }
        
        if (self.nearestRepairesLocationsArray.count > 0) {
            for (int i = 0; i < self.nearestRepairesLocationsArray.count; i++) {
                //[self performSelectorInBackground:@selector(addRepaiersWithIndex:) withObject:[NSNumber numberWithInt:i]];
                //[self.mMapView showAnnotations:self.mMapView.annotations animated:YES];
                //[self recenterMapToPlacemarkForRepaiers:CLLocationCoordinate2DMake([[[mServiceArray objectAtIndex:i] valueForKey:@"lat"] doubleValue], [[[mServiceArray objectAtIndex:i] valueForKey:@"long"] doubleValue])];
                
                                
                [self addRepairAnnotationWithLatitude:[[[self.nearestRepairesLocationsArray objectAtIndex:i] valueForKey:@"lat"] doubleValue]
                                         andLongitude:[[[self.nearestRepairesLocationsArray objectAtIndex:i] valueForKey:@"long"] doubleValue]];
            }
//            if(isFirst){
//            [self showAllMarkers:markersArray];
//                isFirst = NO;
//            }
           // [self.mMapView showAnnotations:self.mMapView.annotations animated:YES];

           //[self getNearRepaierTime];
        }
        countRepair = 0;
        //[self getNearRepaierTime];
       // [self addTime];
      
        } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"error : %@",error.description);

    }];
}



-(void)addCustomerToMap {
    
    
    [self recenterMapToPlacemark:CLLocationCoordinate2DMake([mLatitude doubleValue], [mLongitute doubleValue])];
}
- (void)addRepaiersWithIndex:(NSNumber*)index {
  
//        [self recenterMapToPlacemarkForRepaiers:CLLocationCoordinate2DMake([[[mServiceArray objectAtIndex:[index integerValue]] valueForKey:@"lat"] doubleValue], [[[mServiceArray objectAtIndex:[index integerValue]] valueForKey:@"long"] doubleValue])];
   
    
}
-(MKAnnotationView*)mapView:(MKMapView*)mapView viewForAnnotation:(MapAnotation <MKAnnotation>*)annotation
{
    

    
    UIView * pinView;
    if (annotation.ann_tag == 2) {
        
        
        if (cout < repaiersCount+1) {
            DXAnnotationView * annotationViewRepaier;
            NSLog(@"RepaierAnnotation Coordinate Lat = %f   Long = %f",RepaierAnnotation.coordinate.latitude,RepaierAnnotation.coordinate.longitude);
            pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pointer.png"]];
            [pinView setFrame:CGRectMake(0, 0, 40, 40)];
            UIView * cloudView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 80)];
            UIImageView * bgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cloudView.frame.size.width, cloudView.frame.size.height)];
            bgImageView.image = [UIImage imageNamed:@"cursor"];
            [cloudView addSubview:bgImageView];
            
            UIImageView * clockImage = [[UIImageView alloc] initWithFrame:CGRectMake(8, 17, 20, 20)];
            clockImage.image = [UIImage imageNamed:@"clock"];
            clockImage.backgroundColor = [UIColor clearColor];
            [cloudView addSubview:clockImage];
            // [cloudView addSubview:lbl];
            cloudView.backgroundColor = [UIColor clearColor];
            //annotationViewRepaier = (DXAnnotationView*)[self.mMapView dequeueReusableAnnotationViewWithIdentifier:@"Varpet"];
            
           // if (!annotationViewRepaier) {
                annotationViewRepaier = [[DXAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"Varpet" pinView:pinView calloutView:nil settings:[DXAnnotationSettings defaultSettings]];
                
           // }
            
            [annotationViewRepaier insertTitleTolabel:annotation.title];
            [annotationViewRepaier setEnabled:NO];
            [annotationViewRepaier hideCalloutView];
            annotationViewRepaier.canShowCallout = NO;
            
            cout++;
            [repaiersAnnotations setObject:annotationViewRepaier forKey:[NSString stringWithFormat:@"%.3f",annotation.coordinate.latitude]];
            return annotationViewRepaier;
        }else{   
            
            return [repaiersAnnotations valueForKey:[NSString stringWithFormat:@"%.3f",annotation.coordinate.latitude]];
        }
  
       
        
        
     
    }else {
        
        
        
        if (CustCount < 1) {
            NSLog(@"Customer Coordinate Lat = %f   Long = %f",CustomerAnnotation.coordinate.latitude,CustomerAnnotation.coordinate.longitude);
            if (CustomerAnnotation.coordinate.latitude != 0 && CustomerAnnotation.coordinate.longitude != 0) {
                DXAnnotationView * annotationViewCustomer;
                pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
                [pinView setFrame:CGRectMake(0, 0, 40, 40)];
//                UIView * cloudView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 80)];
//                UIImageView * bgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cloudView.frame.size.width, cloudView.frame.size.height)];
//                bgImageView.image = [UIImage imageNamed:@"cursor"];
//                [cloudView addSubview:bgImageView];
//                
//                UIImageView * clockImage = [[UIImageView alloc] initWithFrame:CGRectMake(8, 12, 20, 20)];
//                clockImage.image = [UIImage imageNamed:@"jam"];
//                clockImage.backgroundColor = [UIColor clearColor];
//                [cloudView addSubview:clockImage];
//                // [cloudView addSubview:lbl];
//                cloudView.backgroundColor = [UIColor clearColor];
                annotationViewCustomer = (DXAnnotationView*)[self.mMapView dequeueReusableAnnotationViewWithIdentifier:@"Customer"];
                
                //if (!annotationViewCustomer) {
                    annotationViewCustomer = [[DXAnnotationView alloc]initWithAnnotation:CustomerAnnotation reuseIdentifier:@"Customer" pinView:pinView calloutView:nil settings:[DXAnnotationSettings defaultSettings]];
                    
                //}
                
               // [annotationViewCustomer insertTitleTolabel:nearTimeStr];
                //[annotationViewCustomer showCalloutView];
                [annotationViewCustomer setEnabled:NO];
                annotationViewCustomer.userInteractionEnabled = NO;
                CustCount++;
                [customersAnnotations setObject:annotationViewCustomer forKey:[NSString stringWithFormat:@"%.3f",annotation.coordinate.latitude]];
                return annotationViewCustomer;
            }else{
              DXAnnotationView * annotationViewCustomer;

             pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pointer.png"]];
             [pinView setFrame:CGRectMake(0, 0, 40, 40)];
             UIView * cloudView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 80)];
             UIImageView * bgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cloudView.frame.size.width, cloudView.frame.size.height)];
             bgImageView.image = [UIImage imageNamed:@"cursor"];
             [cloudView addSubview:bgImageView];
             
             UIImageView * clockImage = [[UIImageView alloc] initWithFrame:CGRectMake(8, 17, 20, 20)];
             clockImage.image = [UIImage imageNamed:@"clock"];
             clockImage.backgroundColor = [UIColor clearColor];
             [cloudView addSubview:clockImage];
             // [cloudView addSubview:lbl];
             cloudView.backgroundColor = [UIColor clearColor];
             annotationViewCustomer = (DXAnnotationView*)[self.mMapView dequeueReusableAnnotationViewWithIdentifier:@"Varpet"];
             
             if (!annotationViewCustomer) {
             annotationViewCustomer = [[DXAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"Varpet" pinView:pinView calloutView:nil settings:[DXAnnotationSettings defaultSettings]];
             
             }
             
             [annotationViewCustomer insertTitleTolabel:annotation.title];
             [annotationViewCustomer setEnabled:NO];
             [annotationViewCustomer hideCalloutView];
             annotationViewCustomer.canShowCallout = NO;
                 return [repaiersAnnotations valueForKey:[NSString stringWithFormat:@"%.3f",annotation.coordinate.latitude]];

             
             
             }
            
            
  
        }else{
            
            if ([[customersAnnotations allKeys] containsObject:[NSString stringWithFormat:@"%.3f",annotation.coordinate.latitude]]) {
                return [customersAnnotations valueForKey:[NSString stringWithFormat:@"%.3f",annotation.coordinate.latitude]];
            }else {
                
                DXAnnotationView * annotationViewCustomer;
                
                pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pointer.png"]];
                [pinView setFrame:CGRectMake(0, 0, 40, 40)];
                UIView * cloudView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 80)];
                UIImageView * bgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cloudView.frame.size.width, cloudView.frame.size.height)];
                bgImageView.image = [UIImage imageNamed:@"cursor"];
                [cloudView addSubview:bgImageView];
                
                UIImageView * clockImage = [[UIImageView alloc] initWithFrame:CGRectMake(8, 17, 20, 20)];
                clockImage.image = [UIImage imageNamed:@"clock"];
                clockImage.backgroundColor = [UIColor clearColor];
                [cloudView addSubview:clockImage];
                // [cloudView addSubview:lbl];
                cloudView.backgroundColor = [UIColor clearColor];
                annotationViewCustomer = (DXAnnotationView*)[self.mMapView dequeueReusableAnnotationViewWithIdentifier:@"Varpet"];
                
                if (!annotationViewCustomer) {
                    annotationViewCustomer = [[DXAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"Varpet" pinView:pinView calloutView:nil settings:[DXAnnotationSettings defaultSettings]];
                    
                }
                
                [annotationViewCustomer insertTitleTolabel:annotation.title];
                [annotationViewCustomer setEnabled:NO];
                [annotationViewCustomer hideCalloutView];
                annotationViewCustomer.canShowCallout = NO;
                return [repaiersAnnotations valueForKey:[NSString stringWithFormat:@"%.3f",annotation.coordinate.latitude]];
                
            }
            
        }
       

    }
    
   }


//- (MKAnnotationView *)mapView:(MKMapView* )mapView viewForAnnotation:(id <MKAnnotation>)annotation
//{
//    MKAnnotationView *annotationView = nil;
////    if( [annotation isKindOfClass:[YOURANNOTATION class] ] )
////    {
//        static NSString * AnnotationID = @"YOURANNOTATION";
//        annotationView = [self.mMapView  dequeueReusableAnnotationViewWithIdentifier:AnnotationID];
//        if( annotationView == nil )
//        {
//            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
//                                                          reuseIdentifier:AnnotationID] ;
//            
//        }
//        UIImage * flagImage = nil;
//        
//        flagImage = [UIImage imageNamed:@"pink.png"];
//       // [annotationView setImage:flagImage];
//        annotationView.canShowCallout = YES;
//        
//        //   add an image to the callout window
//        UIImageView *leftIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pink.png"]];
//        annotationView.leftCalloutAccessoryView = leftIconView;
//    UIImageView *leftIconView1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pink.png"]];
//    annotationView.rightCalloutAccessoryView.backgroundColor = [UIColor clearColor];
//
//    
//        
//        //adding right button accessory
////        UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
////        
////        annotationView.rightCalloutAccessoryView = infoButton;
//    
//        //image size and adding image on left accessory
//        CGRect resizeRect;
//        resizeRect.size = flagImage.size;
//        resizeRect.origin = (CGPoint){0.0f, 0.0f};
//        UIGraphicsBeginImageContext(resizeRect.size);
//        [flagImage drawInRect:resizeRect];
//        UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();    
//        //annotationView.image = resizedImage;
//    UIView * dView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 100)];
//    [dView addSubview:leftIconView];
//    annotationView.detailCalloutAccessoryView.backgroundColor = [UIColor redColor];
//    
//        
//    //}
//    return annotationView;
//}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    
    
    MKAnnotationView *annotation=(MKAnnotationView*)view.annotation;
    
    //do something with your annotation
    
}

-(void)changeMapCentrerCoordinatesBy:(CLLocationCoordinate2D)coordinates {
    
    self.mMapView.centerCoordinate = coordinates;
    MKCoordinateRegion region;
    region.center.latitude = coordinates.latitude;
    region.center.longitude = coordinates.longitude;
    region.span.latitudeDelta = 0.01;
    region.span.longitudeDelta = 0.01;
    region = [self.mMapView regionThatFits:region];
    [self.mMapView setRegion:region animated:TRUE];
}
-(void)geocoderMethod:(CLLocationDegrees)latitude longitude:(CLLocationDegrees) longitude{
    address1 = @"";
    CLGeocoder * ceo = [[CLGeocoder alloc]init];
    CLLocation* loc = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    [ceo reverseGeocodeLocation:loc completionHandler:^(NSArray * placemarks, NSError*  error){
        CLPlacemark* placemark = [placemarks objectAtIndex:0];
        if (placemark){
            //NSLog(@"Placemark  %@", placemark);
            addressDictionary = (NSMutableDictionary*)[placemark addressDictionary];
            address1 = [addressDictionary valueForKey:@"Name"];
//            for (int i = 0;  i < addressArray.count; i++) {
//                if ([address1 isEqualToString:@""]) {
//                    address1 = [pss1 stringByAppendingString:[addressArray objectAtIndex:i]];
//                }else {
//                    address1 = [address1 stringByAppendingString:@","];
//                    address1 = [address1 stringByAppendingString:[addressArray objectAtIndex:i]];
//                }
//                
//            }
           // self.mSearchBar.text = address1;
            [self.view reloadInputViews];
            // NSString* str = placemark.thoroughfare;
            // NSString* locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAd dressLines"]componentsJoinedByString:@" ,"];
            // NSLog(@"addressDictionary %@",placemark.addressDictionary);
        }else{
            NSLog(@"Could not locate");
        }
    }
     ];
    
    
}
- (IBAction)PhoneCallAction:(id)sender {
    
    
    if ([appObj.AcceptNotData valueForKey:@"repairer_phone"]) {
        NSString *phoneNumber = [appObj.AcceptNotData valueForKey:@"repairer_phone"];
        phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"Phone number = %@",phoneNumber);
        NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:phoneNumber]];
        NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:phoneNumber]];
        
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
            [UIApplication.sharedApplication openURL:phoneUrl];
        } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
            [UIApplication.sharedApplication openURL:phoneFallbackUrl];
        } else {
            // Show an error message: Your device can not do phone calls.
        }
        

    }
   
}

- (IBAction)curentLocationAction:(id)sender {
    //self.mVisualEffectView.hidden = NO;
      CLLocationCoordinate2D target =
    CLLocationCoordinate2DMake([mLatitude doubleValue], [mLongitute doubleValue]);
    
    [self.mapView animateToLocation:target];
    [self.mapView animateToZoom:17];
    
    [self backButtonPressed:sender];
    //[self performSelector:@selector(bottomViewUp) withObject:nil afterDelay:0.5];
   
}

- (IBAction)openServicesListBtnAction:(id)sender {
    
     [locationManager stopUpdatingLocation];
    
    _circCertr.map = nil;
    _circCertr = nil;
    _circ.map = nil;
    _circ = nil;
    


    selectedDeviceIndex = 0;
    [self.mDeviceTypeCollectionView reloadData];
    [self getServicesWithIndex:0];
    
    
    self.showRepaierListBtn.hidden = YES;
    self.closeRepaierListBnt.hidden = NO;
    self.menuBtnImageView.image = [UIImage imageNamed:@"red-arrow"];
   
    self.mButtomViewHeight.constant = 0;
    [self.mViewForMap reloadInputViews];
    [self.mAllButtomViews layoutIfNeeded];
    self.mMapViewBottomConstraint.constant = self.mAllButtomViews.frame.size.height;
    
    
    CGRect newFrameForMap = self.mViewForMap.frame;
    CGRect newFrameForMap1 = self.mapView.frame;
    newFrameForMap.size.height = self.view.frame.size.height - self.mAllButtomViews.frame.size.height;
    newFrameForMap1.size.height = self.view.frame.size.height - self.mAllButtomViews.frame.size.height;
    self.mViewForMap.frame = newFrameForMap;
    self.mapView.frame = newFrameForMap1;
    [self.mViewForMap layoutIfNeeded];
    [_mapView layoutIfNeeded];

    [_mapView.settings setAllGesturesEnabled:NO];

    self.infoVindowImageView.hidden = YES;
    self.mPinImageView.hidden = YES;
    
    _currentLocationMarker.map = nil;
    
   
    
    
    CGPoint point = _mapView.center;
    CLLocationCoordinate2D mapCenter = [_mapView.projection coordinateForPoint:point];
    
    _customerLocation = CLLocationCoordinate2DMake(mapCenter.latitude, mapCenter.longitude);
    

     [self addCustomerWithLatitute:_customerLocation.latitude andLongitute:_customerLocation.longitude];
    
    self.mSearchBtn.hidden = YES;
    self.mSquarPoint.hidden = YES;
//    [UIView animateWithDuration:0.5 animations:^{
//
//        CGRect newFrameForMap1 = self.mapView.frame;
//        newFrameForMap1.size.height = self.mAllButtomViews.frame.origin.y;
//        self.mViewForMap.frame = newFrameForMap1;
//        self.mapView.frame = newFrameForMap1;
//        [self.mViewForMap layoutIfNeeded];
//        self.mPinImageView.frame = CGRectMake(self.mPinImageView.frame.origin.x, self.mapView.frame.size.height/2 - self.mPinImageView.frame.size.height, self.mPinImageView.frame.size.width, self.mPinImageView.frame.size.height);
//        self.infoVindowImageView.frame = CGRectMake(self.infoVindowImageView.frame.origin.x, self.mPinImageView.frame.origin.y - self.infoVindowImageView.frame.size.height, self.infoVindowImageView.frame.size.width, self.infoVindowImageView.frame.size.height);
//        
//        [self.mPinImageView layoutIfNeeded];
//        [self.infoVindowImageView layoutIfNeeded];
//        
//        self.mSearchBtn.hidden = YES;
//        self.mSquarPoint.hidden = YES;
//        CGRect newFrameForPointView = self.pointView.frame;
//        newFrameForPointView.origin.y = self.mMapView.frame.size.height/2;
//        self.pointView.frame = newFrameForPointView;
//        [self.pointView layoutIfNeeded];
//        [self.view layoutIfNeeded];
//    }];

}
- (IBAction)closeRepaiersListAction:(id)sender {
    
    if (_circ && _circCertr) {
        [_circ beginRadiusAnimationFrom:_radius to:_radius *100 duration:0.1 completeHandler:^{
            
        }];
    }else{
        
        CLLocationCoordinate2D circleCenter = CLLocationCoordinate2DMake([mLatitude doubleValue], [mLongitute doubleValue]);
        
        _circCertr = [TAMapCircle circleWithPosition:circleCenter radius:_radius + 5];
        _circCertr.fillColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:1];
        _circCertr.strokeColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:1];
        _circCertr.strokeWidth = 1.5f;
        
        _circCertr.map = _mapView;
        
        
        _circ = [TAMapCircle circleWithPosition:circleCenter radius:_radius * 1000];
        _circ.fillColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:0.1f];
        _circ.strokeColor = [UIColor colorWithRed:249/255.f green:72/255.f blue:72/255.f alpha:0.5f];
        _circ.strokeWidth = 1.5f;
        
        _circ.map = _mapView;
        [_circ beginRadiusAnimationFrom:_radius to:_radius * 100 duration:0.1 completeHandler:^{
            
        }];
    }
    
    self.showRepaierListBtn.hidden = NO;
    self.closeRepaierListBnt.hidden = YES;
    self.menuBtnImageView.image = [UIImage imageNamed:@"ic_menu"];
    self.mMapViewBottomConstraint.constant = 0;
    self.mButtomViewHeight.constant = -(234);
    [self.mAllButtomViews layoutIfNeeded];
    self.mSearchBtn.hidden = NO;
    self.mSquarPoint.hidden = NO;

    CGRect newFrameForMap1 = self.mapView.frame;
    newFrameForMap1.size.height = self.view.frame.size.height;
    self.mViewForMap.frame = newFrameForMap1;
    self.mapView.frame = newFrameForMap1;
    [self.mViewForMap layoutIfNeeded];
    [self.mapView layoutIfNeeded];
    [self.view layoutIfNeeded];
   [_mapView.settings setAllGesturesEnabled:YES];
    
    _mCustomerMarker.map = nil;
    self.infoVindowImageView.hidden = NO;
    self.mPinImageView.hidden = NO;
     [locationManager startUpdatingLocation];
    
//    [UIView animateWithDuration:0.5 animations:^{
//        
//                                self.mButtomViewHeight.constant = -(234);
//        
//                                CGRect newFrameForMap = self.mViewForMap.frame;
//                                CGRect newFrameForMap1 = self.mapView.frame;
//                                newFrameForMap.size.height = self.mAllButtomViews.frame.origin.y;
//                                newFrameForMap1.size.height = self.mAllButtomViews.frame.origin.y;
//                                self.mViewForMap.frame = newFrameForMap;
//                                self.mapView.frame = newFrameForMap1;
//                                [self.mViewForMap layoutIfNeeded];
//                                 [_mapView layoutIfNeeded];
//                                 [self.infoVindowImageView layoutIfNeeded];
//                                CGRect newFrameForPointView = self.pointView.frame;
//                                newFrameForPointView.origin.y = self.mViewForMap.frame.size.height/2;
//                                self.pointView.frame = newFrameForPointView;
//                                [self.pointView layoutIfNeeded];
//                                self.mPinImageView.frame = CGRectMake(self.mPinImageView.frame.origin.x, self.mapView.frame.size.height/2 - self.mPinImageView.frame.size.height, self.mPinImageView.frame.size.width, self.mPinImageView.frame.size.height);
//                                 self.infoVindowImageView.frame = CGRectMake(self.infoVindowImageView.frame.origin.x, self.mPinImageView.frame.origin.y - self.infoVindowImageView.frame.size.height, self.infoVindowImageView.frame.size.width, self.infoVindowImageView.frame.size.height);
//                                [self.mPinImageView layoutIfNeeded];
//                                [self.infoVindowImageView layoutIfNeeded];
//                                [self.view layoutIfNeeded];
//                                self.mSearchBtn.hidden = NO;
//                                self.mSquarPoint.hidden = NO;
//                                isOpenedBottomView = NO;
//                            }];
//
    
    
    
}

-(void)bottomViewUp {
    isOpenedBottomView = YES;
     self.mButtomViewHeight.constant = 0;
}
- (IBAction)orderBtnAction:(id)sender {
    
    [self showOrderAlertWithMessage:@"Message" andText:@"Do You want to do order?"];
    
   
    
}

-(void)showOrderAlertWithMessage:(NSString *) message andText:(NSString *) text {
    
    CGPoint point = _mapView.center;
    CLLocationCoordinate2D mapCenter = [_mapView.projection coordinateForPoint:point];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:message
                                  message:text
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
   __block NSString * string = [[NSString alloc] init];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                             if([self checkInternetConnection]){
                                 [ProgressHUD show:@"Please wait..."];
                                 //self.mVisualEffectView.hidden = NO;
                                
                                 _customerLocation = CLLocationCoordinate2DMake(mapCenter.latitude, mapCenter.longitude);
                                 CreateOrderRequest * mCreateOrderRequest = [[CreateOrderRequest alloc] initWithApiKey:_userAccount.apiKey andUserLatitute:[NSString stringWithFormat:@"%f",mapCenter.latitude] andUserLongitute:[NSString stringWithFormat:@"%f",mapCenter.longitude] andCustomerId:[_customerAccount valueForKey:@"UserId"]  andItemId:arrayWithServicesId];
                                 
                                 [mCreateOrderRequest postWithSuccess:^(NSDictionary *response) {
                                     NSLog(@"response : %@",response);
                                     NSError * error;
                                     NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
                                     NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                     string = (NSString *)[dictionary objectForKey:@"message"];
                                     NSLog(@"Order success block : %@",mServiceArray);
                                     
                                     self.mButtomViewHeight.constant = 0;
                                     //self.mVisualEffectView.hidden = YES;
                                     [self showAlertWithMessage:@"Message" andText:string];
                                     [ProgressHUD dismiss];
                                     
                                 } failure:^(NSError *error, NSInteger responseCode) {
                                     NSLog(@"error : %@",error.description);
                                     [ProgressHUD dismiss];
                                     //self.mVisualEffectView.hidden = YES;
                                 }];

                             }else{
                                 [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
                             }

                             
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                         actionWithTitle:@"Cancel"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                             
                         }];

    [alert addAction:ok];
    [alert addAction:cancel];
    [super presentViewController:alert animated:YES completion:nil];
    
}

-(void) showAlertWithMessage:(NSString *) message andText:(NSString *) text {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:message
                                  message:text
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [super presentViewController:alert animated:YES completion:nil];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                             
                         }];
    [alert addAction:ok];
    
}

-(void)getRepaierNewLocations {
    
   __block CLLocationCoordinate2D newLocation;
        NSString * repaierId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"Accept"] valueForKey:@"repairer_id"];
    GetRepairerLocationsRequest * mGetRepairerLocationsRequest = [[GetRepairerLocationsRequest alloc] initWithApiKey:_userAccount.apiKey andRepairerId:repaierId];
    
    [mGetRepairerLocationsRequest postWithSuccess:^(NSDictionary *response) {
        
        NSError * LocationError;
        NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *repaierLocationDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&LocationError];
        NSMutableDictionary* mRepaierLocationDictionary = (NSMutableDictionary*)[repaierLocationDictionary objectForKey:@"message"];
        
        newLocation = CLLocationCoordinate2DMake([[mRepaierLocationDictionary objectForKey:@"lat"] doubleValue], [[mRepaierLocationDictionary objectForKey:@"lng"] doubleValue]);
        
       // [self.mapView convertPoint:postValue fromCoordinateSpace:mark.position];
//        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
//        
////        animation.fromValue = [NSValue valueWithMKCoordinate:oldLocation];
////        animation.toValue = [NSValue valueWithMKCoordinate:newLocation];
//        
//        CGPoint pointfrom = [_mapView.projection pointForCoordinate: oldLocation];
//        CGPoint pointto = [_mapView.projection pointForCoordinate: newLocation];
//        animation.fromValue = [NSValue valueWithCGPoint:pointfrom];
//        animation.toValue = [NSValue valueWithCGPoint:pointto];
//        animation.duration = 7.0;
//        animation.delegate = self;
//        animation.fillMode = kCAFillModeForwards;
//        //[self.layer removeAllAnimations];
//        [_repaierMarker.layer addAnimation:animation forKey:@"position"];

        
        //newLocation = CLLocationCoordinate2DMake(40.000000, 43.999999);
        [CATransaction begin];
        [CATransaction setAnimationDuration:7.0];
        _repaierMarker.position = newLocation;
        [CATransaction commit];
        
        [self showAllMarkers:[NSMutableArray arrayWithObjects:_repaierMarker,_mCustomerMarker, nil]];
        oldLocation = newLocation;
        
        
        NSLog(@"mGetRepairerLocationsRequest = %@",response);
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"Error = %@",error.description);
    }];
   
}
-(void)moveRepairer:(BOOL)isExistCutomer {
    
   [self getRepaierNewLocations];
    
    
//    NSString * repaierId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"Accept"] valueForKey:@"repairer_id"];
//    NSLog(@"\n lat: = %@    \n  long = %@ \n",[[[NSUserDefaults standardUserDefaults] objectForKey:@"Accept"] valueForKey:@"repairer_lat"],[[[NSUserDefaults standardUserDefaults] objectForKey:@"Accept"] valueForKey:@"repairer_lng"]);
//
//        
//    
//            
//    
//            mNewLat = [[[[NSUserDefaults standardUserDefaults] objectForKey:@"Accept"] valueForKey:@"repairer_lat"] doubleValue];
//            mNewLong = [[[[NSUserDefaults standardUserDefaults] objectForKey:@"Accept"] valueForKey:@"repairer_lng"] doubleValue];
//            NSLog(@"Lat : = %@    Lng : = %@",[[mMessageDictionary allValues] objectAtIndex:0],[[mMessageDictionary allValues] objectAtIndex:1]);
//            if (mNewLat && mNewLong) {
//                _mLocation = CLLocationCoordinate2DMake(mNewLat,mNewLong);
//                [self geocoderMethod:mNewLat longitude:mNewLong];
////                CLLocation * location1 = [[CLLocation alloc] initWithLatitude:[mLatitude doubleValue] longitude:[mLongitute doubleValue]];
////                CLLocation * location2 = [[CLLocation alloc] initWithLatitude:[[mMessageDictionary valueForKey:@"lat"] doubleValue] longitude:[[mMessageDictionary valueForKey:@"lng"] doubleValue]];
////                CGFloat distance = [location1 distanceFromLocation:location2];
//                
//                [self recenterMapToPlacemarkForRepaiers:_mLocation];
//                GetDistanceTimeRequest * mGetDistanceTimeRequest = [[GetDistanceTimeRequest alloc] initWithCustomerLocation:CLLocationCoordinate2DMake([self.mMapView centerCoordinate].latitude, [self.mMapView centerCoordinate].longitude) andRepaierLocation:CLLocationCoordinate2DMake(mNewLat, mNewLong)];
//                
//                [mGetDistanceTimeRequest getWithSuccess:^(NSObject *response) {
//                    NSLog(@"response : %@",response);
//                    
//                    NSDictionary * dictionary = (NSDictionary*)response;
//                    
//                    nearTimeStr = [[[[[[dictionary valueForKey:@"rows"] objectAtIndex:0] valueForKey:@"elements"] objectAtIndex:0] valueForKey:@"duration"] valueForKey:@"text"];
//                    self.mCustomerTime.text = nearTimeStr;
//                    NSLog(@"nearTimeStr = %@",nearTimeStr);
//                    [ProgressHUD dismiss];
//                    if (!isExistCutomer) {
//                        [self recenterMapToPlacemark:CLLocationCoordinate2DMake([mLatitude doubleValue], [mLongitute doubleValue])]; //new
//
//                    }
//                    
//                    if( _mMapView.annotations.count==1)
//                    {
//                        NSLog(@"no annotations");
//                        [self addAnnotation1:_mLocation];
//                    }
//                    
//                   // [self setMapLocation:_mLocation];
//                    if (!isDone) {
//                        [self performSelector:@selector(moveAnotation) withObject:nil afterDelay:7];
//
//                    }
//                    
//                } failure:^(NSError *error, NSInteger responseCode) {
//                    NSLog(@"error : %@",error.description);
//                }];
//
//               
//                
//            }
//            //            [NSTimer scheduledTimerWithTimeInterval:7 target:self selector:@selector(moveAnotation) userInfo:nil repeats:YES];
//        
//        
    
    }
-(void)addAnnotation1:(CLLocationCoordinate2D) cllocationCoordinate2D{
    CLLocationCoordinate2D location = cllocationCoordinate2D;
    
    RepaierAnnotation = [[MapAnotation alloc]init];
    RepaierAnnotation.coordinate = location;
    
    
   
    RepaierAnnotation.subtitle = @"";
    double fff   = RepaierAnnotation.coordinate.latitude;
    double lll =  RepaierAnnotation.coordinate.longitude;
    [self.mMapView addAnnotation:RepaierAnnotation];
    if([self checkInternetConnection]){
         [self geocoderMethod:fff longitude:lll];
    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }
    

   
    
    
    
}
-(void) setMapLocation :(CLLocationCoordinate2D)location{
    CLLocationCoordinate2D newLocation = location;
    MKCoordinateSpan span;
    span.latitudeDelta =  0.01;
    span.longitudeDelta = 0.01;
    MKCoordinateRegion region;
    region.center = newLocation;
    region.span = span;
   // [self.mMapView setRegion:region animated:NO];
}

- (void) moveAnotation{
    CLLocationCoordinate2D newLocation =  CLLocationCoordinate2DMake(mNewLat, mNewLong);
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:7];
    RepaierAnnotation.coordinate = newLocation;
    MKCoordinateRegion viewRegion =MKCoordinateRegionMakeWithDistance(newLocation, 1000, 1000);
    MKCoordinateRegion adjustedRegion = [_mMapView regionThatFits:viewRegion];
    [_mMapView setRegion:adjustedRegion animated:YES];
    
    
    [UIView commitAnimations];
    
    
}

-(BOOL)checkInternetConnection{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        mIsConnected = NO;
        NSLog(@"There IS NO internet connection");
    } else {
        NSLog(@"There IS internet connection");
        mIsConnected = YES;
    }
    return mIsConnected;
}
-(void)showAlertWithMessage:(NSString*)message andTitle:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}

- (void)marker:(GMSMarker*)mark setPosition : (CLLocationCoordinate2D)cordinates;
{
   /* NSLog(@"set position");
    [self.mapView convertPoint:postValue fromCoordinateSpace:mark.position];
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    
    animation.fromValue = [NSValue valueWithCGPoint:posValue];
    animation.toValue = [NSValue valueWithCGPoint:andValue];
    animation.duration = 1.0;
    //animation.delegate = self;
    animation.fillMode = kCAFillModeForwards;
    //[self.layer removeAllAnimations];
    [mark.layer addAnimation:animation forKey:@"position"];
    */
   
    
    //NSLog(@"setPosition ANIMATED %x from (%f, %f) to (%f, %f)", self, self.center.x, self.center.y, toPos.x, toPos.y);
    
    
}
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if (flag) {
        CABasicAnimation *blink = [CABasicAnimation animationWithKeyPath:kGMSMarkerLayerOpacity];
        blink.fromValue = [NSNumber numberWithFloat:1.0];
        blink.toValue = [NSNumber numberWithFloat:0.0];
        blink.duration = 1.5;
        blink.repeatCount = 10000;
        [blink setDelegate:self];
        [clockArrow.layer addAnimation:blink forKey:@"blinkmarker"];
    }
}
- (void) runSpinAnimationOnView:(UIView*)view
                       duration:(CGFloat)duration
                      rotations:(CGFloat)rotations
                         repeat:(float)repeat {
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat:  M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
}

#pragma mark -GMSAutocmplate


- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didAutocompleteWithPlace:(GMSPlace *)place {
    
    
    NSLog(@"place = %@",place);
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    //[self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
   // [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


@end
