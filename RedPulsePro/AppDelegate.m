 //
//  AppDelegate.m
//  RedPulsePro
//
//  Created by MacMini on 10/11/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "NavigationController.h"
#import "RootViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <notify.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "SPGooglePlacesPlaceDetailQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompleteUtilities.h"

#import <dlfcn.h>
#import <mach/port.h>
#import <mach/kern_return.h>

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


@import GoogleMaps;

NSString * const ShowNotification = @"ShowNotification";
NSString * const ShowAcceptNotification = @"ShowAcceptNotification";
NSString * const ShowDoneNotification = @"ShowDoneNotification";


@interface AppDelegate () {
    BOOL bDeviceLocked;
    NSArray * arrayWithServices;
}

@end

@implementation AppDelegate

//-(void)registerAppforDetectLockState {
//    
//    int notify_token;
//    notify_register_dispatch("com.apple.springboard.lockstate", &notify_token,dispatch_get_main_queue(), ^(int token) {
//        
//        uint64_t state = UINT64_MAX;
//        notify_get_state(token, &state);
//        
//        if(state == 0) {
//            NSLog(@"unlock device");
//        } else {
//            NSLog(@"lock device");
//        }
//        
//        NSLog(@"com.apple.springboard.lockstate = %llu", state);
//        UILocalNotification *notification = [[UILocalNotification alloc]init];
//        notification.repeatInterval = NSDayCalendarUnit;
//        [notification setAlertBody:@"Hello world!! I come becoz you lock/unlock your device :)"];
//        notification.alertAction = @"View";
//        notification.alertAction = @"Yes";
//        [notification setFireDate:[NSDate dateWithTimeIntervalSinceNow:1]];
//        notification.soundName = UILocalNotificationDefaultSoundName;
//        [notification setTimeZone:[NSTimeZone  defaultTimeZone]];
//        
//        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
//        
//    });
//}

//======================================================================


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    NSLog(@"UUID:: %@", uniqueIdentifier);
    
    [GMSPlacesClient provideAPIKey:@"AIzaSyC3uH8npFnoPXYC2m6F_duGf9GK0RNMN5I"];
    [GMSServices provideAPIKey:@"AIzaSyCwgPOi6ywd9NBLRVaWobWHJnIJDuWGQH4"];
    
    _isBackFromMap = NO;
    
    //[self registerAppforDetectLockState];
    _problemTextField = @"";
    _historyCount = 0;
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    if (![[NSUserDefaults standardUserDefaults] valueForKey:@"isLogin"]){
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLogin"];
    }
    _BASE_URL = @"http://api-red.expandis.fr/";
    application.applicationIconBadgeNumber = 0;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 && [[[UIDevice currentDevice] systemVersion] floatValue] < 10.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }else if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
                
                  }
        }];
    }else {
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    

    if( SYSTEM_VERSION_LESS_THAN( @"10.0" ) )
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
//        if( options != nil )
//        {
//            NSLog( @"registerForPushWithOptions:" );
//        }
    }
    else
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
         {
             if( !error )
             {
                 [[UIApplication sharedApplication] registerForRemoteNotifications]; // required to get the app to do anything at all about push notifications
                 NSLog( @"Push registration success." );
             }
             else
             {
                 NSLog( @"Push registration FAILED" );
                 NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                 NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );  
             }  
         }];  
    }
       // Override point for customization after application launch.
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    
//    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
//    MainViewController *mainViewController = [storyboard instantiateInitialViewController];
//    mainViewController.rootViewController = navigationController;
//    [mainViewController setupWithPresentationStyle:LGSideMenuPresentationStyleSlideAbove type:3];
    
//    UIWindow *window = [UIApplication sharedApplication].delegate.window;
//    
//    window.rootViewController = mainViewController;
//    
//    [UIView transitionWithView:window
//                      duration:0.3
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:nil
//                    completion:nil];

    
    
    return YES;
}

//
//  AppDelegate.m
//  LGSideMenuControllerDemo
//
//  Created by Grigory Lutkov on 28.07.15.
//  Copyright (c) 2015 Grigory Lutkov. All rights reserved.
//






-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    //    [[NSNotificationCenter defaultCenter] postNotificationName:ShowNotification
    //                                                        object:nil
    //                                                      userInfo:nil];
    
    NSString *searchTerm = @"repairer_data";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF LIKE[cd] %@", searchTerm];
    NSArray *filtered = [[notification.request.content.userInfo allKeys] filteredArrayUsingPredicate:predicate];
    
    
    if ([filtered count] > 0) {
        
        
    }else if ([[[notification.request.content.userInfo allValues] objectAtIndex:1] count] > 0) {
        
        self.AcceptNotData = [notification.request.content.userInfo valueForKey:@"repairer_data"];
        NSArray* array = [notification.request.content.userInfo objectForKey:@"order_ids"];
        NSString * servicesString = @"";
        self.customerNameFromNot = [NSString stringWithFormat:@"%@ %@",[[notification.request.content.userInfo objectForKey:@"customer_data"] valueForKey:@"name"],[[notification.request.content.userInfo objectForKey:@"customer_data"] valueForKey:@"surname"]];
        self.customerPhoneNumberFromNot = [[notification.request.content.userInfo objectForKey:@"customer_data"] valueForKey:@"phone"];
        arrayWithServices = [notification.request.content.userInfo objectForKey:@"service_details"];
        for (int i = 0; i < arrayWithServices.count; i++) {
            servicesString = [servicesString stringByAppendingString:@"\n"];
            servicesString = [servicesString stringByAppendingString:[arrayWithServices objectAtIndex:i]];
        }
        self.notifierOrderId = [NSArray arrayWithArray:array];
        self.alertMessage = [[notification.request.content.userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        self.alertText = servicesString;

    }
    
    NSLog(@"Userinfo %@",notification.request.content.userInfo);
    NSString *audioPath = [[NSBundle mainBundle] pathForResource:@"arpeggio" ofType:@"mp3"];
    
    NSData *audioData = [NSData dataWithContentsOfFile:audioPath];
    NSError *err = nil;
    audioPlayer = [[AVAudioPlayer alloc] initWithData:audioData error:&err];
    
    if(err) NSLog(@"%@", [err description]);
    
    self.alertMessage = [[notification.request.content.userInfo valueForKey:@"aps"] valueForKey:@"alert"];//Your Order accepted!
    [audioPlayer setVolume:100];
    [audioPlayer setNumberOfLoops:0]; //loop for ever
    [audioPlayer play];
    
    if ([self.alertMessage isEqualToString:@"Your Order accepted!"]) {
        self.AcceptNotData = [notification.request.content.userInfo valueForKey:@"repairer_data"];
         self.alertMessage = @"Your Order accepted!";
        [[NSUserDefaults standardUserDefaults] setObject:self.AcceptNotData forKey:@"Accept"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:ShowAcceptNotification
                                                            object:nil
                                                          userInfo:nil];
    }else if ([self.alertMessage isEqualToString:@"Your order done!"]){
         self.alertMessage = @"Your order done!";
        [[NSNotificationCenter defaultCenter] postNotificationName:ShowDoneNotification
                                                            object:nil
                                                          userInfo:nil];
        

    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ShowNotification
                                                        object:nil
                                                      userInfo:nil];
    
    completionHandler(UNNotificationPresentationOptionAlert);
}

/*
 
 "repairer_data" =     {
 "repairer_id" = 5;
 "repairer_lat" = "40.7866337384";
 "repairer_lng" = "43.8382301666";
 "repairer_name" = Sasha;
 "repairer_phone" = "+33 1 23 45 67 89";
 "repairer_surname" = Stepanyan;
 };
 
 aps =     {
 alert = "Your Order accepted!";
 badge = 1;
 sound = default;
 };
 "repairer_data" =     {
 "repairer_id" = 1;
 "repairer_lat" = fdgbdfg;
 "repairer_lng" = dfgdfg;
 "repairer_name" = gfhnf;
 "repairer_phone" = dfgdfg;
 "repairer_surname" = fgbdfgb;
 };
 
 */
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
    NSLog(@"Userinfo %@",response.notification.request.content.userInfo);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ShowNotification
                                                        object:nil
                                                      userInfo:nil];
    
}


-(void) setupAVPlayerForURL: (NSURL*) url {
    AVAsset *asset = [AVURLAsset URLAssetWithURL:url options:nil];
    AVPlayerItem *anItem = [AVPlayerItem playerItemWithAsset:asset];
    
    _mPlayer = [AVPlayer playerWithPlayerItem:anItem];
    [_mPlayer addObserver:self forKeyPath:@"status" options:0 context:nil];
    NSLog(@"Notification   from  :setupAVPlayerForURL");
}
-(void)setVolume:(float)vol
{
    [audioPlayer setVolume:vol];
}
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    // on finished event
}
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    //self.textView.text = [userInfo description];
    // We can determine whether an application is launched as a result of the user tapping the action
    // button or whether the notification was delivered to the already-running application by examining
    // the application state.
    if ([userInfo objectForKey:@"order_ids"] != nil) {
        // application.applicationIconBadgeNumber = 1;
        NSArray* array = [userInfo objectForKey:@"order_ids"];
        NSString * servicesString = @"";
        self.customerNameFromNot = [NSString stringWithFormat:@"%@ %@",[[userInfo objectForKey:@"customer_data"] valueForKey:@"name"],[[userInfo objectForKey:@"customer_data"] valueForKey:@"surname"]];
        self.customerPhoneNumberFromNot = [[userInfo objectForKey:@"customer_data"] valueForKey:@"phone"];
        arrayWithServices = [userInfo objectForKey:@"service_details"];
        for (int i = 0; i < arrayWithServices.count; i++) {
        servicesString = [servicesString stringByAppendingString:@"\n"];
         servicesString = [servicesString stringByAppendingString:[arrayWithServices objectAtIndex:i]];
        }
        self.notifierOrderId = [NSArray arrayWithArray:array];
        self.alertMessage = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        self.alertText = servicesString;
        //        NSString *audioPath = [[NSBundle mainBundle] pathForResource:@"arpeggio" ofType:@"mp3"];
        //
        //        NSData *audioData = [NSData dataWithContentsOfFile:audioPath];
        //        NSError *err = nil;
        //        audioPlayer = [[AVAudioPlayer alloc] initWithData:audioData error:&err];
        //
        //        if(err) NSLog(@"%@", [err description]);
        //
        //        [audioPlayer setVolume:100];
        //        [audioPlayer setNumberOfLoops:0]; //loop for ever
        //        [audioPlayer play];
        
    }
    if (application.applicationState == UIApplicationStateActive)
    {
        self.AcceptNotData = [userInfo valueForKey:@"repairer_data"];
        self.alertMessage = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        [application registerForRemoteNotifications];
        
     
        [[NSNotificationCenter defaultCenter] postNotificationName:ShowNotification
                                                            object:nil
                                                          userInfo:nil];
        
        
        NSString *audioPath = [[NSBundle mainBundle] pathForResource:@"arpeggio" ofType:@"mp3"];
        
        NSData *audioData = [NSData dataWithContentsOfFile:audioPath];
        NSError *err = nil;
        audioPlayer = [[AVAudioPlayer alloc] initWithData:audioData error:&err];
        
        if(err) NSLog(@"%@", [err description]);
        
        [audioPlayer setVolume:100];
        [audioPlayer setNumberOfLoops:0]; //loop for ever
        [audioPlayer play];
    }else {
        NSLog(@"");
             [[NSNotificationCenter defaultCenter] postNotificationName:ShowNotification
                                                            object:nil
                                                          userInfo:nil];
        
    }
    
    if ([self.alertMessage isEqualToString:@"Your Order accepted!"]) {
        self.AcceptNotData = [userInfo valueForKey:@"repairer_data"];
        self.alertMessage = @"Your Order accepted!";
        [[NSUserDefaults standardUserDefaults] setObject:self.AcceptNotData forKey:@"Accept"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:ShowAcceptNotification
                                                            object:nil
                                                          userInfo:nil];
    }else if ([self.alertMessage isEqualToString:@"Your order done!"]){
        self.alertMessage = @"Your order done!";
        [[NSNotificationCenter defaultCenter] postNotificationName:ShowDoneNotification
                                                            object:nil
                                                          userInfo:nil];
        
        
    }
    
    NSString *audioPath = [[NSBundle mainBundle] pathForResource:@"arpeggio" ofType:@"mp3"];
    
    NSData *audioData = [NSData dataWithContentsOfFile:audioPath];
    NSError *err = nil;
    audioPlayer = [[AVAudioPlayer alloc] initWithData:audioData error:&err];
    
    if(err) NSLog(@"%@", [err description]);
    
    [audioPlayer setVolume:100];
    [audioPlayer setNumberOfLoops:0]; //loop for ever
    [audioPlayer play];
    
    NSLog(@"Notification   from  :didReceiveRemoteNotification");
}

- (void)application:(UIApplication *)application didChangeStatusBarOrientation:(UIInterfaceOrientation)oldStatusBarOrientation {
    application.statusBarHidden = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && UIInterfaceOrientationIsLandscape(application.statusBarOrientation));
    NSLog(@"Notification   from  :didChangeStatusBarOrientation");
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
    NSLog(@"Did Fail to Register for Remote Notifications");
    NSLog(@"%@, %@", error, error.localizedDescription);
    NSLog(@"Notification   from  :didFailToRegisterForRemoteNotificationsWithError");
    
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings // NS_AVAILABLE_IOS(8_0);
{
    NSLog(@"Notification   from  :didRegisterUserNotificationSettings");
//#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
//    
//    if ([UIApplication respondsToSelector:@selector(registerUserNotificationSettings:)]) {
//        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge|UIUserNotificationTypeAlert|UIUserNotificationTypeSound
//                                                                                 categories:nil];
//        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
//        [[UIApplication sharedApplication] registerForRemoteNotifications];
//    } else {
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
//         UIRemoteNotificationTypeBadge |
//         UIRemoteNotificationTypeAlert |
//         UIRemoteNotificationTypeSound];
//        
//    }
//#else
//    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
//     UIRemoteNotificationTypeBadge |
//     UIRemoteNotificationTypeAlert |
//     UIRemoteNotificationTypeSound];
//    
//#endif
   // [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"Notification   from  :didRegisterForRemoteNotificationsWithDeviceToken");
    NSString *device = [deviceToken description];
    device = [device stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    device = [device stringByReplacingOccurrencesOfString:@" " withString:@""];
    _gDeviceToken = device;
    NSLog(@"My device is: %@", device);}
@end
