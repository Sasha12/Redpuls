//
//  RepairCollectionViewCell.h
//  RedPulsePro
//
//  Created by MacMini on 12/28/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepairCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *rep_Image;
@property (weak, nonatomic) IBOutlet UILabel *rep_nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rep_priceLabel;
@property (weak, nonatomic) IBOutlet UIView *imageBackgroundView;
@property () BOOL isSelected;
@end
