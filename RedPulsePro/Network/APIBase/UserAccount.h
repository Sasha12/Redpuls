//
//  UserAccount.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/8/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface UserAccount : RLMObject


@property NSString* apiKey;
@property NSString* usernName;
@property NSString* deviceToken;
@property NSInteger role;
@property BOOL      isUserLogined;
@property NSString* prym;

-(id)initWithUserName:(NSString*) userName
             andToken:(NSString*) token
            andApiKey:(NSString*)apiKey
              andRole:(NSInteger)role
     andIsUserLogined:(BOOL)isUserLogined;


- (void) createOrUpdateCustomerAccountInStorage;

@end
RLM_ARRAY_TYPE(UserAccount)
