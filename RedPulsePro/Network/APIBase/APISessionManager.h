//
//  APISessionManager.h
//  Mente
//
//  Created by Richard Muscat on 11/12/2015.
//  Copyright © 2015 AAT. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface APISessionManager : AFHTTPSessionManager

+ (id)sharedManager;

@end
