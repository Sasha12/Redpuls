//
//  APIPost.m
//  Mente
//
//  Created by Richard Muscat on 11/12/2015.
//  Copyright © 2015 AAT. All rights reserved.
//

#import "APIPost.h"
//#import "GlobalObjects.h"
//#import "AuthCredentials.h"
//#import "AuthorizationManager.h"

@implementation APIPost

-(void)postWithSuccess:(void (^)(NSDictionary  *response))success
               failure:(void (^)(NSError *error, NSInteger responseCode))failure
{
    
   

    //self.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/sopa+xml",@"application/json",@"text/html",nil];
    self.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    //APISEND_CONTACTGROUPKEY
    [self POST:_relativeURL parameters:_requestParameters progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"uploadProgress: %@", uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"JSON: %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        NSInteger statuscode = response.statusCode;
        NSLog(@"Error in request first attempt: %@", [error localizedDescription]);
        failure(error,statuscode);
    }];
    
//    [self POST:_relativeURL parameters:_requestParameters
//       success:^(NSURLSessionDataTask *task, id responseObject)
//     {
//         NSLog(@"JSON: %@", responseObject);
//         success(responseObject);
//     }
//       failure:^(NSURLSessionDataTask *task, NSError *error) {
//           NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
//           NSInteger statuscode = response.statusCode;
//           NSLog(@"Error in request first attempt: %@", [error localizedDescription]);
////           // If invalid token re-authorize
////           if(statuscode == 401)
////           {
////               AuthCredentials* credentials = [AuthCredentials get];
////               
////               // If available authorize
////               if(credentials!=nil)
////               {   NSLog(@"Found Credentials - Re-Authorizing");
////                   [AuthorizationManager authorise:credentials.username withPasskey:credentials.password andIsSignedIn:YES andSuccess:^(MenteDevice* device, BOOL isPin) {
////                       //relaunch request
////                       [_requestParameters setObject:token forKey:@"Token"];
////                       [self POST:_relativeURL parameters:_requestParameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
////                           success(responseObject);
////                       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
////                           NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
////                           NSInteger statuscode = response.statusCode;
////                           NSLog(@"Error in Re-launching of request: %@", [error localizedDescription]);
////                           failure(error,statuscode);
////                       }];
////                   } andFailure:^(NSError *error, NSInteger responseCode) {
////                       NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
////                       NSInteger statuscode = response.statusCode;
////                       NSLog(@"Error in Re-Authorize: %@", [error localizedDescription]);
////                       failure(error,statuscode);
////                   }];
////               }
////           }
////           else
////           {
////               failure(error,statuscode);
////           }
//           
//           failure(error,statuscode);
//       }];
}

-(void)authoriseWithSuccess:(void (^)(NSDictionary  *response))success
                    failure:(void (^)(NSError *error, NSInteger responseCode))failure
{
    self.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    
    [self POST:_relativeURL parameters:_requestParameters
       success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         success(responseObject);
     }
       failure:
     ^(NSURLSessionDataTask *task, NSError *error) {
         NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
         NSInteger statuscode = response.statusCode;
         NSLog(@"Error: %@", [error localizedDescription]);
         failure(error,statuscode);
     }];
}

@end
