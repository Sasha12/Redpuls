//
//  UserAccount.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/8/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "UserAccount.h"

@implementation UserAccount


-(id)initWithUserName:(NSString*) userName
            andToken:(NSString*) token
           andApiKey:(NSString*)apiKey
             andRole:(NSInteger)role
    andIsUserLogined:(BOOL)isUserLogined

{
       if(!self) return nil;
    
    self.usernName = userName;
    self.deviceToken = token;
    self.apiKey = apiKey;
    self.role = role;
    self.isUserLogined = isUserLogined;
    self.prym = @"1";
    
    return self;

}


+ (NSString *)primaryKey {
    return @"prym";
}


- (void) createOrUpdateCustomerAccountInStorage {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [UserAccount createOrUpdateInRealm:realm withValue:self];
    [realm commitWriteTransaction];
}

@end
