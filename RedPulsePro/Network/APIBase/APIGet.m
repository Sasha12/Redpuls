//
//  APIGet.m
//  Mente
//
//  Created by Richard Muscat on 14/12/2015.
//  Copyright © 2015 AAT. All rights reserved.
//

#import "APIGet.h"
//#import "AuthCredentials.h"
//#import "AuthorizationManager.h"
//#import "GlobalObjects.h"

@implementation APIGet


-(void)getWithSuccess:(void (^)(NSObject  *response))success
               failure:(void (^)(NSError *error, NSInteger responseCode))failure
{
   // if (isGetUsers) {
//        self.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"multipart/form-data",nil];
//        isGetUsers = NO;
   // }else{
    self.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
   // }
    self.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers & NSJSONReadingMutableLeaves & NSJSONReadingAllowFragments];
    
   // self.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];

    [self GET:_relativeURL parameters:_requestParameters
       success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         success(responseObject);
     }
       failure:
     ^(NSURLSessionDataTask *task, NSError *error) {
         NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
         NSInteger statuscode = response.statusCode;
         NSLog(@"Error: %@", [error localizedDescription]);
//         // If invalid token re-authorize
//         if(statuscode == 401)
//         {
//              AuthCredentials* credentials = [AuthCredentials get];
//             
//             // If available authorize
//             if(credentials!=nil)
//             {   NSLog(@"Found Credentials - Re-Authorizing");
//                 [AuthorizationManager authorise:credentials.username withPasskey:credentials.password andIsSignedIn:YES andSuccess:^(MenteDevice* device, BOOL isPin) {
//                     //relaunch request
//                     [_requestParameters setObject:token forKey:@"Token"];
//                     [self GET:_relativeURL parameters:_requestParameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//                         success(responseObject);
//                     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//                         NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
//                         NSInteger statuscode = response.statusCode;
//                         NSLog(@"Error in Re-launching of request: %@", [error localizedDescription]);
//                         failure(error,statuscode);
//                     }];
//                 } andFailure:^(NSError *error, NSInteger responseCode) {
//                     
//                     NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
//                     NSInteger statuscode = response.statusCode;
//                     NSLog(@"Error in Re-Authorize: %@", [error localizedDescription]);
//                     failure(error,statuscode);
//                 }];
//             }else {
//                 failure(error,statuscode);
//             }
//             
//         }else{
//             failure(error,statuscode);
//         }
         
         failure(error,statuscode);
     }];
}





@end
