//
//  APIPut.m
//  Mente
//
//  Created by Richard Muscat on 11/01/2016.
//  Copyright © 2016 AAT. All rights reserved.
//

#import "APIPut.h"
//#import "AuthCredentials.h"
//#import "AuthorizationManager.h"
//#import "GlobalObjects.h"


@implementation APIPut

-(void)putWithSuccess:(void (^)(NSDictionary  *response))success
               failure:(void (^)(NSError *error, NSInteger responseCode))failure
{
    self.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    
    [self PUT:_relativeURL parameters:_requestParameters
       success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         success(responseObject);
     }
       failure:^(NSURLSessionDataTask *task, NSError *error) {
           NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
           NSInteger statuscode = response.statusCode;
           NSLog(@"Error in request first attempt: %@", [error localizedDescription]);
//           // If invalid token re-authorize
//           if(statuscode == 401)
//           {
//               AuthCredentials* credentials = [AuthCredentials get];
//               
//               // If available authorize
//               if(credentials!=nil)
//               {   NSLog(@"Found Credentials - Re-Authorizing");
//                   /*
//                   [AuthorizationManager authorise:credentials.serialNumber withPasskey:credentials.passKey andIsSignedIn:YES andSuccess:^(MenteDevice* device, BOOL isPin) {
//                       //relaunch request
//                       [_requestParameters setObject:token forKey:@"Token"];
//                       [self POST:_relativeURL parameters:_requestParameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//                           success(responseObject);
//                       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//                           NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
//                           NSInteger statuscode = response.statusCode;
//                           NSLog(@"Error in Re-launching of request: %@", [error localizedDescription]);
//                           failure(error,statuscode);
//                       }];
//                   } andFailure:^(NSError *error, NSInteger responseCode) {
//                       NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
//                       NSInteger statuscode = response.statusCode;
//                       NSLog(@"Error in Re-Authorize: %@", [error localizedDescription]);
//                       failure(error,statuscode);
//                   }];*/
//               }
//           }
//           else
//           {
//               failure(error,statuscode);
//           }
           
           failure(error,statuscode);
       }];
}
@end
