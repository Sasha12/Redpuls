//
//  APIPost.h
//  Mente
//
//  Created by Richard Muscat on 11/12/2015.
//  Copyright © 2015 AAT. All rights reserved.
//

#import "APISessionManager.h"

@interface APIPost : APISessionManager

@property NSString *relativeURL;
@property NSMutableDictionary *requestParameters;

-(void)postWithSuccess:(void (^)(NSDictionary  *response))success
               failure:(void (^)(NSError *error, NSInteger responseCode))failure;

-(void)authoriseWithSuccess:(void (^)(NSDictionary  *response))success
               failure:(void (^)(NSError *error, NSInteger responseCode))failure;
@end