//
//  APIPut.h
//  Mente
//
//  Created by Richard Muscat on 11/01/2016.
//  Copyright © 2016 AAT. All rights reserved.
//

#import "APISessionManager.h"

@interface APIPut : APISessionManager

@property NSString *relativeURL;
@property NSMutableDictionary *requestParameters;

-(void)putWithSuccess:(void (^)(NSDictionary  *response))success
               failure:(void (^)(NSError *error, NSInteger responseCode))failure;



@end
