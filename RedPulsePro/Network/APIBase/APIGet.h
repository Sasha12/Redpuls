//
//  APIGet.h
//  Mente
//
//  Created by Richard Muscat on 14/12/2015.
//  Copyright © 2015 AAT. All rights reserved.
//

#import "APISessionManager.h"

@interface APIGet : APISessionManager

@property NSString *relativeURL;
@property NSMutableDictionary *requestParameters;

-(void)getWithSuccess:(void (^)(NSObject  *response))success failure:(void (^)(NSError *error, NSInteger responseCode))failure;



@end
