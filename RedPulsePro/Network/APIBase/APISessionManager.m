//
//  APISessionManager.m
//  Mente
//
//  Created by Richard Muscat on 11/12/2015.
//  Copyright © 2015 AAT. All rights reserved.
//

#import "APISessionManager.h"
//#import "GlobalObjects.h"

@implementation APISessionManager

- (id)init {
    self = [super initWithBaseURL:[NSURL URLWithString:@"Base URL String"]];
    if(!self) return nil;
    
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    return self;
}

+ (id)sharedManager {
    static APISessionManager *_APISessionManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _APISessionManager = [[self alloc] init];
    });
    
    return _APISessionManager;
}




@end
