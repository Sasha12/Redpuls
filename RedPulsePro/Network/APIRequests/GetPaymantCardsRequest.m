//
//  GetPaymantCardsRequest.m
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "GetPaymantCardsRequest.h"
#import "AppDelegate.h"

@implementation GetPaymantCardsRequest

- (id)initWithApiKey:(NSString*)apiKey withCustomerId:(NSString*)customerId
{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",customerId,@"customer_id",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@",@"http://api-red.expandis.fr/v2/payments"];
    }
    return self;
    
}

@end

/*
 http://api-red.expandis.fr/v2/payments
 api_key
 customer_id

*/
