//
//  UpdateLocationsRecuest.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/9/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "UpdateLocationsRecuest.h"
#import "AppDelegate.h"

@implementation UpdateLocationsRecuest

- (id)initWithApiKey:(NSString*)apiKey andUserLatitute:(NSString*)userLatitute andUserLongitute:(NSString*)userLongitute andUserId:(NSString*)userID{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:userID,@"user_id",
                                  apiKey,@"api_key",
                                  userLatitute,@"lat",
                                  userLongitute,@"lng",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"users/updatelatlng"];
    }
    return self;
    
}

@end
