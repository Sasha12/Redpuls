//
//  GetCustomerAddressRequest.m
//  RedPulsePro
//
//  Created by MacMini on 12/9/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "GetCustomerAddressRequest.h"
#import "AppDelegate.h"

@implementation GetCustomerAddressRequest

- (id)initWithApiKey:(NSString*)apiKey withCustomerId:(NSString*)customerId
{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",customerId,@"customer_id",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",@"http://api-red.expandis.fr/v2/",@"customers/address"];
    }
    return self;
    
}

@end


/*
 http://api-red.expandis.fr/v2/customers/address

*/
