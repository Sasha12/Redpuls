//
//  GetOrderViewRequest.h
//  RedPulsePro
//
//  Created by MacMini on 11/20/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "APIPost.h"

@interface GetOrderViewRequest : APIPost

- (id)initWithApiKey:(NSString*)apiKey withOrderId:(NSString*)orderId;

@end
