//
//  GetBrandFiltr.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/28/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "APIPost.h"
#import "AppDelegate.h"

@interface GetBrandFiltr : APIPost
- (id)initWithApiKey:(NSString*)apiKey andBrandId:(NSString*) brandId;
@end
