//
//  UpdateTokenRequest.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/9/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "APIPost.h"

@interface UpdateTokenRequest : APIPost

- (id)initWithOldToken:(NSString*)oldToken andNewToken:(NSString*)newToken andApiKey:(NSString*)apiKey;


@end
