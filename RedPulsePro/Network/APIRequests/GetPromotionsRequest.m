//
//  GetPromotionsRequest.m
//  RedPulsePro
//
//  Created by MacMini on 11/27/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "GetPromotionsRequest.h"
#import "AppDelegate.h"

@implementation GetPromotionsRequest

- (id)initWithApiKey:(NSString*)apiKey withCustomerId:(NSString*)customerId
{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",customerId,@"customer_id",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",@"http://api-red.expandis.fr/v2/",@"promotions"];
    }
    return self;
    
}


@end

/*
 /promotions
 api_key
 customer_id
 http://api-red.expandis.fr/v2/promotions

*/
