//
//  GetServicesRequest.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/2/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "GetServicesRequest.h"
#import "AppDelegate.h"
@implementation GetServicesRequest



- (id)initWithApiKey:(NSString *)apiKey
{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;

        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",nil];
        
        self.relativeURL = @"http://api-red.expandis.fr/v2/services";//[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"services"];
    }
    return self;
    
}



@end
