//
//  GetRepairerdatabycustomerRequest.m
//  RedPulse
//
//  Created by MacMini on 10/5/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "GetRepairerdatabycustomerRequest.h"
#import "AppDelegate.h"

@implementation GetRepairerdatabycustomerRequest

- (id)initWithApiKey:(NSString*)apiKey andCustomerId:(NSString*) customerId{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",customerId,@"customer_id", nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"orders/repairerdatabycustomer"];
    }
    return self;
    
}

@end


/*
customer_id, api_key
*/
