//
//  SetDefoultCardRequest.m
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "SetDefoultCardRequest.h"
#import "AppDelegate.h"

@implementation SetDefoultCardRequest

- (id)initWithApiKey:(NSString*)apiKey withCustomerId:(NSString*)customerId cardId:(NSString*)cardId
{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",customerId,@"customer_id",cardId,@"card_id",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@",@"http://api-red.expandis.fr/v2/payments/default"];
    }
    return self;
    
}


@end
