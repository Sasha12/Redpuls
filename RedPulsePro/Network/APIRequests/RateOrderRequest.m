//
//  RateOrderRequest.m
//  RedPulsePro
//
//  Created by MacMini on 11/27/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "RateOrderRequest.h"
#import "AppDelegate.h"

@implementation RateOrderRequest

- (id)initWithApiKey:(NSString*)apiKey withRate:(NSString*)rate comment:(NSString*)comment order_id:(NSString*)order_id
{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",
                                  rate,@"rate",
                                  comment,@"comment",
                                  order_id,@"order_id",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"orders/review"];
    }
    return self;
    
}

@end
/*
 /orders/review
 api_key
 rate
 comment

*/
