//
//  CreateCardRequest.m
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "CreateCardRequest.h"
#import "AppDelegate.h"

@implementation CreateCardRequest

- (id)initWithApiKey:(NSString*)apiKey withCustomerId:(NSString*)customerId expYear:(NSString*)exp_year expMonth:(NSString*)exp_month  cvc:(NSString*)cvc cardNumbers:(NSString*)card_number name:(NSString*)name  isDefault:(NSString*)is_default
{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",customerId,@"customer_id",exp_year,@"exp_year",exp_month,@"exp_month",cvc,@"cvc",card_number,@"card_number",name,@"name",is_default,@"is_default", nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@",@"http://api-red.expandis.fr/v2/payments/create"];
    }
    return self;
    
}
@end


