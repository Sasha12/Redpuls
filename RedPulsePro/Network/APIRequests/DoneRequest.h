//
//  DoneRequest.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/12/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "APIPost.h"

@interface DoneRequest : APIPost
- (id)initWithApiKey:(NSString*)apiKey andUserId:(NSString*)userId andOrderId:(NSArray*)orderId;

@end
