//
//  DeleteCardRequest.h
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "APIPost.h"

@interface DeleteCardRequest : APIPost
- (id)initWithApiKey:(NSString*)apiKey withCustomerId:(NSString*)customerId cardId:(NSString*)cardId;

@end
