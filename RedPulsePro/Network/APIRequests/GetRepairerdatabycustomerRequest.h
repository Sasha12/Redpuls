//
//  GetRepairerdatabycustomerRequest.h
//  RedPulse
//
//  Created by MacMini on 10/5/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "APIPost.h"

@interface GetRepairerdatabycustomerRequest : APIPost

- (id)initWithApiKey:(NSString*)apiKey andCustomerId:(NSString*) customerId;

@end
