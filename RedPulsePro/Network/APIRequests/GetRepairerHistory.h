//
//  GetRepairerHistory.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/6/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "APIPost.h"

@interface GetRepairerHistory : APIPost
- (id)initWithApiKey:(NSString*)apiKey andUserId:(NSString*)userId;

@end
