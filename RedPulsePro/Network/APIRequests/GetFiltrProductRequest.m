//
//  GetFiltrProductRequest.m
//  LGSideMenuControllerDemo
//
//  Created by Admin on 01/10/2016.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "GetFiltrProductRequest.h"
#import "AppDelegate.h"

@implementation GetFiltrProductRequest

- (id)initWithApiKey:(NSString*)apiKey andProductId:(NSString*) productId{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",@"1",@"product_id", nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"orders/filterproduct"];
    }
    return self;
    
}

@end

/*http://api-red.expandis.fr/orders/filterproduct
 /orders/filterproduct, product_id, api_key
 result:  "{\"success\":true,\"message\":[{\"id\":10,\"name\"😕"Ecrane case\",\"short_description\"😕"ecran case\",\"price\":96.99},{\"id\":12,\"name\"😕"Battery Replacmant\",\"short_description\"😕"Battery Replacmant short Description\",\"price\":100.23},{\"id\":13,\"name\"😕"TOMB\\u00c9S DANS L'EAU\",\"short_description\"😕"TOMB\\u00c9S DANS L'EAU short desc\",\"price\":99.6},{\"id\":14,\"name\"😕"AUTRES\",\"short_description\"😕"AUTRES short desc\",\"price\":50.23},{\"id\":15,\"name\"😕"Remplacement haut-parleur\",\"short_description\"😕"Remplacement haut-parleur short desc\",\"price\":67.02}]}"
*/
