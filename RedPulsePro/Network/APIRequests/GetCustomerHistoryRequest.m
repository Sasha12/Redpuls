//
//  GetCustomerHistoryRequest.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/12/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "GetCustomerHistoryRequest.h"
#import "AppDelegate.h"
@implementation GetCustomerHistoryRequest

- (id)initWithApiKey:(NSString*)apiKey andUserId:(NSString*)userId {
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",
                                  userId,@"user_id",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"orders/userhistory"];
    }
    return self;
    
}

@end


