//
//  GetServiceItemsRequest.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/3/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "GetServiceItemsRequest.h"
#import "AppDelegate.h"
@implementation GetServiceItemsRequest


- (id)initWithApiKey:(NSString*)apiKey andServiceName:(NSString *) serviceName andBrandId:(NSString*)brandId
{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",
                                                                                serviceName,@"service_name",
                                                                                    brandId,@"brand_id",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"services/view"];
    }
    return self;
    
}


@end
