//
//  GetRepairerLocationsRequest.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/12/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "APIPost.h"

@interface GetRepairerLocationsRequest : APIPost

- (id)initWithApiKey:(NSString*)apiKey andRepairerId:(NSString*) repId;


@end
