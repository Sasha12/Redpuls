//
//  GetCustomerAddressRequest.h
//  RedPulsePro
//
//  Created by MacMini on 12/9/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "APIPost.h"

@interface GetCustomerAddressRequest : APIPost
- (id)initWithApiKey:(NSString*)apiKey withCustomerId:(NSString*)customerId;
@end
