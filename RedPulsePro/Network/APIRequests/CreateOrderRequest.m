//
//  CreateOrderRequest.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/9/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "CreateOrderRequest.h"
#import "AppDelegate.h"

@implementation CreateOrderRequest


- (id)initWithApiKey:(NSString*)apiKey
     andUserLatitute:(NSString*)userLatitute
    andUserLongitute:(NSString*)userLongitute
       andCustomerId:(NSString*)customerId
           andItemId:(NSArray*)itemId
{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",
                                  userLatitute,@"lat",
                                  userLongitute,@"lng",
                                  customerId,@"user_id",
                                  itemId,@"item_id",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"orders/createorder"];
    }
    return self;
    
}



@end
