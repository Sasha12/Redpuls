//
//  GetBrandsRequest.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/4/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "APIPost.h"

@interface GetBrandsRequest : APIPost

- (id)initWithApiKey:(NSString*)apiKey;


@end
