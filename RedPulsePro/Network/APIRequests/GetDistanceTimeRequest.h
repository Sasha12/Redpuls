//
//  GetDistanceTimeRequest.h
//  RedPulsePro
//
//  Created by MacMini on 10/20/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "APIGet.h"
#import <CoreLocation/CoreLocation.h>

@interface GetDistanceTimeRequest : APIGet

- (id)initWithCustomerLocation:(CLLocationCoordinate2D)customerLocation andRepaierLocation:(CLLocationCoordinate2D)repaierLocation;
@end
