//
//  GetBrandFiltr.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/28/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "GetBrandFiltr.h"

@implementation GetBrandFiltr

- (id)initWithApiKey:(NSString*)apiKey andBrandId:(NSString*) brandId{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",@"1",@"brand_id", nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"orders/filterbrand"];
    }
    return self;
    
}


@end


/*
 /orders/filterbrand brand_id, api_key
 result:  "{\"success\":true,\"message\":{\"2\":\"galaxy s3\",\"5\":\"galaxy s4\",\"6\":\"galaxy s4 mini\",\"7\":\"galaxy s6\",\"8\":\"chdfgh\"}}"
 
*/
