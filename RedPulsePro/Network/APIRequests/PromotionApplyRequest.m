//
//  PromotionApplyRequest.m
//  RedPulsePro
//
//  Created by MacMini on 11/27/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "PromotionApplyRequest.h"
#import "AppDelegate.h"

@implementation PromotionApplyRequest

- (id)initWithApiKey:(NSString*)apiKey withCustomerId:(NSString*)customerId promotion_code:(NSString*)promotion_code
{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",
                                                                                 customerId,@"customer_id",
                                                                             promotion_code,@"promotion_code",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",@"http://api-red.expandis.fr/v2/",@"promotions/apply"];
    }
    return self;
    
}

@end

/*
 /promotions/apply
 api_key
 customer_id
 promotion_code
@"http://api-red.expandis.fr/v2/"
*/
