//
//  GetnearestsRequest.h
//  RedPulsePro
//
//  Created by MacMini on 10/21/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "APIPost.h"

@interface GetnearestsRequest : APIPost
- (id)initWithApiKey:(NSString*)apiKey;
@end
