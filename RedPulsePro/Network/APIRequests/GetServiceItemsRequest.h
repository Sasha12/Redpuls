//
//  GetServiceItemsRequest.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/3/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "APIPost.h"

@interface GetServiceItemsRequest : APIPost

- (id)initWithApiKey:(NSString*)apiKey andServiceName:(NSString *) serviceName andBrandId:(NSString*)brandId;

@end
