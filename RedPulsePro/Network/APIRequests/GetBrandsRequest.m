//
//  GetBrandsRequest.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/4/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "GetBrandsRequest.h"
#import "AppDelegate.h"

@implementation GetBrandsRequest

- (id)initWithApiKey:(NSString *)apiKey {
    self = [super init];
    if (self) {
        AppDelegate * appObj = [UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"brands"];
    }
    return self;
    
}

@end

