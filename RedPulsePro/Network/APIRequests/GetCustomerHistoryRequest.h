//
//  GetCustomerHistoryRequest.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/12/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "APIPost.h"

@interface GetCustomerHistoryRequest : APIPost
- (id)initWithApiKey:(NSString*)apiKey andUserId:(NSString*)userId;

@end
