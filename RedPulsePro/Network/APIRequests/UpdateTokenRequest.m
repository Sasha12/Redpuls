//
//  UpdateTokenRequest.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/9/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "UpdateTokenRequest.h"
#import "AppDelegate.h"

@implementation UpdateTokenRequest

- (id)initWithOldToken:(NSString*)oldToken andNewToken:(NSString*)newToken andApiKey:(NSString*)apiKey {
    self = [super init];
    if (self) {
        AppDelegate * appObj = [UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",
                                  oldToken,@"old_token",
                                  newToken,@"new_token",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"users/updatetoken"];
    }
    return self;
    
}


@end
