//
//  MapStyle.m
//  RedPulsePro
//
//  Created by MacMini on 12/16/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "MapStyle.h"

@implementation MapStyle

- (id)initWithApiKey:(NSString*)apiKey 
{
    self = [super init];
    if (self) {
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"AIzaSyCwgPOi6ywd9NBLRVaWobWHJnIJDuWGQH4",@"key",
                                  @"50.54718805343634,1.7545627593993807",@"center",
                                  @"14",@"zoom",
                                  @"png",@"format",
                                  @"roadmap",@"maptype",
                                  @"color:0xb2a94a",@"style",
                                  @"element:geometry%7Cvisibility:on",@"style",
                                  @"feature:administrative%7Celement:geometry.fill%7Cvisibility:off",@"style",
                                  @"feature:administrative.land_parcel%7Cvisibility:off",@"style",
                                  @"feature:administrative.neighborhood%7Cvisibility:off",@"style",
                                  @"feature:administrative.province%7Celement:geometry%7Cvisibility:off",@"style",
                                  @"feature:poi%7Celement:labels.text%7Cvisibility:off",@"style",
                                  @"feature:poi.business%7Cvisibility:off",@"style",
                                  @"feature:poi.park%7Celement:geometry%7Ccolor:0xdbd04f",@"style",
                                  @"feature:poi.park%7Celement:labels.text%7Cvisibility:off",@"style",
                                  @"feature:road%7Cvisibility:off",@"style",
                                  @"feature:road%7Celement:labels%7Cvisibility:off",@"style",
                                  @"feature:road.arterial%7Celement:geometry%7Csaturation:100%7Clightness:100%7Cweight:3",@"style",
                                  @"feature:transit.line%7Celement:geometry%7Ccolor:0xb6ad4c%7Csaturation:100%7Clightness:100%7Cvisibility:off%7Cweight:8",@"style",
                                  @"feature:water%7Celement:labels.text%7Cvisibility:off",@"style",
                                  @"480x360",@"style",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@",@"https://maps.googleapis.com/maps/api/staticmap"];
    }
    return self;
    
}

@end

/*
&size=
*/
