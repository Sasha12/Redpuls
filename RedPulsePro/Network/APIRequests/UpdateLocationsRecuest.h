//
//  UpdateLocationsRecuest.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/9/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "APIPost.h"

@interface UpdateLocationsRecuest : APIPost
- (id)initWithApiKey:(NSString*)apiKey andUserLatitute:(NSString*)userLatitute andUserLongitute:(NSString*)userLongitute andUserId:(NSString*)userID;

@end
