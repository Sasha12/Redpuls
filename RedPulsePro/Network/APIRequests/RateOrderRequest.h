//
//  RateOrderRequest.h
//  RedPulsePro
//
//  Created by MacMini on 11/27/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "APIPost.h"

@interface RateOrderRequest : APIPost
- (id)initWithApiKey:(NSString*)apiKey withRate:(NSString*)rate comment:(NSString*)comment order_id:(NSString*)order_id;
@end
