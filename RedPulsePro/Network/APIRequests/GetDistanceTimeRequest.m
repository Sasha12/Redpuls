//
//  GetDistanceTimeRequest.m
//  RedPulsePro
//
//  Created by MacMini on 10/20/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "GetDistanceTimeRequest.h"



@implementation GetDistanceTimeRequest

- (id)initWithCustomerLocation:(CLLocationCoordinate2D)customerLocation andRepaierLocation:(CLLocationCoordinate2D)repaierLocation
{
    self = [super init];
    if (self) {

        NSArray * arrayForKey = @[@"units",@"origins",@"destinations"];
        NSArray * arrayForValue = [NSArray arrayWithObjects:@"imperial",[NSString stringWithFormat:@"%f,%f",customerLocation.latitude,customerLocation.longitude],[NSString stringWithFormat:@"%f,%f",repaierLocation.latitude,repaierLocation.longitude], nil];
        NSDictionary * dicForrequest = [[NSDictionary alloc] initWithObjects:arrayForValue forKeys:arrayForKey];
        self.requestParameters = [[NSMutableDictionary alloc] initWithDictionary:dicForrequest];
        
        self.relativeURL =@"https://maps.googleapis.com/maps/api/distancematrix/json";
        
    }
    return self;
    
}


@end
//https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=40.6655101,-73.89188969999998&destinations=40.6905615,-73.9976592
