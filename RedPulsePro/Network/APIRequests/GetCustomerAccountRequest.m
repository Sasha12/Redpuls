//
//  GetCustomerAccountRequest.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/2/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "GetCustomerAccountRequest.h"
#import "AppDelegate.h"

@implementation GetCustomerAccountRequest


- (id)initWithUserName:(NSString*)userName andPassword:(NSString*) password
{
    self = [super init];
    if (self) {
        
        NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
        NSLog(@"UUID:: %@", uniqueIdentifier);
        
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        NSArray * arrayForKey = @[@"username",@"password",@"token",@"identifier"];
        NSArray * arrayForValue = [NSArray arrayWithObjects:userName,password,appObj.gDeviceToken,uniqueIdentifier,nil];
        NSDictionary * dicForrequest = [[NSDictionary alloc] initWithObjects:arrayForValue forKeys:arrayForKey];
        self.requestParameters = [[NSMutableDictionary alloc] initWithDictionary:dicForrequest];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"v1/users/login"];
        
    }
    return self;
    
}

@end


  
