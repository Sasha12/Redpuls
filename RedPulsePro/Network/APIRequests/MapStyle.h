//
//  MapStyle.h
//  RedPulsePro
//
//  Created by MacMini on 12/16/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "APIPost.h"

@interface MapStyle : APIPost
- (id)initWithApiKey:(NSString*)apiKey ;
@end
