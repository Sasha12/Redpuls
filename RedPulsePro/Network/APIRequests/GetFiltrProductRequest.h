//
//  GetFiltrProductRequest.h
//  LGSideMenuControllerDemo
//
//  Created by Admin on 01/10/2016.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "APIPost.h"

@interface GetFiltrProductRequest : APIPost
- (id)initWithApiKey:(NSString*)apiKey andProductId:(NSString*) productId;
@end
