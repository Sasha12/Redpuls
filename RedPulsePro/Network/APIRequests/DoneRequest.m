//
//  DoneRequest.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/12/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "DoneRequest.h"
#import "AppDelegate.h"


@implementation DoneRequest


- (id)initWithApiKey:(NSString*)apiKey andUserId:(NSString*)userId andOrderId:(NSArray*)orderId {
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",
                                  userId,@"user_id",
                                  orderId,@"order_id",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"orders/done"];
    }
    return self;
    
}

@end
