//
//  CreateOrderRequest.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/9/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "APIPost.h"

@interface CreateOrderRequest : APIPost

- (id)initWithApiKey:(NSString*)apiKey
     andUserLatitute:(NSString*)userLatitute
    andUserLongitute:(NSString*)userLongitute
       andCustomerId:(NSString*)customerId
           andItemId:(NSArray*)itemId;


@end
