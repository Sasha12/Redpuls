//
//  GetOrderViewRequest.m
//  RedPulsePro
//
//  Created by MacMini on 11/20/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "GetOrderViewRequest.h"
#import "AppDelegate.h"

@implementation GetOrderViewRequest

- (id)initWithApiKey:(NSString*)apiKey withOrderId:(NSString*)orderId
{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",orderId,@"order_id",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"orders/view"];
    }
    return self;
    
}

@end
//params :api_key, order_id
