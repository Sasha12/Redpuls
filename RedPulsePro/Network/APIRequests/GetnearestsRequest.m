//
//  GetnearestsRequest.m
//  RedPulsePro
//
//  Created by MacMini on 10/21/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "GetnearestsRequest.h"
#import "AppDelegate.h"
@implementation GetnearestsRequest

- (id)initWithApiKey:(NSString*)apiKey
{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"users/getnearests"];
    }
    return self;
    
}


@end
///orders/getnearests
//api_key, lat, lng,
