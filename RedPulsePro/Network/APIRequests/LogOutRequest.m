//
//  LogOutRequest.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/2/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "LogOutRequest.h"
#import "AppDelegate.h"
@implementation LogOutRequest


- (id)initWithApiKey:(NSString *)apiKey
{
    self = [super init];
    if (self) {
        AppDelegate * appObj = [UIApplication sharedApplication].delegate;
        NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
        NSLog(@"UUID:: %@", uniqueIdentifier);
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",
                                                                        appObj.gDeviceToken,@"token",
                                                                           uniqueIdentifier,@"identifier",nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"v1/users/logout"];
    }
    return self;
    
}

@end
