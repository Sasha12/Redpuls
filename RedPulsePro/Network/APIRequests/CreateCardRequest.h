//
//  CreateCardRequest.h
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "APIPost.h"

@interface CreateCardRequest : APIPost
- (id)initWithApiKey:(NSString*)apiKey withCustomerId:(NSString*)customerId expYear:(NSString*)exp_year expMonth:(NSString*)exp_month  cvc:(NSString*)cvc cardNumbers:(NSString*)card_number name:(NSString*)name  isDefault:(NSString*)is_default;

@end
