//
//  GetCustomerAccountRequest.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/2/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "APIPost.h"

@interface GetCustomerAccountRequest : APIPost


- (id)initWithUserName:(NSString*)userName andPassword:(NSString*) password;
@end


