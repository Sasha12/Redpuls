//
//  GetRepairerLocationsRequest.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/12/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "GetRepairerLocationsRequest.h"
#import "AppDelegate.h"

@implementation GetRepairerLocationsRequest

- (id)initWithApiKey:(NSString*)apiKey andRepairerId:(NSString*) repId{
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        self.requestParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:apiKey,@"api_key",repId,@"repairerId", nil];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"users/getusercoord"];
    }
    return self;
    
}


@end
