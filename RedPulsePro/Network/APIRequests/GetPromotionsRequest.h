//
//  GetPromotionsRequest.h
//  RedPulsePro
//
//  Created by MacMini on 11/27/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "APIPost.h"

@interface GetPromotionsRequest : APIPost
- (id)initWithApiKey:(NSString*)apiKey withCustomerId:(NSString*)customerId;
@end
