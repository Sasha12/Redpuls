//
//  AcceptRequest.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/9/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "AcceptRequest.h"
#import "AppDelegate.h"

@implementation AcceptRequest



- (id)initWithApiKey:(NSString*)apiKey andUserId:(NSString*)userId andOrderId:(NSArray*)orderId {
    self = [super init];
    if (self) {
        AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        NSArray* arrayWithKeys = @[@"api_key",@"user_id",@"order_id"];
        NSArray* arrayWithCalues = @[apiKey,userId,orderId];

        self.requestParameters = [[NSMutableDictionary alloc] initWithObjects:arrayWithCalues forKeys:arrayWithKeys];
        
        self.relativeURL =[NSString stringWithFormat:@"%@%@",appObj.BASE_URL,@"orders/accept"];
    }
    return self;
    
}

@end

