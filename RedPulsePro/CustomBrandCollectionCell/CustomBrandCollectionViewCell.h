//
//  CustomBrandCollectionViewCell.h
//  RedPulsePro
//
//  Created by MacMini on 10/17/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomBrandCollectionViewCell : UICollectionViewCell
-(void)addImage;
@property (strong, nonatomic) UIImageView *mBrandImageView;
@end
