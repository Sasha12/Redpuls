//
//  CustomBrandCollectionViewCell.m
//  RedPulsePro
//
//  Created by MacMini on 10/17/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "CustomBrandCollectionViewCell.h"

@implementation CustomBrandCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   }

-(instancetype)init {
    self = [super init];
    self.mBrandImageView = [[UIImageView alloc] initWithFrame:self.frame];
    
    [self addSubview:self.mBrandImageView];

    return self;
}
-(void)addImage{
    self.mBrandImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 40)];
    [self addSubview:self.mBrandImageView];
}
@end
