//
//  ViewController.m
//  NewApp
//
//  Created by MacMini on 8/11/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "MainViewController.h"
#import "GetCustomerAccountRequest.h"
#import "CustomerAccount.h"
#import "GetServicesRequest.h"
#import "Services.h"
#import "GetBrandsRequest.h"
#import "Brands.h"
#import "RepairerAccount.h"
#import "UserAccount.h"
#import "UpdateLocationsRecuest.h"
#import <CoreLocation/CoreLocation.h>
#import "CustomerHistoryList.h"
#import "GetCustomerHistoryRequest.h"
#import <Realm.h>
#import <ProgressHUD.h>
#import "RepairerViewController.h"
#import "RootViewController.h"
#import "GetRepairerHistory.h"
#import "ProductDetails.h"
#import "RepairerHistoryList.h"
#import "GetCustomerAddressRequest.h"
#import "CustomerAddressList.h"
#import "Reachability.h"
#import "LogOutRequest.h"

@interface LoginViewController () <UITextFieldDelegate> {
    AppDelegate * appObj;
    JsonToDictionary * jsonObject;
    
    NSArray * arrayWithKeys;
    NSArray * arrayWithValue;
    NSMutableDictionary * parametrs;
    RepairerAccount * repairerAccount;
    CLLocationManager *locationManager;
    NSTimer* myTimer;
    NSString* mLatitude;
    NSString* mLongitute;
    NSArray* arrayWithServicesList;
    NSInteger startValue;
    BOOL mIsConnected;
}

@property() CustomerAccount * customerAccount;

@property (weak, nonatomic) IBOutlet UITextField *mUserNameTF;
@property (weak, nonatomic) IBOutlet UITextField *mPasswordTF;
@property (weak, nonatomic) IBOutlet UIButton *mLoginBtn;
@end

@implementation LoginViewController
- (void)viewWillAppear:(BOOL)animated {
    appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    jsonObject = [[JsonToDictionary alloc] init];
    
    
    if (appObj.isBackFromMap) {
       // appObj.isBackFromMap = NO;
        //[self performSegueWithIdentifier:@"goProfile" sender:nil];
        
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerForKeyboardNotifications];
    self.mUserNameTF.delegate = self;
    self.mUserNameTF.layer.borderWidth = 1.f;
    self.mUserNameTF.layer.borderColor = [UIColor whiteColor].CGColor;
    self.mUserNameTF.layer.cornerRadius = 1.f;
    [self.mUserNameTF setValue:[UIColor whiteColor]forKeyPath:@"_placeholderLabel.textColor"];
    self.mPasswordTF.delegate = self;
    self.mPasswordTF.layer.borderWidth = 1.f;
    self.mPasswordTF.layer.borderColor = [UIColor whiteColor].CGColor;
    self.mPasswordTF.layer.cornerRadius = 1.f;
    [self.mPasswordTF setValue:[UIColor whiteColor]forKeyPath:@"_placeholderLabel.textColor"];
    self.mVisualView.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:1];
    
    self.mLoginBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    self.mVisualView.backgroundColor = [[UIColor darkGrayColor] colorWithAlphaComponent:1];
    
    appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if (appObj.isBackFromMap) {
        // appObj.isBackFromMap = NO;
       // [self performSegueWithIdentifier:@"goProfile" sender:nil];
        
    }
    //self.mVisualView.backgroundColor = [[UIColor darkGrayColor] colorWithAlphaComponent:0.8];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handelLogOutButtonPress)
                                                 name:@"LogOutButtonPressFromRepaier"
                                               object:nil];
    
}
-(void)viewDidAppear:(BOOL)animated {
    appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if (appObj.isBackFromMap) {
        // appObj.isBackFromMap = NO;
       // [self performSegueWithIdentifier:@"goProfile" sender:nil];
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)handelLogOutButtonPress {
    NSLog(@"handelLogOutButtonPress!!!!!!!!!!!============");
    [myTimer invalidate];
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _mScrollView.contentInset = contentInsets;
    _mScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.mPasswordTF.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, self.mPasswordTF.frame.origin.y-kbSize.height - 50);
        [_mScrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _mScrollView.contentInset = contentInsets;
    _mScrollView.scrollIndicatorInsets = contentInsets;
}
//- (void)textFieldDidBeginEditing:(UITextField *)textField {
//    CGRect toVisible = CGRectMake(0,textField.frame.origin.y-300 , textField.frame.size.height -230, textField.frame.size.width);
//    [_mScrollView scrollRectToVisible:toVisible animated:YES];
//}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.mUserNameTF) {
        [self.mPasswordTF becomeFirstResponder];
    }else {
    [textField resignFirstResponder];
    }
    
    return YES;
}
-(void)getCustomerAddressListWithArray:(NSMutableArray*)addressLDictionary {
    

        NSMutableArray* addressListArray = addressLDictionary;
        
        for (int i = 0; i < addressListArray.count; i++) {
            CustomerAddressList * mCustomerAddressList = [[CustomerAddressList alloc] initWithRestAPIResponse:[addressListArray objectAtIndex:i]];
            [mCustomerAddressList createOrUpdateCustomerAccountInStorage];
        }
        NSLog(@"addressListArray = %@",addressListArray);
        
   
}

-(void)getCustomerHistoryListWithArray:(NSMutableArray*)historyArray {
    
    RLMResults<CustomerAccount *> *mRepairerAccount = [CustomerAccount allObjects];
    _customerAccount = [mRepairerAccount firstObject];
    

        NSArray * messageForServices = historyArray;
        appObj.serviceList = messageForServices;
        arrayWithServicesList = messageForServices;
        appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
        NSLog(@"arrayWithServicesList = %@",arrayWithServicesList);
        appObj.historyCount = messageForServices.count;
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        RLMResults<CustomerHistoryList *> *mCustomerHistoryList = [CustomerHistoryList allObjects];
        NSArray * mCustomerHistoryListArray = (NSArray*)mCustomerHistoryList;
        
        if (mCustomerHistoryListArray.count > 0) {
            for (int i = 0; i < [mCustomerHistoryListArray count]; i++) {
                [realm beginWriteTransaction];
                [realm deleteObject:[mCustomerHistoryListArray objectAtIndex:i]];
                [realm commitWriteTransaction];
            }
        }
        
        
        for (int i = 0; i < [arrayWithServicesList count]; i++) {
            
            NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[arrayWithServicesList objectAtIndex:i] valueForKey:@"repairer_image"]]];
            CustomerHistoryList * customerHistoryList = [[CustomerHistoryList alloc] initWithRestAPIResponse:[arrayWithServicesList objectAtIndex:i] andKey:[NSString stringWithFormat:@"%d",i] imageData:imageData];
            
            [customerHistoryList createOrUpdateCustomerAccountInStorage];
            
        }
        
        [self goToCustomerPage];
  }


-(void)getServicesWithDictionary:(NSDictionary*)serviseDictionary {
    
    RLMResults<CustomerAccount *> *mRepairerAccount = [CustomerAccount allObjects];
    _customerAccount = [mRepairerAccount firstObject];
    
           NSDictionary * messageForServices = serviseDictionary;
        
        NSArray * servicesNamesArray = [[NSArray alloc] init];
        NSString* serviceNameStr;
        NSArray* servicesDetailsArray = [[NSArray alloc] init];
        NSMutableArray* servicesDetailsRLMOBjectsArray = [[NSMutableArray alloc] init];
        servicesNamesArray = [messageForServices allKeys];
        
        
        RLMRealm *realm1 = [RLMRealm defaultRealm];
        RLMResults<ServiceItems *> *mServices1 = [ServiceItems allObjects];
        NSArray * mServicesArr1 = (NSArray*)mServices1;
        
        if (mServicesArr1.count > 0) {
            //for (int i = 0; i < [mServicesArr1 count]; i++) {
            [realm1 beginWriteTransaction];
            [realm1 deleteObjects:mServices1];
            [realm1 commitWriteTransaction];
            //}
        }
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        RLMResults<Services *> *mServices = [Services allObjects];
        NSArray * mServicesArr = (NSArray*)mServices;
        
        if (mServicesArr.count > 0) {
            // for (int i = 0; i < [mServicesArr count]; i++) {
            [realm beginWriteTransaction];
            [realm deleteObjects:mServices];
            [realm commitWriteTransaction];
            //}
        }
        
        for (int i = 0; i < servicesNamesArray.count; i++) {
            if ([servicesNamesArray objectAtIndex:i]) {
                serviceNameStr = [servicesNamesArray objectAtIndex:i];
                
                servicesDetailsArray = [messageForServices valueForKey:serviceNameStr];
                servicesDetailsRLMOBjectsArray = [[NSMutableArray alloc] init];
                for (int j = 0 ; j < servicesDetailsArray.count; j++) {
                    NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[servicesDetailsArray objectAtIndex:j] valueForKey:@"service_logo"]]];
                    ServiceItems * mServiceItems = [[ServiceItems alloc] initWithRestAPIResponse:[servicesDetailsArray objectAtIndex:j] andImageData:imageData];

                    [mServiceItems createOrUpdateCustomerAccountInStorage];
                }
                
                RLMResults<ServiceItems *> *mServiceItems = [ServiceItems allObjects];
                startValue = mServiceItems.count - servicesDetailsArray.count;
                for (int k = (int)startValue;k < mServiceItems.count;k++) {
                    RLMObject *object = [mServiceItems objectAtIndex:k];
                    [servicesDetailsRLMOBjectsArray addObject:object];
                }
                
                Services * mServices = [[Services alloc] initWithRestAPIResponse:(RLMArray *)servicesDetailsRLMOBjectsArray name:serviceNameStr];
                [mServices createOrUpdateCustomerAccountInStorage];
                
            }
            
            
        }
        
        RLMResults<Services *> *mService = [Services allObjects];
        NSLog(@"mService = %@",mService);
   
}

#pragma mark -- Actions
- (IBAction)loginAction:(id)sender {
    
    
    
    [self.mUserNameTF resignFirstResponder];
    [self.mPasswordTF resignFirstResponder];
    if([self checkInternetConnection]){
        
        //self.mVisualView.hidden = NO;
        [ProgressHUD show:@"Please wait..."];
        if ([self.mUserNameTF.text isEqualToString:@""] || [self.mPasswordTF.text isEqualToString:@""]) {
            [ProgressHUD dismiss];
            //self.mVisualView.hidden = YES;
            [self showAlertWithMessage:@"Info" andText:@"Please select all fealds!"];
        }else{
            
            
            GetCustomerAccountRequest * getCustomerAccountRequest = [[GetCustomerAccountRequest alloc] initWithUserName:self.mUserNameTF.text  andPassword:self.mPasswordTF.text];
            [getCustomerAccountRequest postWithSuccess:^(NSDictionary *response) {
                
                
                NSError * error;
                NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                NSDictionary * servicesListDictionary = [dictionary  objectForKey:@"service_list"];
                NSMutableArray * addressListDictionary = [dictionary  objectForKey:@"address_list"];
                NSMutableArray * historyListDictionary = [dictionary  objectForKey:@"history_list"];
                
                NSString* messageDictionary = [dictionary objectForKey:@"message"];
                NSString* successString = [NSString stringWithFormat:@"%@",[dictionary objectForKey:@"success"]];
                NSDictionary* Dick = [NSDictionary dictionaryWithDictionary:dictionary];
                BOOL success = [dictionary objectForKey:@"success"];
                //            NSLog(@"USERNAME===%@", [messageDictionary objectForKey:@"username"]);
                NSArray * responseArray = [dictionary allValues];
                if ([successString isEqualToString:@"1"]) {
                    if (!messageDictionary) {
                        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLogin"];
                        [self showAlertWithMessage:@"Error" andText:[responseArray objectAtIndex:0]];
                    }else {
                        
                        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isLogin"];
                        if ([[messageDictionary valueForKey:@"role"] integerValue] == 20) {
                            //user
                            
                            CustomerAccount * customerAccount = [[CustomerAccount alloc] initWithRestAPIResponse:(NSDictionary*)messageDictionary isUserLogined:YES];
                            [customerAccount createOrUpdateCustomerAccountInStorage];
                            
                            
                            UserAccount * userAccount1 = [[UserAccount alloc] initWithUserName:customerAccount.UserName andToken:appObj.gDeviceToken andApiKey:customerAccount.ApiKey andRole:customerAccount.Role andIsUserLogined:NO];
                            [userAccount1 createOrUpdateCustomerAccountInStorage];
                            
                            
                            [self getServicesWithDictionary:servicesListDictionary];
                            [self getCustomerAddressListWithArray:addressListDictionary];
                            [self getCustomerHistoryListWithArray:historyListDictionary];
                            
                            
                            /*
                            GetBrandsRequest * getBrandsRequest = [[GetBrandsRequest alloc] initWithApiKey:[customerAccount getApiKey]];
                            [getBrandsRequest postWithSuccess:^(NSDictionary *response) {
                                NSError * error;
                                NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
                                NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                NSArray* messageDictionaryForBrand = [dictionary objectForKey:@"message"];
                                //NSArray * responseArray = [dictionary allValues];
                                for (int i = 0; i < [messageDictionaryForBrand count]; i++) {
                                    Brands * brands = [[Brands alloc] initWithBrandId:[[messageDictionaryForBrand objectAtIndex:i] valueForKey:@"id"] andBrandLogo:[[messageDictionaryForBrand objectAtIndex:i] valueForKey:@"brand_logo_b"] andBrandName:[[messageDictionaryForBrand objectAtIndex:i] valueForKey:@"name"] andRedLogo:[[messageDictionaryForBrand objectAtIndex:i] valueForKey:@"brand_logo_w"]];
                                    [brands createOrUpdateCustomerAccountInStorage];
                                }
                                //[self performSegueWithIdentifier:@"goToCustomerPage" sender:nil];
                                
                                NSLog(@"brands = ");
                                
                            } failure:^(NSError *error, NSInteger responseCode) {
                                NSLog(@"Brands Error = %@",error.description);
                            }];
                            
                            */
                                                       //
                           
                            
                            UpdateLocationsRecuest * updateLocationsRecuest = [[UpdateLocationsRecuest alloc] initWithApiKey:customerAccount.ApiKey andUserLatitute:mLatitude andUserLongitute:mLongitute andUserId:customerAccount.UserId];
                            
                            [updateLocationsRecuest postWithSuccess:^(NSDictionary *response) {
                                NSLog(@"Update User Location = %@",response);
                                //[self sendUpdateLocationRequest];
                            } failure:^(NSError *error, NSInteger responseCode) {
                                NSLog(@"Error Update User Location = %@",error.description);
                            }];
                            
                        }else {
                            
                            if ([[Dick valueForKey:@"message"] valueForKey:@"first_login"] == [NSNumber numberWithInteger:0]) {
                                NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
                                NSLog(@"UUID:: %@", uniqueIdentifier);
                                
                                [self showUUIDAlertWithMessage:@"Your device Unique id" andText:uniqueIdentifier witApikay:[[Dick valueForKey:@"message"] valueForKey:@"api_key"]];
                            }else {
                            repairerAccount = [[RepairerAccount alloc]initWithRestAPIResponse:messageDictionary isUserLogined:YES];
                            [repairerAccount  createOrUpdateCustomerAccountInStorage];
                            //[self performSegueWithIdentifier:@"goReaier" sender:nil];
                            
                            
                            //                        RLMRealm *realm = [RLMRealm defaultRealm];
                            //                        RLMResults<UserAccount *> *userAccounts = [UserAccount allObjects];
                            //                        UserAccount *userAccount = [userAccounts firstObject];
                            //
                            //                        [realm beginWriteTransaction];
                            //                        userAccount.usernName = repairerAccount.UserName;
                            //                        userAccount.role = repairerAccount.Role;
                            //                        userAccount.isUserLogined = NO;
                            //                        userAccount.deviceToken = appObj.gDeviceToken;
                            //                        userAccount.apiKey = repairerAccount.ApiKey;
                            //                        [realm commitWriteTransaction];
                            //
                            UserAccount * userAccount2 = [[UserAccount alloc] initWithUserName:repairerAccount.UserName andToken:appObj.gDeviceToken andApiKey:repairerAccount.ApiKey andRole:repairerAccount.Role andIsUserLogined:NO];
                            [userAccount2 createOrUpdateCustomerAccountInStorage];
                            
                            
                            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
                            locationManager = [[CLLocationManager alloc] init];
                            locationManager.delegate = self;
                            locationManager.distanceFilter = kCLDistanceFilterNone;
                            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
                            
                            //[self updateLocation];
                            [self updateLocation];
                            
                            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
                                [locationManager requestWhenInUseAuthorization];
                            
                            [locationManager requestWhenInUseAuthorization];
                            //[locationManager requestAlwaysAuthorization];
                            [locationManager startUpdatingLocation];
                            
                            [self getRepaierHistory];
                        }
                        }
                    }
                    
                    
                }else {
                    //                if ([[dictionary objectForKey:@"message"] isEqualToString:@"Wrong Username"] || [[dictionary objectForKey:@"message"] isEqualToString:@"Password required!"]) {
                    //                    NSLog(@"login error");
                    [ProgressHUD dismiss];
                    //self.mVisualView.hidden = YES;
                    [self showAlertWithMessage:@"Login Error" andText:@"Invalid username/password"];
                    // }
                    
                }
            } failure:^(NSError *error, NSInteger responseCode) {
                [ProgressHUD dismiss];
                //self.mVisualView.hidden = YES;
                [self showAlertWithMessage:@"Error" andText:[NSString stringWithFormat:@"Error N:%ld",(long)responseCode]];
                NSLog(@"Error = %@",error.description);
            }];
            
        }
        

        
    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }
    

    
}
-(void)getRepaierHistory {
    RLMResults<RepairerAccount *> *mRepairerAccount = [RepairerAccount allObjects];
    repairerAccount = [mRepairerAccount firstObject];
    
    //[self performSelectorInBackground:@selector(startProgressHud) withObject:nil];
    
    GetRepairerHistory* getRepairerHistory = [[GetRepairerHistory alloc] initWithApiKey:repairerAccount.ApiKey andUserId:repairerAccount.UserId];
    
    
    
    
    [getRepairerHistory postWithSuccess:^(NSDictionary *response) {
        
        //RepairerHistoryList
        
        
        
        NSError * error;
        NSData* serviceData = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *servicesList = [NSJSONSerialization JSONObjectWithData:serviceData options:kNilOptions error:&error];
        NSArray * messageForServices = [servicesList objectForKey:@"message"];
        appObj.serviceList = messageForServices;
        arrayWithServicesList = messageForServices;
        
        NSLog(@"arrayWithServicesList = %@",arrayWithServicesList);
        
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        RLMResults<RepairerHistoryList *> *mCustomerHistoryList = [RepairerHistoryList allObjects];
        NSArray * mRepairerHistoryListArray = (NSArray*)mCustomerHistoryList;
        
        if (mRepairerHistoryListArray.count > 0) {
            for (int i = 0; i < [mRepairerHistoryListArray count]; i++) {
                [realm beginWriteTransaction];
                [realm deleteObject:[mRepairerHistoryListArray objectAtIndex:i]];
                [realm commitWriteTransaction];
            }
        }
        CGFloat count = arrayWithServicesList.count;
        CGFloat tiv = 1;
        CGFloat progress = tiv / count;
        __block  CGFloat prog = 0;
        
        
        int y = 0;
        for (int i = 0; i < [arrayWithServicesList count]; i++) {
            
            
            for (int j = 0 ; j < [[[arrayWithServicesList objectAtIndex:i] valueForKey:@"product_details"] count]; j++) {
                ProductDetails * mProductDetails = [[ProductDetails alloc] initWithRestAPIResponse:[[[arrayWithServicesList objectAtIndex:i] valueForKey:@"product_details"] objectAtIndex:j] andKey:y];
                [mProductDetails createOrUpdateCustomerAccountInStorage];
                RLMResults<ProductDetails *> *mProductDetails11 = [ProductDetails allObjects];
                NSLog(@"mProductDetails11 = %ld",[mProductDetails11 count]);
                y++;
                NSLog(@"y = %d",y);
                
                startValue = [mProductDetails11 count] - j - 1;
            }
            RLMResults<ProductDetails *> *mProductDetails = [ProductDetails allObjects];
            
            NSMutableArray *array = [NSMutableArray array];
            for (int k = (int)startValue;k < mProductDetails.count;k++) {
                RLMObject *object = [mProductDetails objectAtIndex:k];
                [array addObject:object];
            }
            
            NSLog(@"array = %@",array);
            RepairerHistoryList * repairerHistoryList = [[RepairerHistoryList alloc] initWithRestAPIResponse:[arrayWithServicesList objectAtIndex:i] with:(RLMArray *)array];
            
            [repairerHistoryList createOrUpdateCustomerAccountInStorage];
        }
         [self goToRepaierPage];
    } failure:^(NSError *error, NSInteger responseCode) {
        
    }];
    
    
    
}

-(void)goToRepaierPage {
    // Override point for customization after application launch.
    [ProgressHUD dismiss];
    //self.mVisualView.hidden = YES;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    //UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationControllerRepaier"];
    RepairerViewController * repairerViewController = [storyboard instantiateViewControllerWithIdentifier:@"RepairerViewController"];
    MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainIdentifire"];
    mainViewController.rootViewController = repairerViewController;

    [mainViewController setupWithPresentationStyle:LGSideMenuPresentationStyleSlideAbove type:3];
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    
    window.rootViewController = mainViewController;
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
    
    
}

-(void)goToCustomerPage {
    // Override point for customization after application launch.
     [ProgressHUD dismiss];
    //self.mVisualView.hidden = YES;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
   // UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    RootViewController * rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"RootViewController"];
    MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainIdentifire"];
    mainViewController.rootViewController = rootViewController;

    [mainViewController setupWithPresentationStyle:LGSideMenuPresentationStyleSlideAbove type:3];
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    
    window.rootViewController = mainViewController;
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
   
    
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
     NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
     NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    mLongitute = [NSString stringWithFormat:@"%.10f", newLocation.coordinate.longitude];
    mLatitude = [NSString stringWithFormat:@"%.10f", newLocation.coordinate.latitude];
}



- (void)updateLocation {
    myTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self
                                             selector:@selector(sendUpdateLocationRequest) userInfo:nil repeats:YES];
}
- (void)sendUpdateLocationRequest {
    NSLog(@"ccccccccccccc");
    // CLLocation *currentLocation = [[CLLocation alloc] init];
    
    UpdateLocationsRecuest * updateLocationsRecuest = [[UpdateLocationsRecuest alloc] initWithApiKey:repairerAccount.ApiKey andUserLatitute:mLatitude andUserLongitute:mLongitute andUserId:repairerAccount.UserId];
    
    [updateLocationsRecuest postWithSuccess:^(NSDictionary *response) {
        NSLog(@"Update User Location = %@",response);
        //[self sendUpdateLocationRequest];
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"Error Update User Location = %@",error.description);
    }];
}

-(void)viewDidDisappear:(BOOL)animated {
    
}



-(BOOL)checkInternetConnection{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        mIsConnected = NO;
        NSLog(@"There IS NO internet connection");
    } else {
        NSLog(@"There IS internet connection");
        mIsConnected = YES;
    }
    return mIsConnected;
}
-(void)showAlertWithMessage:(NSString*)message andTitle:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}

-(void) showUUIDAlertWithMessage:(NSString *) message andText:(NSString *) text witApikay:(NSString*)apiKay{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:message
                                  message:text
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [super presentViewController:alert animated:YES completion:nil];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                            
                             LogOutRequest * logOutRequest = [[LogOutRequest alloc] initWithApiKey:apiKay];
                             
                             
                            [logOutRequest postWithSuccess:^(NSDictionary *response) {
                                NSLog(@"LOGOUT response = %@",response);
                                [ProgressHUD dismiss];
                            } failure:^(NSError *error, NSInteger responseCode) {
                                [ProgressHUD dismiss];
                                NSLog(@"LOGOUT ERROR = %@",error.description);
                            }];
                             
                             
                         }];
    [alert addAction:ok];
    
}

@end
