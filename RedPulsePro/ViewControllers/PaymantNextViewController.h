//
//  PaymantNextViewController.h
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymantNextViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIVisualEffectView *mVisualView;

- (IBAction)nextAction:(id)sender;
- (IBAction)backAction:(id)sender;

@end
