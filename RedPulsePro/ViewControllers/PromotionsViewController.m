//
//  PromotionsViewController.m
//  RedPulsePro
//
//  Created by MacMini on 11/27/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "PromotionsViewController.h"
#import "PromotionsCustomTableViewCell.h"
#import "GetPromotionsRequest.h"
#import "UserAccount.h"
#import "CustomerAccount.h"
#import "PromotionApplyRequest.h"
#import <ProgressHUD.h>
#import "Reachability.h"

@interface PromotionsViewController () <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate> {
    NSMutableArray* arrayWithPromotions;
    NSString* selectedPromoCod;
    BOOL mIsConnected;
}
@property() UserAccount* userAccount;
@property() CustomerAccount* mCustomerAccount;
@end

@implementation PromotionsViewController

-(void)viewWillAppear:(BOOL)animated {
    [ProgressHUD show:@"Please wait..."];
    //self.mVisualView.hidden = NO;
    RLMResults<UserAccount*>* mUserAccount = [UserAccount allObjects];
    _userAccount = [mUserAccount firstObject];
    
    RLMResults<CustomerAccount *> *mUserAccounts = [CustomerAccount allObjects];
    _mCustomerAccount = [mUserAccounts firstObject];
    if([self checkInternetConnection]){
        [ProgressHUD show:@"Please wait..."];
        //self.mVisualView.hidden = NO;
        [self getPromotions];
    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    [self.navigationItem setTitle:@"Enter Promo Code"];
    self.mApplyBtn.layer.masksToBounds = NO;
    self.mApplyBtn.layer.cornerRadius = 5;
    
    

    
}
-(void)getPromotions {
    GetPromotionsRequest * mGetPromotionsRequest = [[GetPromotionsRequest alloc] initWithApiKey:_userAccount.apiKey withCustomerId:_mCustomerAccount.UserId];
    
    [mGetPromotionsRequest postWithSuccess:^(NSDictionary *response) {
        NSLog(@"GetPromotionsRequest RESPONS = %@",response);
        NSError * error;
        NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        arrayWithPromotions = [dictionary objectForKey:@"message"];
        NSLog(@"GetPromotionsRequest arrayWithPromotions = %@",arrayWithPromotions);
        [self.mTableView reloadData];
        [ProgressHUD dismiss];
        //self.mVisualView.hidden = YES;
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"GetPromotionsRequest ERROR = %@",error.description);
        [ProgressHUD dismiss];
        //self.mVisualView.hidden = YES;
    }];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    arrayWithPromotions = [[NSMutableArray alloc] init];
    self.mPromCodeFieldTF.delegate = self;
    self.mTableView.delegate = self;
    self.mTableView.dataSource = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    
    [textField resignFirstResponder];
    return YES;
}
#pragma mark -- UITableViewDelegate,UITableViewDataSource



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayWithPromotions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PromotionsCustomTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(!cell){
        cell = [[PromotionsCustomTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.mPromCodeLbl.text = [[arrayWithPromotions objectAtIndex:indexPath.row] valueForKey:@"promotion_code"];
    NSString* usedString = [NSString stringWithFormat:@"%@",[[arrayWithPromotions objectAtIndex:indexPath.row] valueForKey:@"used"]];
    if ([usedString isEqualToString:@"1"]) {
        cell.mUsedLbl.text = @"USED" ;
        cell.mUsedLbl.textColor = [UIColor colorWithRed:202/255.f green:37/255.f blue:44/255.f alpha:1];

    }else if ([usedString isEqualToString:@"0"]){
        cell.mUsedLbl.text = @"UNUSED" ;
        cell.mUsedLbl.textColor = [UIColor blackColor];
        
    }
    [cell.mUsedLbl layoutIfNeeded];
    cell.mDescriptionLbl.text = [[arrayWithPromotions objectAtIndex:indexPath.row] valueForKey:@"description"];
    
    return cell;

    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.mPromCodeFieldTF.text = [[arrayWithPromotions objectAtIndex:indexPath.row] valueForKey:@"promotion_code"];
    selectedPromoCod = [[arrayWithPromotions objectAtIndex:indexPath.row] valueForKey:@"promotion_code"];
    [self.view layoutIfNeeded];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)applyAtion:(id)sender {
    
    if([self checkInternetConnection]){
        [ProgressHUD show:@"Please wait..."];
        //self.mVisualView.hidden = NO;
        PromotionApplyRequest * mPromotionApplyRequest = [[PromotionApplyRequest alloc] initWithApiKey:_userAccount.apiKey withCustomerId:_mCustomerAccount.UserId promotion_code:selectedPromoCod];
        
        [mPromotionApplyRequest postWithSuccess:^(NSDictionary *response) {
            NSLog(@"PromotionApplyRequest RESPONSE = %@",response);
            NSError * error;
            NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSString * message = [dictionary objectForKey:@"message"];
            [self getPromotions];
            [self showAlertWithMessage:@"Info" andText:message];
            
        } failure:^(NSError *error, NSInteger responseCode) {
            [ProgressHUD dismiss];
            //self.mVisualView.hidden = YES;
            NSLog(@"PromotionApplyRequest ERROR = %@",error.description);
        }];

    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }
    }

-(void) showAlertWithMessage:(NSString *) message andText:(NSString *) text {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:message
                                  message:text
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [super presentViewController:alert animated:YES completion:nil];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                             
                         }];
    [alert addAction:ok];
    
}


-(BOOL)checkInternetConnection{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        mIsConnected = NO;
        NSLog(@"There IS NO internet connection");
    } else {
        NSLog(@"There IS internet connection");
        mIsConnected = YES;
    }
    return mIsConnected;
}
-(void)showAlertWithMessage:(NSString*)message andTitle:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}

@end
