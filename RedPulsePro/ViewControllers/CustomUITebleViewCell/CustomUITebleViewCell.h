//
//  CustomUITebleViewCell.h
//  NewApp
//
//  Created by MacMini on 8/12/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomUITebleViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mDeviceImageView;
@property (weak, nonatomic) IBOutlet UILabel *mNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mOrderAddressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *mStatusImage;
@property (weak, nonatomic) IBOutlet UILabel *mStatusLabel;

@property (weak, nonatomic) IBOutlet UILabel *mStatus;

@property (weak, nonatomic) IBOutlet UIButton *mAcceptBtn;

@property (weak, nonatomic) IBOutlet UILabel *mCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *mCountStatusLbl;

- (IBAction)AcceptButtonAction:(id)sender;



@end
