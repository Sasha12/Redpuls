//
//  PaymentCardsListViewController.m
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "PaymentCardsListViewController.h"
#import "CustomCardsListTableViewCell.h"
#import "RootViewController.h"
#import "CustomAddCardTableViewCell.h"
#import "CardDetails.h"
#import "CustomerAccount.h"
#import "UserAccount.h"
#import "AppDelegate.h"
#import "SetDefoultCardRequest.h"
#import "DeleteCardRequest.h"
#import <Realm.h>
#import <ProgressHUD.h>
#import "Reachability.h"

@interface PaymentCardsListViewController () <UITableViewDelegate,UITableViewDataSource> {
    AppDelegate * appObj;
    BOOL mIsConnected;
}
@property() UserAccount* userAccount;
@property() CustomerAccount* customerAccount;
@property() NSMutableArray* cardListArray;
@end

@implementation PaymentCardsListViewController


-(void)viewWillAppear:(BOOL)animated {
    [ProgressHUD dismiss];
    //self.mVisualView.hidden = YES;
}
-(void)viewDidAppear:(BOOL)animated {
    appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    RLMResults<UserAccount *> *mUserAccount = [UserAccount allObjects];
    _userAccount = [mUserAccount firstObject];
    
    RLMResults<CustomerAccount *> *mCustomerAccount = [CustomerAccount allObjects];
    _customerAccount = [mCustomerAccount firstObject];
    RLMResults<CardDetails *> *mCardDetails = [CardDetails allObjects];
    _cardListArray = (NSMutableArray*)mCardDetails;
    
    
    RLMResults<CardDetails *> *mCardDetailsSorted = [mCardDetails sortedResultsUsingProperty:@"Default_card" ascending:NO];
    _cardListArray = (NSMutableArray*)mCardDetailsSorted;
    
    
    [self.mTableView reloadData];
}


- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handelEditButtonPress)
                                                 name:@"EditButtonPress"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handelDeleteButtonPress)
                                                 name:@"DeleteButtonPress"
                                               object:nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)handelEditButtonPress {
    NSLog(@"handelEditButtonPress  %ld",(long)appObj.selectedCardCellIndex);
    if ([[[_cardListArray objectAtIndex:appObj.selectedCardCellIndex] valueForKey:@"Default_card"] isEqualToString:@"0"]){
         [self showAlertForEditWithMessage:@"" andText:@"Set as default?"];
    }
   
}

-(void)handelDeleteButtonPress {
    NSLog(@"handelDeleteButtonPress  %ld",(long)appObj.selectedCardCellIndex);
    [self showAlertForDeleteWithMessage:@"" andText:@"Are you sure that you want to delete the information of this card?"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_cardListArray.count > 0) {
         return _cardListArray.count + 1;
    }else{
         return 1;
    }
   
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     CustomCardsListTableViewCell* CardListsCell = [tableView dequeueReusableCellWithIdentifier:@"CardListsCell"];
     CustomAddCardTableViewCell* addcardCell = [tableView dequeueReusableCellWithIdentifier:@"addcardCell"];

    if (indexPath.row != _cardListArray.count) {
       
        if(!CardListsCell){
            CardListsCell = [[CustomCardsListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CardListsCell"];
        }
        if (_cardListArray.count > 0) {
            
            CardListsCell.cardName.text = [[_cardListArray objectAtIndex:indexPath.row] valueForKey:@"Brand"];
            CardListsCell.cardNumberLbl.text = [NSString stringWithFormat:@"xxxx xxxx xxxx %@",[[_cardListArray objectAtIndex:indexPath.row] valueForKey:@"Last4"]];
            if ([[[_cardListArray objectAtIndex:indexPath.row] valueForKey:@"Default_card"] isEqualToString:@"0"]) {
                CardListsCell.editImage.hidden = NO;
                CardListsCell.edditbutton.hidden = NO;
                [CardListsCell.defoultChackboxCircle setImage:[UIImage imageNamed:@"klor_empty"]];
            }else {
                [CardListsCell.defoultChackboxCircle setImage:[UIImage imageNamed:@"klor_full"]];
                CardListsCell.editImage.hidden = YES;
                CardListsCell.edditbutton.hidden = YES;
            }
            
            CardListsCell.expireDateLbl.text = [NSString stringWithFormat:@"Expire %@",[[_cardListArray objectAtIndex:indexPath.row] valueForKey:@"Exp_date"]];
            [CardListsCell.cardImage setImage:[UIImage imageWithData:[[_cardListArray objectAtIndex:indexPath.row] valueForKey:@"cardLogoData"] scale:[[UIScreen mainScreen] scale]]];
            CardListsCell.edditbutton.tag  = (NSInteger)indexPath.row;
            CardListsCell.deleteButton.tag = (NSInteger)indexPath.row;

        }
        
        
        return CardListsCell;
    }else {
               if(!addcardCell){
            addcardCell = [[CustomAddCardTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"addcardCell"];
                   
        }
        addcardCell.mBackgroungredView.layer.cornerRadius = 3;
        return addcardCell;
    }
   
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _cardListArray.count) {
        [ProgressHUD show:@"Please wait..."];
        //self.mVisualView.hidden = NO;
        [self performSegueWithIdentifier:@"goToNext" sender:nil];
    }
}
-(void)setAsDefoult {
    
    SetDefoultCardRequest * mSetDefoultCardRequest = [[SetDefoultCardRequest alloc] initWithApiKey:_userAccount.apiKey withCustomerId:_customerAccount.UserId cardId:[[_cardListArray objectAtIndex:appObj.selectedCardCellIndex] valueForKey:@"Id"]];
    
    [mSetDefoultCardRequest postWithSuccess:^(NSDictionary *response) {
        NSLog(@"response = %@",response);
        NSError * error;
        NSData* editCardData = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *editCardList = [NSJSONSerialization JSONObjectWithData:editCardData options:kNilOptions error:&error];
        NSString * messageForAlert = [editCardList objectForKey:@"message"];
        NSMutableArray * arrayWithCardsList = [editCardList objectForKey:@"card_list"];
        [self showAlertWithMessage:@"" andText:messageForAlert];
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        RLMResults<CardDetails *> *mCardDetails = [CardDetails allObjects];
        NSArray * mCardDetailsArray = (NSArray*)mCardDetails;
        
        if (mCardDetailsArray.count > 0) {
            for (int i = 0; i < [mCardDetailsArray count]; i++) {
                [realm beginWriteTransaction];
                [realm deleteObject:[mCardDetailsArray objectAtIndex:i]];
                [realm commitWriteTransaction];
            }
        }
        
        
        for (int i = 0; i < arrayWithCardsList.count; i++) {
            
            NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[arrayWithCardsList objectAtIndex:i] valueForKey:@"card_logo"]]];
            
            CardDetails * mCardDetails = [[CardDetails alloc] initWithRestAPIResponse:[arrayWithCardsList objectAtIndex:i] imageData:imageData];
            [mCardDetails createOrUpdateCustomerAccountInStorage];
        }

        RLMResults<CardDetails *> *cardDetails = [CardDetails allObjects];
        _cardListArray = (NSMutableArray*)cardDetails;
        
        RLMResults<CardDetails *> *mCardDetailsSorted = [cardDetails sortedResultsUsingProperty:@"Default_card" ascending:NO];
        _cardListArray = (NSMutableArray*)mCardDetailsSorted;

        [self.mTableView reloadData];
        [ProgressHUD dismiss];
        //self.mVisualView.hidden = YES;
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"error = %@",error.description);
        [ProgressHUD dismiss];
        //self.mVisualView.hidden = YES;
    }];
    
}

-(void)DeleteCardRequest {
    DeleteCardRequest * mDeleteCardRequest = [[DeleteCardRequest alloc] initWithApiKey:_userAccount.apiKey withCustomerId:_customerAccount.UserId cardId:[[_cardListArray objectAtIndex:appObj.selectedCardCellIndex] valueForKey:@"Id"]];
    
    [mDeleteCardRequest postWithSuccess:^(NSDictionary *response) {
        NSLog(@"response = %@",response);
        NSError * error;
        NSData* deleteCardData = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *deleteCardList = [NSJSONSerialization JSONObjectWithData:deleteCardData options:kNilOptions error:&error];
        NSString * messageForAlert = [deleteCardList objectForKey:@"message"];
        NSMutableArray * arrayWithCardsList = [deleteCardList objectForKey:@"card_list"];
        [self showAlertWithMessage:@"" andText:messageForAlert];
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        RLMResults<CardDetails *> *mCardDetails = [CardDetails allObjects];
        NSArray * mCardDetailsArray = (NSArray*)mCardDetails;
        NSInteger count = mCardDetailsArray.count;
        NSLog(@"count = %ld",mCardDetailsArray.count);
         NSLog(@"count = %ld",count);
        if (mCardDetailsArray.count > 0) {
            for (int i = 0; i < count; i++) {
                [realm beginWriteTransaction];
                [realm deleteObject:[mCardDetailsArray objectAtIndex:0]];
                [realm commitWriteTransaction];
            }
        }
        
        
        for (int i = 0; i < arrayWithCardsList.count; i++) {
            
            NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[arrayWithCardsList objectAtIndex:i] valueForKey:@"card_logo"]]];
            
            CardDetails * mCardDetails = [[CardDetails alloc] initWithRestAPIResponse:[arrayWithCardsList objectAtIndex:i] imageData:imageData];
            [mCardDetails createOrUpdateCustomerAccountInStorage];
        }
        
        RLMResults<CardDetails *> *cardDetails = [CardDetails allObjects];
        _cardListArray = (NSMutableArray*)cardDetails;
        
        RLMResults<CardDetails *> *mCardDetailsSorted = [cardDetails sortedResultsUsingProperty:@"Default_card" ascending:NO];
        _cardListArray = (NSMutableArray*)mCardDetailsSorted;

        
        [self.mTableView reloadData];
        [ProgressHUD dismiss];
        //self.mVisualView.hidden = YES;
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"error = %@",error.description);
        [ProgressHUD dismiss];
        //self.mVisualView.hidden = YES;
    }];
    
}

-(void) showAlertWithMessage:(NSString *) message andText:(NSString *) text {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:message
                                  message:text
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [super presentViewController:alert animated:YES completion:nil];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                             
                         }];
    [alert addAction:ok];
    
}


-(void) showAlertForEditWithMessage:(NSString *) message andText:(NSString *) text {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:message
                                  message:text
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [super presentViewController:alert animated:YES completion:nil];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                             if([self checkInternetConnection]){
                                 [ProgressHUD show:@"Please wait..."];
                                 //self.mVisualView.hidden = NO;
                                 [self setAsDefoult];
                             }else{
                                 [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
                             }
                             

                            
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                         actionWithTitle:@"CANCEL"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                             
                         }];

    [alert addAction:ok];
    [alert addAction:cancel];
    
}

-(void) showAlertForDeleteWithMessage:(NSString *) message andText:(NSString *) text {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:message
                                  message:text
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [super presentViewController:alert animated:YES completion:nil];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                             if([self checkInternetConnection]){
                                 [ProgressHUD show:@"Please wait..."];
                                 //self.mVisualView.hidden = NO;
                                 [self DeleteCardRequest];
                             }else{
                                 [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
                             }
                             [ProgressHUD show:@"Please wait..."];
                             //self.mVisualView.hidden = NO;
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 //Do some thing here
                                 
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
   // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
   // RootViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"RootViewController"];
   // [self presentViewController:rootViewController animated:YES completion:nil];
    
    
}


-(BOOL)checkInternetConnection{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        mIsConnected = NO;
        NSLog(@"There IS NO internet connection");
    } else {
        NSLog(@"There IS internet connection");
        mIsConnected = YES;
    }
    return mIsConnected;
}
-(void)showAlertWithMessage:(NSString*)message andTitle:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}

@end
