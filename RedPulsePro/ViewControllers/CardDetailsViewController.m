//
//  CardDetailsViewController.m
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "CardDetailsViewController.h"
#import "CreateCardRequest.h"
#import "UserAccount.h"
#import "CustomerAccount.h"
#import "PaymentCardsListViewController.h"
#import "GetPaymantCardsRequest.h"
#import "CardDetails.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "MainViewController.h"
#import <ProgressHUD.h>
#import "Reachability.h"


@interface CardDetailsViewController () <UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource> {
    NSString * selectedMonth;
    NSString * selectedyear;
    BOOL isSelectCheckbox;
    NSString * defoultValue;
    AppDelegate * appObj;
    BOOL mIsConnected;
}

@property() UserAccount * userAccount;
@property() CustomerAccount * customerAccount;
@property (weak, nonatomic) IBOutlet UIDatePicker *monthPicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *yearPicker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *monthPickerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *yearPickerHeight;

@property (weak, nonatomic) IBOutlet UIImageView *chackboxImage;
@property (weak, nonatomic) IBOutlet UIPickerView *mMonthPicker;

@property (weak, nonatomic) IBOutlet UIPickerView *mYearPicker;
@end

@implementation CardDetailsViewController

-(void)viewWillAppear:(BOOL)animated {
    //self.mVisualView.hidden = YES;
    [ProgressHUD dismiss];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    defoultValue = @"0";
    selectedMonth = @"12";
    selectedyear = @"2016";
    self.monthPickerHeight.constant = 0;
    self.yearPickerHeight.constant = 0;
    self.yearChooseBtn.hidden = YES;
    self.monthChooseBtn.hidden = YES;
    isSelectCheckbox = NO;
    self.monthPicker.datePickerMode = UIDatePickerModeDate;
    self.yearPicker.datePickerMode = UIDatePickerModeDate;
    self.cardNameTextFieald.delegate = self;
    self.cardNumberTextField.delegate = self;
    self.securCodeTextField.delegate = self;
   
    self.monthChooseBtn.layer.cornerRadius = 3;
    self.yearChooseBtn.layer.cornerRadius = 3;
    self.addButton.layer.cornerRadius = 3;
    
    appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    RLMResults<UserAccount *> *mUserAccount = [UserAccount allObjects];
    _userAccount = [mUserAccount firstObject];
    
    RLMResults<CustomerAccount *> *mCustomerAccount = [CustomerAccount allObjects];
    _customerAccount = [mCustomerAccount firstObject];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)createCard {
    CreateCardRequest * mCreateCardRequest = [[CreateCardRequest alloc] initWithApiKey:_userAccount.apiKey withCustomerId:_customerAccount.UserId expYear:selectedyear expMonth:selectedMonth cvc:self.securCodeTextField.text cardNumbers:self.cardNumberTextField.text name:self.cardNameTextFieald.text isDefault:defoultValue];
    
    [mCreateCardRequest postWithSuccess:^(NSDictionary *response) {
        NSLog(@"respons = %@",response);
        //PaymentCardsListViewController
        
        NSError * error;
        NSData* deleteCardData = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *deleteCardList = [NSJSONSerialization JSONObjectWithData:deleteCardData options:kNilOptions error:&error];
        NSString * messageForAlert = [deleteCardList objectForKey:@"message"];
        NSString* isSuccess = [NSString stringWithFormat:@"%@",[deleteCardList objectForKey:@"success"]];
        if ([isSuccess isEqualToString:@"0"]) {
             NSLog(@"isSuccess = %@",isSuccess);
            [ProgressHUD dismiss];
            //self.mVisualView.hidden = YES;
            [self showAlertWithMessage:messageForAlert andTitle:@""];
        }else{
            NSLog(@"isSuccess = %@",isSuccess);
             [self getCardsListWittMessage:messageForAlert];
        }
       
        
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"error = %@",error.description);
        [ProgressHUD dismiss];
         //self.mVisualView.hidden = YES;
    }];
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == self.mMonthPicker) {
        return 12;
    }else{
       return 50;
    }
    
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    NSString * title = @"";
    
    if (pickerView == self.mMonthPicker) {
        title = [NSString stringWithFormat:@"%ld",(long)row + 1];
    }else {
        NSInteger year = 2016;
        title = [NSString stringWithFormat:@"%ld",year + row];
       
    }
    
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (pickerView == self.mMonthPicker) {
        selectedMonth = [NSString stringWithFormat:@"%ld",(long)row + 1];
        NSLog(@" selectedMonth = %@", selectedMonth);
    }else {
        NSInteger year = 2016;
        selectedyear = [NSString stringWithFormat:@"%ld",year + row];
        NSLog(@" selectedyear = %@", selectedyear);

    }
    
    
}
#pragma mark -- UITextFieldDelegate


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return YES;
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.yearChooseBtn.hidden = YES;
    self.monthChooseBtn.hidden = YES;
    [UIView animateWithDuration:3 animations:^{
        self.yearPickerHeight.constant = 0;
        [self.yearArrow setImage:[UIImage imageNamed:@"var"]];
        [self.yearArrow layoutIfNeeded];
        [self.yearPicker layoutIfNeeded];
    }];
    
    [UIView animateWithDuration:3 animations:^{
        self.monthPickerHeight.constant = 0;
        [self.monthArrow setImage:[UIImage imageNamed:@"var"]];
        [self.monthArrow layoutIfNeeded];
        [self.monthPicker layoutIfNeeded];
    }];
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason {
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.cardNumberTextField) {
        [self.cardNameTextFieald becomeFirstResponder];
    } else if(textField == self.cardNameTextFieald) {
        [self.securCodeTextField becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    
    
    return YES;
}

-(void)getCardsListWittMessage:(NSString*)message {
    GetPaymantCardsRequest * mGetPaymantCardsRequest = [[GetPaymantCardsRequest alloc] initWithApiKey:_userAccount.apiKey withCustomerId:_customerAccount.UserId];
    
    [mGetPaymantCardsRequest postWithSuccess:^(NSDictionary *response) {
        NSLog(@"response = %@",response);
        NSError * mError;
        NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *addressListDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&mError];
        NSMutableArray* cardListArray = (NSMutableArray*)[addressListDictionary objectForKey:@"message"];
        
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        RLMResults<CardDetails *> *mCardDetails = [CardDetails allObjects];
        NSArray * mCardDetailsArray = (NSArray*)mCardDetails;
        
        if (mCardDetailsArray.count > 0) {
            for (int i = 0; i < [mCardDetailsArray count]; i++) {
                [realm beginWriteTransaction];
                [realm deleteObject:[mCardDetailsArray objectAtIndex:i]];
                [realm commitWriteTransaction];
            }
        }
        
        
        for (int i = 0; i < cardListArray.count; i++) {
            
            NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[cardListArray objectAtIndex:i] valueForKey:@"card_logo"]]];
            
            CardDetails * mCardDetails = [[CardDetails alloc] initWithRestAPIResponse:[cardListArray objectAtIndex:i] imageData:imageData];
            [mCardDetails createOrUpdateCustomerAccountInStorage];
        }
        
        [ProgressHUD dismiss];
        //self.mVisualView.hidden = YES;
        [self showAlertWithMessage:@"" andText:message];
        
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"error = %@",error.description);
    }];
    

}
-(void) showAlertWithMessage:(NSString *) message andText:(NSString *) text {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:message
                                  message:text
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [super presentViewController:alert animated:YES completion:nil];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             appObj.isaddCardPage = YES;
                             [ProgressHUD show:@"Please wait..."];
                             //self.mVisualView.hidden = NO;
                             [self dismissViewControllerAnimated:YES completion:nil];
                             
                             UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            // PaymentCardsListViewController *paymentCardsListViewController = [storyboard instantiateViewControllerWithIdentifier:@"PaymentCardsListViewController"];
                            // [self presentViewController:paymentCardsListViewController animated:YES completion:nil];
                             /*
                             RootViewController * rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"RootViewController"];
                             MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainIdentifire"];
                             mainViewController.rootViewController = rootViewController;
                             [mainViewController setupWithPresentationStyle:LGSideMenuPresentationStyleSlideAbove type:3];
                             
                             UIWindow *window = [UIApplication sharedApplication].delegate.window;
                             
                             window.rootViewController = mainViewController;
*/
                             
                         }];
    [alert addAction:ok];
    
}


-(void) showAreFildsRequaretMessage {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:@"All fildes are requaired"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [super presentViewController:alert animated:YES completion:nil];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                            
                         }];
    [alert addAction:ok];
    
}


- (IBAction)backAction:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)monthAction:(id)sender {
    
    [self.cardNameTextFieald resignFirstResponder];
    [self.cardNumberTextField resignFirstResponder];
    [self.securCodeTextField resignFirstResponder];
    
    if (self.monthPickerHeight.constant > 0) {
        self.monthChooseBtn.hidden = YES;
        [self.monthArrow setImage:[UIImage imageNamed:@"var"]];
        [self.monthArrow layoutIfNeeded];
        [UIView animateWithDuration:3 animations:^{
            self.monthPickerHeight.constant = 0;
            [self.monthPicker layoutIfNeeded];
        }];
    }else{
        self.monthChooseBtn.hidden = NO;
        [self.monthArrow setImage:[UIImage imageNamed:@"ver"]];
        [self.monthArrow layoutIfNeeded];
        [UIView animateWithDuration:3 animations:^{
            self.monthPickerHeight.constant = 187;
            [self.monthPicker layoutIfNeeded];
        }];
        
    }
    
}

- (IBAction)yearAction:(id)sender {
    
    [self.cardNameTextFieald resignFirstResponder];
    [self.cardNumberTextField resignFirstResponder];
    [self.securCodeTextField resignFirstResponder];
    
    if (self.yearPickerHeight.constant > 0) {
        self.yearChooseBtn.hidden = YES;
        [self.yearArrow setImage:[UIImage imageNamed:@"var"]];
        [self.yearArrow layoutIfNeeded];
        [UIView animateWithDuration:3 animations:^{
            self.yearPickerHeight.constant = 0;
            
            [self.yearPicker layoutIfNeeded];
        }];
    }else{
        self.yearChooseBtn.hidden = NO;
        [self.yearArrow setImage:[UIImage imageNamed:@"ver"]];
        [self.yearArrow layoutIfNeeded];
        [UIView animateWithDuration:3 animations:^{
            self.yearPickerHeight.constant = 187;
            
            [self.yearPicker layoutIfNeeded];
        }];
        
    }
    
}
- (IBAction)chooseMonthAction:(id)sender {
    self.expirationMonth.text = selectedMonth;
    self.monthChooseBtn.hidden = YES;
    [UIView animateWithDuration:3 animations:^{
        self.monthPickerHeight.constant = 0;
        [self.monthArrow setImage:[UIImage imageNamed:@"var"]];
        [self.monthArrow layoutIfNeeded];
        [self.monthPicker layoutIfNeeded];
    }];

}
- (IBAction)chooseYearAction:(id)sender {
    self.yearChooseBtn.hidden = YES;
     self.expirationYearLbl.text = selectedyear;
    [UIView animateWithDuration:3 animations:^{
        self.yearPickerHeight.constant = 0;
        [self.yearArrow setImage:[UIImage imageNamed:@"var"]];
        [self.yearArrow layoutIfNeeded];
        [self.yearPicker layoutIfNeeded];
    }];
}

- (IBAction)chackboxAction:(id)sender {
    
    if (isSelectCheckbox) {
        defoultValue = @"0";
        isSelectCheckbox = NO;
        [self.chackboxImage setImage:[UIImage imageNamed:@"ptichka_empaty"]];
    }else{
        defoultValue = @"1";
        isSelectCheckbox = YES;
        [self.chackboxImage setImage:[UIImage imageNamed:@"PTICHKA_FULL"]];
    }
}

- (IBAction)addAction:(id)sender {
    [self.cardNameTextFieald resignFirstResponder];
    [self.cardNumberTextField resignFirstResponder];
    [self.securCodeTextField resignFirstResponder];
    
    if ([self.cardNameTextFieald.text length] > 0 && [self.cardNumberTextField.text length] > 0 && [self.securCodeTextField.text length] > 0) {
       [ProgressHUD show:@"Please wait..."];
        //self.mVisualView.hidden = NO;
        if([self checkInternetConnection]){
            [self createCard];
        }else{
            [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
        }
        

        
    }else {
        [self showAreFildsRequaretMessage];
    }
}


-(BOOL)checkInternetConnection{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        mIsConnected = NO;
        NSLog(@"There IS NO internet connection");
    } else {
        NSLog(@"There IS internet connection");
        mIsConnected = YES;
    }
    return mIsConnected;
}
-(void)showAlertWithMessage:(NSString*)message andTitle:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}

@end
