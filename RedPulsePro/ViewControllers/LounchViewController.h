//
//  LounchViewController.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/4/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LounchViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIProgressView *mProgressView;
@property (weak, nonatomic) IBOutlet UIImageView *mLogo;
@end
