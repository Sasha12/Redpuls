//
//  CutomCollecionViewCellForOrderDetails.h
//  RedPulsePro
//
//  Created by MacMini on 11/20/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CutomCollecionViewCellForOrderDetails : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderNameLbl;

@property (weak, nonatomic) IBOutlet UILabel *orderPriceLbl;
@property (weak, nonatomic) IBOutlet UIView *mLineView;
@end
