//
//  OrderDetailsViewController.h
//  RedPulsePro
//
//  Created by MacMini on 11/20/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface OrderDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet MKMapView *mMapView;
@property (weak, nonatomic) IBOutlet UIImageView *mRepaierImageView;
@property (weak, nonatomic) IBOutlet UILabel *mRepaierNameLbl;
@property (weak, nonatomic) IBOutlet UIButton *star1;
@property (weak, nonatomic) IBOutlet UIButton *star2;
@property (weak, nonatomic) IBOutlet UIButton *star3;
@property (weak, nonatomic) IBOutlet UIButton *star4;
@property (weak, nonatomic) IBOutlet UIButton *star5;

@property (weak, nonatomic) IBOutlet UILabel *repairTypeLbl;
@property (weak, nonatomic) IBOutlet UICollectionView *mCollectionView;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *mVisualView;

@property (weak, nonatomic) IBOutlet UILabel *mOrderTitleLbl;

@property (weak, nonatomic) IBOutlet UIView *mViewForMap;


- (IBAction)starsAction:(id)sender;


- (IBAction)backAction:(id)sender;




@end
