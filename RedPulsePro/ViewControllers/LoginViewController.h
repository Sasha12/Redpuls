//
//  ViewController.h
//  NewApp
//
//  Created by MacMini on 8/11/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JsonToDictionary.h"
#import "BaseViewController.h"

@interface LoginViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIScrollView *mScrollView;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *mVisualView;

//- (void)logOut;

@end

