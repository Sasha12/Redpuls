//
//  PromotionsCustomTableViewCell.h
//  RedPulsePro
//
//  Created by MacMini on 11/27/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromotionsCustomTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mPromCodeLbl;

@property (weak, nonatomic) IBOutlet UILabel *mUsedLbl;

@property (weak, nonatomic) IBOutlet UILabel *mDescriptionLbl;




@end
