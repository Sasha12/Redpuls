//
//  PaymantNextViewController.m
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "PaymantNextViewController.h"
#import "SetDefoultCardRequest.h"
#import "GetPaymantCardsRequest.h"
#import "AppDelegate.h"
#import <ProgressHUD.h>

@interface PaymantNextViewController () {
    AppDelegate * appObj;
}

@end

@implementation PaymantNextViewController

-(void)viewWillAppear:(BOOL)animated {
    [ProgressHUD dismiss];
    //self.mVisualView.hidden = YES;
    if (appObj.isaddCardPage) {
        [ProgressHUD show:@"Please wait..."];
        //self.mVisualView.hidden = NO;
     //self.view.hidden = YES;
    }
    appObj = (AppDelegate *)[UIApplication sharedApplication].delegate;
   
}

-(void)viewDidAppear:(BOOL)animated {
    
    if (appObj.isaddCardPage) {
        [ProgressHUD show:@"Please wait..."];
        //self.mVisualView.hidden = NO;
        //self.view.hidden = YES;
        appObj.isaddCardPage = NO;
        [self dismissViewControllerAnimated:YES completion:nil];
    }

}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)nextAction:(id)sender {
    [ProgressHUD show:@"Please wait..."];
   //self.mVisualView.hidden = NO;
    [self performSegueWithIdentifier:@"goToCardDetails" sender:nil];
}

- (IBAction)backAction:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}
@end
