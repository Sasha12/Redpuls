//
//  HistoryViewController.h
//  RedPulsePro
//
//  Created by MacMini on 10/23/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryViewController : UIViewController {
    NSMutableIndexSet *expandedSections;
}
@property (weak, nonatomic) IBOutlet UITableView *mTableView;
@property (weak, nonatomic) IBOutlet UILabel *mMessageLbl;

@property (weak, nonatomic) IBOutlet UIVisualEffectView *mVisualEffectView;
@end
