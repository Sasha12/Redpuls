//
//  OrderDetailsViewController.m
//  RedPulsePro
//
//  Created by MacMini on 11/20/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "OrderDetailsViewController.h"
#import "AppDelegate.h"
#import "GetOrderViewRequest.h"
#import "CustomerAccount.h"
#import "CutomCollecionViewCellForOrderDetails.h"
#import "CustomerHistoryList.h"
#import "MapAnotation.h"
#import <DXAnnotationView.h>
#import <DXAnnotationSettings.h>
#import "RateOrderRequest.h"
#import "UserAccount.h"
#import <Realm.h>
#import "Reachability.h"
#import <ProgressHUD.h>

@import GoogleMaps;

@interface OrderDetailsViewController () <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,MKMapViewDelegate> {
    
    MKPointAnnotation *selectedPlaceAnnotation;
    NSMutableDictionary* DictionaryForOrderDetails;
    double repLat;
    double repLong;
    NSString* mTitle;
    NSString* mPhoneNumber;
    NSString* orderId;
    NSMutableArray * rateAray;
    NSMutableArray * rateKeyArray;
    AppDelegate * appObj;
    BOOL mIsConnected;
    
    NSMutableDictionary * mRateDictionary;
}

@property() CustomerAccount * customerAccount;
@property() UserAccount * userAccount;
@property() NSMutableArray * mCustomerHistoryListArray;
@property() GMSMapView * mapView;


@end

@implementation OrderDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mMapView.delegate = self;
    DictionaryForOrderDetails = [[NSMutableDictionary alloc] init];
    self.mCollectionView.delegate = self;
    self.mCollectionView.dataSource = self;
    rateAray = [[NSMutableArray alloc] init];
    rateKeyArray = [[NSMutableArray alloc] init];
    mRateDictionary = [[NSMutableDictionary alloc] init]
    ;
   
   
}

-(void)viewWillAppear:(BOOL)animated {
   
    
    if (![[NSUserDefaults standardUserDefaults] valueForKey:@"isRate"]){
        [[NSUserDefaults standardUserDefaults] setValue:mRateDictionary forKey:@"isRate"];
    }else {
        mRateDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:@"isRate"];
    }
    RLMResults<CustomerAccount*>* mCustomerAccount = [CustomerAccount allObjects];
    _customerAccount = [mCustomerAccount firstObject];
    RLMResults<CustomerHistoryList*>* mCustomerHistoryList = [CustomerHistoryList allObjects];
    _mCustomerHistoryListArray = (NSMutableArray*)mCustomerHistoryList;
    
    RLMResults<UserAccount*>* mUserAccount = [UserAccount allObjects];
    _userAccount = [mUserAccount firstObject];
    appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    orderId = [[_mCustomerHistoryListArray objectAtIndex:appObj.selectedOrderIndexForDetails] valueForKey:@"OrderId"];
    if (mRateDictionary.count > 0) {
        if ([[mRateDictionary allKeys] containsObject:orderId]) {
            
            for (int i = 0 ; i < [[mRateDictionary allValues] count]; i++) {
                
                if ([[[mRateDictionary allKeys] objectAtIndex:i] isEqualToString:orderId]) {
                    
                    [self addStarWithRate:[[[mRateDictionary allValues]  objectAtIndex:i] integerValue]];
                    /*
                    self.star1.enabled = NO;
                    self.star2.enabled = NO;
                    self.star3.enabled = NO;
                    self.star4.enabled = NO;
                    self.star5.enabled = NO;
                    self.star1.userInteractionEnabled = NO;
                    self.star2.userInteractionEnabled = NO;
                    self.star3.userInteractionEnabled = NO;
                    self.star4.userInteractionEnabled = NO;
                    self.star5.userInteractionEnabled = NO;
                    */
                }
                
            }
     
        }
    }
    
    [self getOrderViewDetails];
}

-(void)setMapWithCoordinate:(CLLocationCoordinate2D)coordinate {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude
                                                            longitude:coordinate.longitude
                                                                 zoom:16];
    _mapView = [GMSMapView mapWithFrame:self.mViewForMap.frame camera:camera];
    
    
    //GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:[[NSBundle mainBundle] URLForResource:@"mapstyle" withExtension:@"json"] error:nil];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
     marker.position = camera.target;
     marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = _mapView;
    [_mapView animateToLocation:coordinate];

    _mapView.mapStyle = style;

    [self.mViewForMap addSubview:_mapView];
}
-(void)getOrderViewDetails {
    
    
    DictionaryForOrderDetails = appObj.DictionaryForOrderDetails;
    repLat = appObj.repLat;
    repLong = appObj.repLong;
    self.mRepaierNameLbl.text = appObj.repName;
    
    [self.mRepaierImageView setImage:appObj.repImage];
    self.mRepaierImageView.layer.cornerRadius = 35;
    self.mRepaierImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.mRepaierImageView.layer.borderWidth = 2;
    self.mRepaierImageView.layer.masksToBounds = YES;
    [self.mCollectionView reloadData];
    self.mOrderTitleLbl.text = [NSString stringWithFormat:@"ORDER %@",appObj.repOrderId];
    [self.view layoutIfNeeded];
    //mTitle = [NSString stringWithFormat:@"ORDER %@",appObj.repOrderId];
    //[self recenterMapToPlacemark:CLLocationCoordinate2DMake(repLat, repLong) withName:[NSString stringWithFormat:@"ORDER %@",appObj.repOrderId]];

     [self setMapWithCoordinate:CLLocationCoordinate2DMake(repLat, repLong)];
    
//    GetOrderViewRequest * mGetOrderViewRequest = [[GetOrderViewRequest alloc] initWithApiKey:_customerAccount.ApiKey withOrderId:orderId];
//    
//    [mGetOrderViewRequest postWithSuccess:^(NSDictionary *response) {
//        NSLog(@"mGetOrderViewRequest Respons = %@",response);
//
//        NSError * error;
//        NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
//        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
//        NSDictionary * orderDetailsDictionary = [dictionary objectForKey:@"message"];
//        DictionaryForOrderDetails = [orderDetailsDictionary valueForKey:@"order_details"];
//        repLat = [[[orderDetailsDictionary valueForKey:@"address_coordinates"] valueForKey:@"lat"] doubleValue];
//        repLong = [[[orderDetailsDictionary valueForKey:@"address_coordinates"] valueForKey:@"lng"] doubleValue];
//        self.mRepaierNameLbl.text = [NSString stringWithFormat:@"%@ %@",[[orderDetailsDictionary valueForKey:@"repairer_data"] valueForKey:@"name"],[[orderDetailsDictionary  valueForKey:@"repairer_data"] valueForKey:@"surname"]];
//        mPhoneNumber = [[orderDetailsDictionary  valueForKey:@"repairer_data"] valueForKey:@"phone"];
//        
//        [self.mRepaierImageView setImage:[UIImage imageWithData:
//                                   [NSData dataWithContentsOfURL:
//                                    [NSURL URLWithString:[[orderDetailsDictionary valueForKey:@"repairer_data"] valueForKey:@"image"]]]]];
//        self.mRepaierImageView.layer.cornerRadius = 35;
//        self.mRepaierImageView.layer.borderColor = [UIColor whiteColor].CGColor;
//        self.mRepaierImageView.layer.borderWidth = 2;
//        self.mRepaierImageView.layer.masksToBounds = YES;
//        [self.mCollectionView reloadData];
//        self.mOrderTitleLbl.text = [NSString stringWithFormat:@"ORDER %@",[orderDetailsDictionary valueForKey:@"order_id"]];
//        [self.view layoutIfNeeded];
//        mTitle = [NSString stringWithFormat:@"ORDER %@",[orderDetailsDictionary valueForKey:@"order_id"]];
//        [self recenterMapToPlacemark:CLLocationCoordinate2DMake(repLat, repLong) withName:[NSString stringWithFormat:@"ORDER %@",[orderDetailsDictionary valueForKey:@"order_id"]]];
//        
//        //,[[orderDetailsDictionary valueForKey:@"repairer_data"] valueForKey:@"phone"]
//        
//    } failure:^(NSError *error, NSInteger responseCode) {
//        NSLog(@"mGetOrderViewRequest ERROR = %@",error.description);
//    }];
//    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark --CollectionVew Delegate Metods 

-(CGSize)collectionView:(UICollectionView *)collectionView
layout:(UICollectionViewLayout *)collectionViewLayout
sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    // Adjust cell size for orientation
//    if (UIDeviceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
//        return CGSizeMake(170.f, 170.f);
//    }
    return CGSizeMake(self.view.frame.size.width, 60.f);
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return DictionaryForOrderDetails.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CutomCollecionViewCellForOrderDetails *cell = (CutomCollecionViewCellForOrderDetails *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    //[cell1.mCircleImageView setImage:[UIImage imageNamed:@"klor"]];
    
    NSNumber * num = [[DictionaryForOrderDetails valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]] valueForKey:@"price"];
    cell.orderNameLbl.text = [[DictionaryForOrderDetails valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]] valueForKey:@"name"];
    cell.orderPriceLbl.text = [NSString stringWithFormat:@"€%.2f",num.floatValue];
    cell.orderPriceLbl.textColor = [UIColor blackColor];
    cell.orderPriceLbl.textColor = [UIColor blackColor];
    NSLog(@"indexPath.row = %ld",(long)indexPath.row);
    NSLog(@"DictionaryForOrderDetails.count = %ld",(long)DictionaryForOrderDetails.count);
    if (indexPath.row == DictionaryForOrderDetails.count-1) {
        cell.orderNameLbl.textColor = [UIColor colorWithRed:202/255.f green:37/255.f blue:44/255.f alpha:1];
        [cell.orderNameLbl setFont:[UIFont systemFontOfSize:30]];
        cell.orderNameLbl.adjustsFontSizeToFitWidth = YES;
        cell.orderNameLbl.text = @"CHARGED";
        cell.orderPriceLbl.text = [NSString stringWithFormat:@"€%.2f",[(NSNumber *)[DictionaryForOrderDetails valueForKey:@"total"] floatValue]];
        cell.orderPriceLbl.textColor = [UIColor colorWithRed:202/255.f green:37/255.f blue:44/255.f alpha:1];
        [cell.orderPriceLbl setFont:[UIFont systemFontOfSize:30]];
        cell.orderPriceLbl.adjustsFontSizeToFitWidth = YES;
        cell.mLineView.hidden = YES;
    }
    return cell;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (void)recenterMapToPlacemark:(CLLocationCoordinate2D)locations withName:(NSString*)name{
    
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    
    span.latitudeDelta = 0.02;
    span.longitudeDelta = 0.02;
    
    region.span = span;
    region.center = locations;
    MapAnotation *  CustomerAnnotation = [[MapAnotation alloc] init];
    CustomerAnnotation.coordinate = locations;
    
    
   // CustomerAnnotation.title = name;
    
    
    [self.mMapView addAnnotation:CustomerAnnotation];
    [self.mMapView setRegion:region];
    //    mLatitude = [NSString stringWithFormat:@"%f",locations.latitude];
    //    mLongitute = [NSString stringWithFormat:@"%f",locations.longitude];
    
}

-(MKAnnotationView*)mapView:(MKMapView*)mapView viewForAnnotation:(MapAnotation <MKAnnotation>*)annotation
{
    UIView * pinView;
    DXAnnotationView * annotationViewRepaier;
    // NSLog(@"RepaierAnnotation Coordinate Lat = %f   Long = %f",RepaierAnnotation.coordinate.latitude,RepaierAnnotation.coordinate.longitude);
    UIImageView* image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"karmir.png"]];
    //image.contentMode = UIViewContentMode.ScaleAspectFit;
    image.contentMode = UIViewContentModeScaleAspectFit;
    //pinView = [[UIImageView alloc] initWithImage:image];
    [image setFrame:CGRectMake(0, 40, 20, 20)];
    UIView * cloudView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 60)];

    UIImageView * bgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cloudView.frame.size.width, cloudView.frame.size.height)];
    bgImageView.image = [UIImage imageNamed:@"fon"];
    [cloudView addSubview:bgImageView];

    UILabel * titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 25)];
    titleLbl.backgroundColor = [UIColor whiteColor];
    [titleLbl setTextColor:[UIColor blackColor]];
    titleLbl.text = mTitle;
    titleLbl.textAlignment = NSTextAlignmentCenter;
    [cloudView addSubview:titleLbl];
    
    UIButton * clockImage = [[UIButton alloc] initWithFrame:CGRectMake(0, 25, 150, 25)];
    [clockImage.titleLabel setTextColor:[UIColor blackColor]];
    [clockImage setTintColor:[UIColor blackColor]];
    
    [clockImage setTitle:@"Repair Phone No See here" forState:UIControlStateNormal];
    [clockImage.titleLabel setFont:[UIFont systemFontOfSize:10]];
    clockImage.backgroundColor = [UIColor whiteColor];
    [clockImage addTarget:self action:@selector(phoneAction) forControlEvents:UIControlEventTouchUpInside];
    [clockImage setTitleColor:[UIColor colorWithRed:36/255.0 green:71/255.0 blue:113/255.0 alpha:1.0] forState:UIControlStateNormal];
    clockImage.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [cloudView addSubview:clockImage];

    cloudView.backgroundColor = [UIColor clearColor];
    annotationViewRepaier = (DXAnnotationView*)[self.mMapView dequeueReusableAnnotationViewWithIdentifier:@"Varpet"];
    
    if (!annotationViewRepaier) {
        annotationViewRepaier = [[DXAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"Varpet" pinView:image calloutView:nil settings:[DXAnnotationSettings defaultSettings]];
        
    }
    
    annotationViewRepaier.detailCalloutAccessoryView = cloudView;
    [annotationViewRepaier insertTitleTolabel:annotation.title];
    [annotationViewRepaier setEnabled:NO];
    [annotationViewRepaier showCalloutView];
    annotationViewRepaier.canShowCallout = YES;
    
    return annotationViewRepaier;
    
}
-(void)phoneAction {
    NSLog(@"call call ");
    //mPhoneNumber
    NSString *phoneNumber = mPhoneNumber;
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"Phone number = %@",phoneNumber);
    NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:phoneNumber]];
    NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:phoneNumber]];
    
    if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
        [UIApplication.sharedApplication openURL:phoneUrl];
    } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
        [UIApplication.sharedApplication openURL:phoneFallbackUrl];
    } else {
        // Show an error message: Your device can not do phone calls.
    }
    

}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    
    
    MKAnnotationView *annotation=(MKAnnotationView*)view.annotation;
    
    //do something with your annotation
    
}

#pragma mark --MKMap

- (void)addPlacemarkAnnotationToMap:(CLPlacemark *)placemark addressString:(NSString *)address {
    [self.mMapView removeAnnotation:selectedPlaceAnnotation];
    //[selectedPlaceAnnotation release];
    
    selectedPlaceAnnotation = [[MKPointAnnotation alloc] init];
    selectedPlaceAnnotation.coordinate = placemark.location.coordinate;
    selectedPlaceAnnotation.title = address;
    [self.mMapView addAnnotation:selectedPlaceAnnotation];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)addStarWithRate:(NSInteger)rate {
    switch (rate) {
        case 1:
            [self.star1 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star2 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.star3 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.star4 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.star5 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.view layoutIfNeeded];
            break;
        case 2:
            [self.star1 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star2 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star3 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.star4 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.star5 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.view layoutIfNeeded];
            break;
        case 3:
            [self.star1 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star2 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star3 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star4 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.star5 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.view layoutIfNeeded];
            break;
        case 4:
            [self.star1 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star2 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star3 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star4 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star5 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.view layoutIfNeeded];
            break;
        case 5:
            [self.star1 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star2 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star3 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star4 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.star5 setImage:[UIImage imageNamed:@"starFull"] forState:UIControlStateNormal];
            [self.view layoutIfNeeded];
            break;
        default:
            [self.star1 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.star2 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.star3 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.star4 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.star5 setImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
            [self.view layoutIfNeeded];
            break;
    }
}
- (IBAction)starsAction:(id)sender {
   __block UITextField * comentTF = [[UITextField alloc] init];
    
    [self addStarWithRate:[sender tag]];
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:@"Comment"
                                 message:@""
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Rate"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                             if([self checkInternetConnection]){
                                 [ProgressHUD show:@"Please wait..."];
                                 //self.mVisualView.hidden = NO;
                                 RateOrderRequest * mRateOrderRequest = [[RateOrderRequest alloc] initWithApiKey:_userAccount.apiKey withRate:[NSString stringWithFormat:@"%lD",(long)[sender tag]] comment:comentTF.text order_id:orderId];
                                 
                                 [mRateOrderRequest postWithSuccess:^(NSDictionary *response) {
                                     NSLog(@"RateOrderRequest RESPONS = %@",response);
                                     NSError * error;
                                     NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
                                     NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                     NSString * message = [dictionary objectForKey:@"message"];
                                     
                                     // [mRateDictionary setObject:[NSString stringWithFormat:@"%ld",(long)[sender tag]] forKey:orderId];
                                     
                                     mRateDictionary = [[NSUserDefaults standardUserDefaults] valueForKey:@"isRate"];
                                     rateAray = [[mRateDictionary allValues] mutableCopy];
                                     rateKeyArray = [[mRateDictionary allKeys] mutableCopy];
                                     
                                     [rateAray addObject:[NSString stringWithFormat:@"%ld",(long)[sender tag]]];
                                     [rateKeyArray addObject:orderId];
                                     
                                     mRateDictionary = [[NSMutableDictionary alloc] initWithObjects:rateAray forKeys:rateKeyArray];
                                     
                                     
                                     
                                     
                                     
                                     [[NSUserDefaults standardUserDefaults] setObject:mRateDictionary forKey:@"isRate"];
                                     [ProgressHUD dismiss];
                                     //self.mVisualView.hidden = YES;
                                     [self showAlertWithMessage:@"Info" andText:message];
                                 } failure:^(NSError *error, NSInteger responseCode) {
                                     [ProgressHUD dismiss];
                                     //self.mVisualView.hidden = YES;
                                     NSLog(@"RateOrderRequest ERROR = %@",error.description);
                                 }];

                             }else{
                                 [ProgressHUD dismiss];
                                 //self.mVisualView.hidden = YES;
                                 [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
                             }
                             
                             

                             
                             
                             [view dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    [view addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        comentTF = textField;
        textField.placeholder = @"Your Comment";
        textField.secureTextEntry = NO; //for passwords etc
    }];
    [view addAction:ok];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}

- (IBAction)backAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void) showAlertWithMessage:(NSString *) message andText:(NSString *) text {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:message
                                  message:text
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [super presentViewController:alert animated:YES completion:nil];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                             
                         }];
    [alert addAction:ok];
    
}

-(BOOL)checkInternetConnection{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        mIsConnected = NO;
        NSLog(@"There IS NO internet connection");
    } else {
        NSLog(@"There IS internet connection");
        mIsConnected = YES;
    }
    return mIsConnected;
}
-(void)showAlertWithMessage:(NSString*)message andTitle:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}


@end
