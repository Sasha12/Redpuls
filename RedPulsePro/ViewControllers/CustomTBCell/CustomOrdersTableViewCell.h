//
//  CustomOrdersTableViewCell.h
//  RedPulsePro
//
//  Created by MacMini on 10/22/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomOrdersTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mCustomerImage;
@property (weak, nonatomic) IBOutlet UILabel *mNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *mDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *mOrderNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *mPriceLbl;


@end
