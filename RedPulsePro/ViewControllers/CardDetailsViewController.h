//
//  CardDetailsViewController.h
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *cardNumberTextField;

@property (weak, nonatomic) IBOutlet UITextField *cardNameTextFieald;

@property (weak, nonatomic) IBOutlet UITextField *securCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *monthChooseBtn;
@property (weak, nonatomic) IBOutlet UIButton *yearChooseBtn;


@property (weak, nonatomic) IBOutlet UIImageView *monthArrow;
@property (weak, nonatomic) IBOutlet UIImageView *yearArrow;

@property (weak, nonatomic) IBOutlet UIButton *addButton;

@property (weak, nonatomic) IBOutlet UILabel *expirationMonth;
@property (weak, nonatomic) IBOutlet UILabel *expirationYearLbl;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *mVisualView;

- (IBAction)backAction:(id)sender;
- (IBAction)monthAction:(id)sender;
- (IBAction)yearAction:(id)sender;
- (IBAction)chackboxAction:(id)sender;
- (IBAction)addAction:(id)sender;

@end
