//
//  CustomCardsListTableViewCell.m
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "CustomCardsListTableViewCell.h"
#import "AppDelegate.h"

@implementation CustomCardsListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)deleteAction:(id)sender {
    AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appObj.selectedCardCellIndex = [sender tag];
    NSLog(@"deleteAction with tag = %ld",(long)[sender tag]);
     [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteButtonPress" object:nil];
}

- (IBAction)editAction:(id)sender {
    AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appObj.selectedCardCellIndex = [sender tag];
    NSLog(@"editAction  with tag = %ld",(long)[sender tag]);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EditButtonPress" object:nil];
}
@end
