//
//  CustomAddCardTableViewCell.h
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAddCardTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mBackgroungredView;
@property (weak, nonatomic) IBOutlet UILabel *addCardLbl;
@end
