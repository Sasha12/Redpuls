//
//  CustomCardsListTableViewCell.h
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCardsListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *defoultChackboxCircle;
@property (weak, nonatomic) IBOutlet UIImageView *cardImage;
@property (weak, nonatomic) IBOutlet UILabel *cardName;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberLbl;
@property (weak, nonatomic) IBOutlet UILabel *expireDateLbl;
@property (weak, nonatomic) IBOutlet UIButton *edditbutton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIImageView *editImage;



- (IBAction)deleteAction:(id)sender;
- (IBAction)editAction:(id)sender;

@end
