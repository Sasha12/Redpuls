    //
//  LounchViewController.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/    /16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "AppDelegate.h"
#import "LounchViewController.h"
#import "CustomerAccount.h"
#import "UserAccount.h"
#import "UpdateTokenRequest.h"
#import "UpdateLocationsRecuest.h"
#import <CoreLocation/CoreLocation.h>
#import "RepairerAccount.h"
#import "MainViewController.h"
#import "GetCustomerHistoryRequest.h"
#import "CustomerHistoryList.h"
#import "GetServicesRequest.h"
#import "Services.h"
#import "ServiceItems.h"
#import "RootViewController.h"
#import "RepairerViewController.h"
#import "GetRepairerHistory.h"
#import "ProductDetails.h"
#import "RepairerHistoryList.h"
#import "GetCustomerAddressRequest.h"
#import "CustomerAddressList.h"
#import "Reachability.h"

#import <Realm.h>

@interface LounchViewController () {
    AppDelegate * appObj;
    NSArray * mUsers;
    
    RepairerAccount * repairerAccount;
    CLLocationManager *locationManager;
    
    NSString* mLatitude;
    NSString* mLongitute;
    NSArray* arrayWithServicesList;
    NSInteger startValue;
    BOOL isNoUser;
    BOOL isStopHeartAnimation;
    BOOL mIsConnected;

    
}
@property() CustomerAccount * customerAccount;

@end

@implementation LounchViewController

-(void)viewDidAppear:(BOOL)animated {
     self.mProgressView.progress = 0;
    if(isNoUser){
        [self animation:50];
    }

//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:1];
//    [UIView setAnimationDelay:10.0];
//    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
//    
//     self.mProgressView.progress = 1;
//    [UIView commitAnimations];
    
    
    
    //[self getStartedAction];
    
//    [UIView animateWithDuration:7 animations:^{
//        [self.mProgressView setProgress:1.0 animated:YES];
//
//    } completion:^(BOOL finished) {
//        [NSTimer scheduledTimerWithTimeInterval:7
//                                         target:self
//                                       selector:@selector(getStartedAction)
//                                       userInfo:nil
//                                        repeats:NO];
//           }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    isStopHeartAnimation = NO;
    appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    
    

    
    
    RLMResults<RepairerAccount *> *repairerAccounts = [RepairerAccount allObjects];
    repairerAccount = [repairerAccounts firstObject];
    RLMResults<CustomerAccount *> *mRepairerAccount = [CustomerAccount allObjects];
    _customerAccount = [mRepairerAccount firstObject];
    RLMResults<UserAccount *> *mUserAccount = [UserAccount allObjects];
    
    mUsers = (NSArray*)mUserAccount;
    
//    [UIView animateWithDuration:0.5 animations:^{
//        self.mLogo.transform = CGAffineTransformMakeScale(0.5, 0.5);
//    }
//                     completion:^(BOOL finished){
//                         [UIView animateWithDuration:0.5 animations:^{
//                             self.mLogo.transform = CGAffineTransformMakeScale(1, 1);
//                         }];
//                     }];
    
    [UIView animateKeyframesWithDuration:0.7 delay:0.0 options:UIViewKeyframeAnimationOptionAutoreverse | UIViewKeyframeAnimationOptionRepeat animations:^{
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.7 animations:^{
            self.mLogo.transform = CGAffineTransformMakeScale(0.7, 0.7);
        }];
        [UIView addKeyframeWithRelativeStartTime:0.7 relativeDuration:0.7 animations:^{
            self.mLogo.transform = CGAffineTransformMakeScale(1, 1);

        }];
    } completion:nil];
    if (_customerAccount) {
        
        if([self checkInternetConnection]){
             [self getCustomerAddressList];
        }else{
            [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
        }

        
        
       
        
        

    }else if(repairerAccount){
        if([self checkInternetConnection]){
        [self getRepaierHistory];
        }else{
            [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
        }
    }else{
        isNoUser = YES;
    }
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)getStartedAction {
    
    if([self checkInternetConnection]){
        
        RLMResults<UserAccount *> *mUserAccount = [UserAccount allObjects];
        
        mUsers = (NSArray*)mUserAccount;
        
        
        
        if (mUsers.count > 0) {
            UpdateTokenRequest * updateTokenRequest = [[UpdateTokenRequest alloc] initWithOldToken:[[mUsers objectAtIndex:0] valueForKey:@"deviceToken"] andNewToken:appObj.gDeviceToken andApiKey:[[mUsers objectAtIndex:0] valueForKey:@"apiKey"]];
            [updateTokenRequest postWithSuccess:^(NSDictionary *response) {
                NSLog(@"Update Token response = %@",response);
            } failure:^(NSError *error, NSInteger responseCode) {
                NSLog(@"Update Token error = %@",error.description);
            }];
            
            
            if ([[mUsers objectAtIndex:0] valueForKey:@"isUserLogined"] && [[[mUsers objectAtIndex:0] valueForKey:@"role"] integerValue] == 20) {
                //[self performSegueWithIdentifier:@"goToProfilePage" sender:nil];
                [self goToCustomerPage];
                
//                UpdateLocationsRecuest * updateLocationsRecuest = [[UpdateLocationsRecuest alloc] initWithApiKey:[[mUsers objectAtIndex:0] valueForKey:@"apiKey"] andUserLatitute:mLatitude andUserLongitute:mLongitute andUserId:@""];
//                
//                [updateLocationsRecuest postWithSuccess:^(NSDictionary *response) {
//                    NSLog(@"Update User Location = %@",response);
//                    //[self sendUpdateLocationRequest];
//                } failure:^(NSError *error, NSInteger responseCode) {
//                    NSLog(@"Error Update User Location = %@",error.description);
//                }];
                
            }else if ([[mUsers objectAtIndex:0] valueForKey:@"isUserLogined"] && [[[mUsers objectAtIndex:0] valueForKey:@"role"] integerValue] == 10) {
                
                locationManager.desiredAccuracy = kCLLocationAccuracyBest;
                locationManager = [[CLLocationManager alloc] init];
                locationManager.delegate = self;
                locationManager.distanceFilter = kCLDistanceFilterNone;
                locationManager.desiredAccuracy = kCLLocationAccuracyBest;
                
                [self updateLocation];
                //[self performSelectorInBackground:@selector(updateLocation) withObject:nil];
                
                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
                    [locationManager requestWhenInUseAuthorization];
                
                [locationManager requestWhenInUseAuthorization];
                //[locationManager requestAlwaysAuthorization];
                [locationManager startUpdatingLocation];
                
                [self goToRepaierPage];
                //[self performSegueWithIdentifier:@"goReaier" sender:nil];
                
            }else {
                [self performSegueWithIdentifier:@"goToSignInPage" sender:nil];
            }
        }else {
            UserAccount * userAccount = [[UserAccount alloc] initWithUserName:@"Name" andToken:appObj.gDeviceToken andApiKey:@"apiKey" andRole:1 andIsUserLogined:YES];
            [userAccount createOrUpdateCustomerAccountInStorage];
            
            
            [self performSegueWithIdentifier:@"goToSignInPage" sender:nil];
        }
        

       
    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }
    


    
    
}
-(void)getCustomerAddressList {
   __block NSInteger addressCount = 0;
    GetCustomerAddressRequest * mGetCustomerAddressRequest = [[GetCustomerAddressRequest alloc] initWithApiKey:_customerAccount.ApiKey withCustomerId:_customerAccount.UserId];
    
    [mGetCustomerAddressRequest postWithSuccess:^(NSDictionary *response) {
        //NSLog(@"mGetCustomerAddressRequest response = %@",response);
        NSError * error;
        NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *addressListDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
      NSMutableArray* addressListArray = (NSMutableArray*)[addressListDictionary objectForKey:@"message"];
        
        for (int i = 0; i < addressListArray.count; i++) {
            addressCount++;
            CustomerAddressList * mCustomerAddressList = [[CustomerAddressList alloc] initWithRestAPIResponse:[addressListArray objectAtIndex:i]];
            [mCustomerAddressList createOrUpdateCustomerAccountInStorage];
        }
       // NSLog(@"addressListArray = %@",addressListArray);
       
        
        if (addressListArray.count < 50) {
            [self performSelector:@selector(stopAlAnimations) withObject:nil afterDelay:5];
        }else {
            if (addressCount == addressListArray.count) {
                [self getServices];
            }
        }
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"mGetCustomerAddressRequest error = %@",error.description);
    }];
}
-(void)stopAlAnimations {
    [self getServices];
     [self.mLogo.layer removeAllAnimations];
}
-(void)getServices {
   __block NSInteger count = 0;
    GetServicesRequest * mGetServicesRequest = [[GetServicesRequest alloc] initWithApiKey:_customerAccount.ApiKey];
    [mGetServicesRequest postWithSuccess:^(NSDictionary *response) {
        //NSLog(@"GetServicesRequest respons = %@",response);
        NSError * error;
        NSData* serviceData = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *servicesList = [NSJSONSerialization JSONObjectWithData:serviceData options:kNilOptions error:&error];
        NSDictionary * messageForServices = [servicesList objectForKey:@"message"];
        
        NSArray * servicesNamesArray = [[NSArray alloc] init];
        NSString* serviceNameStr;
        NSArray* servicesDetailsArray = [[NSArray alloc] init];
        NSMutableArray* servicesDetailsRLMOBjectsArray = [[NSMutableArray alloc] init];
        servicesNamesArray = [messageForServices allKeys];
        
        
               RLMRealm *realm1 = [RLMRealm defaultRealm];
        RLMResults<ServiceItems *> *mServices1 = [ServiceItems allObjects];
        NSArray * mServicesArr1 = (NSArray*)mServices1;
        
        if (mServicesArr1.count > 0) {
            //for (int i = 0; i < [mServicesArr1 count]; i++) {
                [realm1 beginWriteTransaction];
                [realm1 deleteObjects:mServices1];
                [realm1 commitWriteTransaction];
            //}
        }

        RLMRealm *realm = [RLMRealm defaultRealm];
        RLMResults<Services *> *mServices = [Services allObjects];
        NSArray * mServicesArr = (NSArray*)mServices;
        
        if (mServicesArr.count > 0) {
           // for (int i = 0; i < [mServicesArr count]; i++) {
                [realm beginWriteTransaction];
                [realm deleteObjects:mServices];
                [realm commitWriteTransaction];
            //}
        }

        for (int i = 0; i < servicesNamesArray.count; i++) {
            count++;
            if ([servicesNamesArray objectAtIndex:i]) {
                serviceNameStr = [servicesNamesArray objectAtIndex:i];
                
                servicesDetailsArray = [messageForServices valueForKey:serviceNameStr];
                servicesDetailsRLMOBjectsArray = [[NSMutableArray alloc] init];
                for (int j = 0 ; j < servicesDetailsArray.count; j++) {
                     NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[servicesDetailsArray objectAtIndex:j] valueForKey:@"service_logo"]]];
                    ServiceItems * mServiceItems = [[ServiceItems alloc] initWithRestAPIResponse:[servicesDetailsArray objectAtIndex:j] andImageData:imageData];
                    [mServiceItems createOrUpdateCustomerAccountInStorage];
                }
                
                RLMResults<ServiceItems *> *mServiceItems = [ServiceItems allObjects];
                startValue = mServiceItems.count - servicesDetailsArray.count;
                for (int k = (int)startValue;k < mServiceItems.count;k++) {
                    RLMObject *object = [mServiceItems objectAtIndex:k];
                    [servicesDetailsRLMOBjectsArray addObject:object];
                }
                
                Services * mServices = [[Services alloc] initWithRestAPIResponse:(RLMArray *)servicesDetailsRLMOBjectsArray name:serviceNameStr];
                [mServices createOrUpdateCustomerAccountInStorage];
                
            }
            
            
        }
        if (count == servicesNamesArray.count) {
            [self getCustomerHistoryList];
        }
        
        
         RLMResults<Services *> *mService = [Services allObjects];
       // NSLog(@"mService = %@",mService);
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"GetServicesRequest error = %@",error.description);
    }];
}
-(void)getCustomerHistoryList {
    

    GetCustomerHistoryRequest *mGetCustomerHistoryRequest = [[GetCustomerHistoryRequest alloc] initWithApiKey:_customerAccount.ApiKey andUserId:_customerAccount.UserId];
    
    [mGetCustomerHistoryRequest postWithSuccess:^(NSDictionary *response) {
        
       // NSLog(@"GetCustomerHistoryRequest RESPONS = %@",response);
        
        NSError * error;
        NSData* serviceData = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *servicesList = [NSJSONSerialization JSONObjectWithData:serviceData options:kNilOptions error:&error];
        NSArray * messageForServices = [servicesList objectForKey:@"message"];
        appObj.serviceList = messageForServices;
        arrayWithServicesList = messageForServices;
        appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
       // NSLog(@"arrayWithServicesList = %@",arrayWithServicesList);
        appObj.historyCount = messageForServices.count;
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        RLMResults<CustomerHistoryList *> *mCustomerHistoryList = [CustomerHistoryList allObjects];
        NSArray * mCustomerHistoryListArray = (NSArray*)mCustomerHistoryList;
        
        if (mCustomerHistoryListArray.count > 0) {
            for (int i = 0; i < [mCustomerHistoryListArray count]; i++) {
                [realm beginWriteTransaction];
                [realm deleteObject:[mCustomerHistoryListArray objectAtIndex:i]];
                [realm commitWriteTransaction];
            }
        }
        CGFloat count = arrayWithServicesList.count;
        CGFloat tiv = 1;
        CGFloat progress = tiv / count;
      __block  CGFloat prog = 0;
        dispatch_async(dispatch_get_main_queue(), ^{
          
           
             [self animation:count];
                  });
//
        
       dispatch_async(dispatch_get_global_queue(0, 0), ^{
           for (int i = 0; i < [arrayWithServicesList count]; i++) {
               prog = prog + progress;
               NSLog(@"progress = %f",prog);
               
               // self.mProgressView.progress = prog;
               
               // [self.view layoutIfNeeded];
               
               
               
               NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[arrayWithServicesList objectAtIndex:i] valueForKey:@"repairer_image"]]];
               CustomerHistoryList * customerHistoryList = [[CustomerHistoryList alloc] initWithRestAPIResponse:[arrayWithServicesList objectAtIndex:i] andKey:[NSString stringWithFormat:@"%d",i] imageData:imageData];
               
               [customerHistoryList createOrUpdateCustomerAccountInStorage];
               
           }
       });
     
        
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"GetCustomerHistoryRequest ERROR = %@",error.description);
    }];
}

-(void)getRepaierHistory {
    RLMResults<RepairerAccount *> *mRepairerAccount = [RepairerAccount allObjects];
    repairerAccount = [mRepairerAccount firstObject];
    
    //[self performSelectorInBackground:@selector(startProgressHud) withObject:nil];
    
    GetRepairerHistory* getRepairerHistory = [[GetRepairerHistory alloc] initWithApiKey:repairerAccount.ApiKey andUserId:repairerAccount.UserId];
    
    
    
    
    [getRepairerHistory postWithSuccess:^(NSDictionary *response) {
        
        //RepairerHistoryList
        
        
        
        NSError * error;
        NSData* serviceData = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *servicesList = [NSJSONSerialization JSONObjectWithData:serviceData options:kNilOptions error:&error];
        NSArray * messageForServices = [servicesList objectForKey:@"message"];
        appObj.serviceList = messageForServices;
        arrayWithServicesList = messageForServices;
        
        NSLog(@"arrayWithServicesList = %@",arrayWithServicesList);
        
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        RLMResults<RepairerHistoryList *> *mCustomerHistoryList = [RepairerHistoryList allObjects];
        NSArray * mRepairerHistoryListArray = (NSArray*)mCustomerHistoryList;
   
        if (mRepairerHistoryListArray.count > 0) {
            for (int i = 0; i < [mRepairerHistoryListArray count]; i++) {
                [realm beginWriteTransaction];
                [realm deleteObject:[mRepairerHistoryListArray objectAtIndex:i]];
                [realm commitWriteTransaction];
            }
        }
        CGFloat count = arrayWithServicesList.count;
        CGFloat tiv = 1;
        CGFloat progress = tiv / count;
        __block  CGFloat prog = 0;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self animation:count];
        });

        int y = 0;
        for (int i = 0; i < [arrayWithServicesList count]; i++) {
            
            
            for (int j = 0 ; j < [[[arrayWithServicesList objectAtIndex:i] valueForKey:@"product_details"] count]; j++) {
                ProductDetails * mProductDetails = [[ProductDetails alloc] initWithRestAPIResponse:[[[arrayWithServicesList objectAtIndex:i] valueForKey:@"product_details"] objectAtIndex:j] andKey:y];
                [mProductDetails createOrUpdateCustomerAccountInStorage];
                RLMResults<ProductDetails *> *mProductDetails11 = [ProductDetails allObjects];
                NSLog(@"mProductDetails11 = %ld",[mProductDetails11 count]);
                y++;
                NSLog(@"y = %d",y);
                
                startValue = [mProductDetails11 count] - j - 1;
            }
            RLMResults<ProductDetails *> *mProductDetails = [ProductDetails allObjects];
            
            NSMutableArray *array = [NSMutableArray array];
            for (int k = (int)startValue;k < mProductDetails.count;k++) {
                RLMObject *object = [mProductDetails objectAtIndex:k];
                [array addObject:object];
            }
            
            NSLog(@"array = %@",array);
            RepairerHistoryList * repairerHistoryList = [[RepairerHistoryList alloc] initWithRestAPIResponse:[arrayWithServicesList objectAtIndex:i] with:(RLMArray *)array];
            
            [repairerHistoryList createOrUpdateCustomerAccountInStorage];
            
            
           
            
        }
        
    } failure:^(NSError *error, NSInteger responseCode) {

}];
    
    
    
}
-(void)animation:(CGFloat)count {
    if (count < 50) {
        count = 50;
    }
    [UIView animateWithDuration:count*0.1 animations:^{
        [self.mProgressView setProgress:1 animated:YES];
        
    } completion:^(BOOL finished) {
        [NSTimer scheduledTimerWithTimeInterval:count*0.1
                                         target:self
                                       selector:@selector(getStartedAction)
                                       userInfo:nil
                                        repeats:NO];
    }];

}
-(void)goToRepaierPage {
    // Override point for customization after application launch.
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    //UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationControllerRepaier"];
    RepairerViewController * repairerViewController = [storyboard instantiateViewControllerWithIdentifier:@"RepairerViewController"];
    MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainIdentifire"];
    mainViewController.rootViewController = repairerViewController;
    [mainViewController setupWithPresentationStyle:LGSideMenuPresentationStyleSlideAbove type:3];
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    
    window.rootViewController = mainViewController;
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
    
}

-(void)goToCustomerPage {
    // Override point for customization after application launch.
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
   // UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    RootViewController * rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"RootViewController"];
    MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainIdentifire"];
    mainViewController.rootViewController = rootViewController;
    [mainViewController setupWithPresentationStyle:LGSideMenuPresentationStyleSlideAbove type:3];
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    
    window.rootViewController = mainViewController;
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];

}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    mLongitute = [NSString stringWithFormat:@"%.10f", newLocation.coordinate.longitude];
    mLatitude = [NSString stringWithFormat:@"%.10f", newLocation.coordinate.latitude];
   }



- (void)updateLocation {
   appObj.myTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self
                                             selector:@selector(sendUpdateLocationRequest) userInfo:nil repeats:YES];
}
- (void)sendUpdateLocationRequest {
    NSLog(@"bbbbbbbbbbbbbbbbbbbbbb");
    // CLLocation *currentLocation = [[CLLocation alloc] init];
    
//    RLMResults<RepairerAccount*> * repairerAccounts = [RepairerAccount allObjects];
//    RepairerAccount * mRepairerAccount = [repairerAccounts firstObject];
    
    UpdateLocationsRecuest * updateLocationsRecuest = [[UpdateLocationsRecuest alloc] initWithApiKey:repairerAccount.ApiKey andUserLatitute:mLatitude andUserLongitute:mLongitute andUserId:repairerAccount.UserId];
    
    [updateLocationsRecuest postWithSuccess:^(NSDictionary *response) {
        NSLog(@"Update User Location = %@",response);
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"Error Update User Location = %@",error.description);
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)checkInternetConnection{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        mIsConnected = NO;
        NSLog(@"There IS NO internet connection");
    } else {
        NSLog(@"There IS internet connection");
        mIsConnected = YES;
    }
    return mIsConnected;
}
-(void)showAlertWithMessage:(NSString*)message andTitle:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}


@end
