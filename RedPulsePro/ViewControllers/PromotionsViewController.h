//
//  PromotionsViewController.h
//  RedPulsePro
//
//  Created by MacMini on 11/27/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromotionsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *mPromCodeFieldTF;
@property (weak, nonatomic) IBOutlet UIButton *mApplyBtn;

@property (weak, nonatomic) IBOutlet UITableView *mTableView;

@property (weak, nonatomic) IBOutlet UIVisualEffectView *mVisualView;





- (IBAction)applyAtion:(id)sender;

@end
