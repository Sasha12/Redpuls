//
//  BaseViewController.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/2/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

-(void) showAlertWithMessage:(NSString *) message andText:(NSString *) text;

@end
