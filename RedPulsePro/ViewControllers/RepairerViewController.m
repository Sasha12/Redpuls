 //
//  RepairerViewController.m
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/6/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import "RepairerViewController.h"
#import <Realm.h>
#import "RepairerAccount.h"
#import "GetRepairerHistory.h"
#import "RepairerHistoryList.h"
#import "LogOutRequest.h"
#import "AppDelegate.h"
#import "UserAccount.h"
#import "CustomUITebleViewCell.h"
#import "AcceptRequest.h"
#import "DoneOrderRequest.h"
#import "UpdateLocationsRecuest.h"
#import <CoreLocation/CoreLocation.h>
#import "ProductDetails.h"
#import "DoneOrderRequest.h"
#import "MapAnotation.h"
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import "ALCustomColoredAccessory.h"
#import "CustomOrdersTableViewCell.h"
#import <DXAnnotationView.h>
#import <DXAnnotationSettings.h>
#import <ProgressHUD.h>
#import "Reachability.h"

@interface RepairerViewController () <CLLocationManagerDelegate,MKMapViewDelegate,UITableViewDelegate,UITableViewDataSource,UITableViewDataSourcePrefetching> {
    AppDelegate * appObj;
    NSArray * arrayWithOrders;
    RepairerAccount * repairerAccount;
    CLLocationManager *locationManager;
    NSTimer* myTimer;
    NSString* mLatitude;
    NSString* mLongitute;
    NSString* stringWithOrdersId;
    NSArray * arrayWithServicesList;
    NSMutableArray* arrayWithValidOrderList;
    NSInteger mIndex;
    NSInteger startValue;
    BOOL mIsConnected;
}

@property()SPGooglePlacesAutocompleteQuery* query;

@end

@implementation RepairerViewController


-(void)viewWillAppear:(BOOL)animated {
    
//    UIApplication * application = [UIApplication sharedApplication].delegate;
//    application.applicationIconBadgeNumber = 0;
    mIndex = 0;
    stringWithOrdersId = @"";
//    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    locationManager = [[CLLocationManager alloc] init];
//    locationManager.delegate = self;
//    locationManager.distanceFilter = kCLDistanceFilterNone;
//    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    // [self updateLocation];
//    [self performSelectorInBackground:@selector(updateLocation) withObject:nil];
//
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
//        [locationManager requestWhenInUseAuthorization];
//    
//    [locationManager requestWhenInUseAuthorization];
//    [locationManager requestAlwaysAuthorization];
//    [locationManager startUpdatingLocation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handelNotification:)
                                                 name:ShowNotification
                                               object:nil];

    
//    self.mUserImage.layer.masksToBounds = YES;
//    self.mUserImage.layer.cornerRadius = self.mUserImage.frame.size.width/2;
//    self.mUserImage.layer.borderWidth = 3.0f;
//    self.mUserImage.layer.borderColor = [UIColor colorWithRed:71/255.0 green:67/255.0 blue:65/255.0 alpha:1.0].CGColor;
    
    self.mTableView.delegate = self;
    self.mTableView.dataSource = self;
    //[ProgressHUD show:@"Please wait..."];
    //[self performSelectorInBackground:@selector(startProgressHud) withObject:nil];
    
    if([self checkInternetConnection]){
        [self getRepaierHistory];
    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }
    

   
    
    // ap krnas iPadov kpnis or heraxosov el zangem xosanq?ok
}

-(void)getRepaierHistory {
    RLMResults<RepairerAccount *> *mRepairerAccount = [RepairerAccount allObjects];
    repairerAccount = [mRepairerAccount firstObject];
    
    //[self performSelectorInBackground:@selector(startProgressHud) withObject:nil];
    
 
      
            RLMResults<RepairerHistoryList *> *mRepairerHistoryList11 = [RepairerHistoryList allObjects];
            NSLog(@"mRepairerHistoryList11");
            for (int i = 0; i < [mRepairerHistoryList11 count]; i++) {
            if ([[[arrayWithServicesList objectAtIndex:i] valueForKey:@"status"] isEqualToString:@"1"]) {
                [arrayWithValidOrderList addObject:[arrayWithServicesList objectAtIndex:i]];
                mIndex++;
            }
            
        }
        
        if (arrayWithValidOrderList.count > 0) {
            self.mNotOrderLbl.hidden = YES;
            [self recenterMapToPlacemark:CLLocationCoordinate2DMake([[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"lat"] doubleValue], [[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"lng"] doubleValue]) withName:[NSString stringWithFormat:@"%@ %@",[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"customer_name"],[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"customer_surname"]]];
            [self.mMapView reloadInputViews];
            self.mCustomerNameLbl.text = [NSString stringWithFormat:@"%@ %@",[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"customer_name"],[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"customer_surname"]];
            [self.mPhoneNumberBtn setTitle:[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"customer_phone"] forState:UIControlStateNormal];
            [self.view layoutIfNeeded];
        }else {
            self.mNotOrderLbl.hidden = NO;
            self.mCustomerNameLbl.hidden = YES;
            self.mPhoneNumberBtn.hidden = YES;
            self.mDoneBtn.hidden = YES;
        }
        
        RLMResults<RepairerHistoryList *> *repairerAccount = [RepairerHistoryList allObjects];
        arrayWithOrders = (NSArray*)repairerAccount;
        [self.mTableView reloadData];
        [self.mTableView reloadData];
    
 
}
-(void)startProgressHud {
    
}
- (void) handelNotification:(NSNotification*) notification {
    
    appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.mNotOrderLbl.hidden = YES;
    self.mCustomerNameLbl.hidden = NO;
    self.mPhoneNumberBtn.hidden = NO;
    self.mDoneBtn.hidden = NO;
    [self.view layoutIfNeeded];
    self.mCustomerNameLbl.text = appObj.customerNameFromNot;
    //self.mPhoneNumberBtn.titleLabel.text = appObj.customerPhoneNumberFromNot;
    [self.mPhoneNumberBtn setTitle:appObj.customerPhoneNumberFromNot forState:UIControlStateNormal];
    [self.view layoutIfNeeded];
        NSLog(@"Accept Selected Index = %@",appObj.notifierOrderId);
        [self showAcceptAlertWithSelectedIndex:nil];
    //[self getRepaierHistory];
}
    

- (void) viewDidDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)viewDidAppear:(BOOL)animated {

    // [self performSelectorInBackground:@selector(startProgressHud) withObject:nil];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    mIndex = 0;
    startValue = 0;
   // [ProgressHUD show:@"Please wait..."];
    arrayWithValidOrderList = [[NSMutableArray alloc] init];
    
    //========================
    
    if (!expandedSections)
    {
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
    
    self.mTableView.delegate = self;
    self.mTableView.dataSource = self;

    //=======================
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    mLongitute = [NSString stringWithFormat:@"%.10f", newLocation.coordinate.longitude];
    mLatitude = [NSString stringWithFormat:@"%.10f", newLocation.coordinate.latitude];
}


- (void)updateLocation {
    myTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self
                                                      selector:@selector(sendUpdateLocationRequest) userInfo:nil repeats:YES];
}
- (void)sendUpdateLocationRequest {
    
    if([self checkInternetConnection]){
        UpdateLocationsRecuest * updateLocationsRecuest = [[UpdateLocationsRecuest alloc] initWithApiKey:repairerAccount.ApiKey andUserLatitute:mLatitude andUserLongitute:mLongitute andUserId:repairerAccount.UserId];
        
        [updateLocationsRecuest postWithSuccess:^(NSDictionary *response) {
            NSLog(@"Update User Location = %@",response);
        } failure:^(NSError *error, NSInteger responseCode) {
            NSLog(@"Error Update User Location = %@",error.description);
        }];

    }else{
        [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }
    NSLog(@"aaaaaaaaaaaaaaaaaa");
      // CLLocation *currentLocation = [[CLLocation alloc] init];
    
  }



-(void)setImageTo:(UIImageView*) imageView andIndex:(NSInteger)index {
    [imageView setImage:[UIImage imageWithData:
                                     [NSData dataWithContentsOfURL:
                                      [NSURL URLWithString:[[arrayWithServicesList objectAtIndex:index] valueForKey:@"ItemImagePath"]]]]];
}


- (void)showAcceptAlertWithSelectedIndex:(NSInteger)index {
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:appObj.alertMessage
                                  message:appObj.alertText
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* accept = [UIAlertAction
                             actionWithTitle:@"ACCEPT"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 NSLog(@"Accept!!");
                                 
//                                 for (int i = 0; i < appObj.notifierOrderId.count; i++) {
//                                     stringWithOrdersId = [stringWithOrdersId stringByAppendingString:[appObj.notifierOrderId objectAtIndex:i]];
//                                     stringWithOrdersId = [stringWithOrdersId stringByAppendingString:@","];
//`
//                                 }
//                                 
                                 if([self checkInternetConnection]){
                                     AcceptRequest * acceptRequest = [[AcceptRequest alloc] initWithApiKey:repairerAccount.ApiKey andUserId:repairerAccount.UserId andOrderId:appObj.notifierOrderId];
                                     //                                 AcceptRequest * acceptRequest = [[AcceptRequest alloc] initWithApiKey:repairerAccount.ApiKey andUserId:repairerAccount.UserId andOrderId:appObj.notifierOrderId valueForKey:@"OrderId"];
                                     
                                     [acceptRequest postWithSuccess:^(NSDictionary *response) {
                                         
                                         GetRepairerHistory* getRepairerHistory = [[GetRepairerHistory alloc] initWithApiKey:repairerAccount.ApiKey andUserId:repairerAccount.UserId];
                                         
                                         
                                         [getRepairerHistory postWithSuccess:^(NSDictionary *response) {
                                             
                                             //RepairerHistoryList
                                             NSError * error;
                                             NSData* serviceData = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
                                             NSDictionary *servicesList = [NSJSONSerialization JSONObjectWithData:serviceData options:kNilOptions error:&error];
                                             NSArray * messageForServices = [servicesList objectForKey:@"message"];
                                             appObj.serviceList = messageForServices;
                                             arrayWithServicesList = messageForServices;
                                             
                                             NSLog(@"arrayWithServicesList = %@",arrayWithServicesList);
                                             
                                             
                                             RLMRealm *realm = [RLMRealm defaultRealm];
                                             RLMResults<RepairerHistoryList *> *mCustomerHistoryList = [RepairerHistoryList allObjects];
                                             NSArray * mRepairerHistoryListArray = (NSArray*)mCustomerHistoryList;
                                             [arrayWithValidOrderList removeAllObjects];
                                             if (arrayWithValidOrderList.count > 0) {
                                                 for (int i = 0; i < [mRepairerHistoryListArray count]; i ++) {
                                                     [realm beginWriteTransaction];
                                                     [realm deleteObject:[mRepairerHistoryListArray objectAtIndex:i]];
                                                     [realm commitWriteTransaction];
                                                 }
                                             }
                                             
                                             
                                             int f = 0;
                                             for (int i = 0; i < [arrayWithServicesList count]; i++) {
                                                 for (int j = 0 ; j < [[[arrayWithServicesList objectAtIndex:i] valueForKey:@"product_details"] count]; j++) {
                                                     ProductDetails * mProductDetails = [[ProductDetails alloc] initWithRestAPIResponse:[[[arrayWithServicesList objectAtIndex:i] valueForKey:@"product_details"] objectAtIndex:j] andKey:f];
                                                     [mProductDetails createOrUpdateCustomerAccountInStorage];
                                                     f++;
                                                     RLMResults<ProductDetails *> *mProductDetails11 = [ProductDetails allObjects];
                                                     
                                                     startValue = [mProductDetails11 count] - j - 1;
                                                 }
                                                 
                                                 RLMResults<ProductDetails *> *mProductDetails = [ProductDetails allObjects];
                                                 
                                                 
                                                 NSMutableArray *array = [NSMutableArray array];
                                                 for (int k = (int)startValue;k < mProductDetails.count;k++) {
                                                     RLMObject *object = [mProductDetails objectAtIndex:k];
                                                     [array addObject:object];
                                                 }
                                                 
                                                 
                                                 RepairerHistoryList * repairerHistoryList = [[RepairerHistoryList alloc] initWithRestAPIResponse:[arrayWithServicesList objectAtIndex:i] with:(RLMArray*)array];
                                                 [repairerHistoryList createOrUpdateCustomerAccountInStorage];
                                                 
                                                 if ([[[arrayWithServicesList objectAtIndex:i] valueForKey:@"status"] isEqualToString:@"1"]) {
                                                     [arrayWithValidOrderList addObject:[arrayWithServicesList objectAtIndex:i]];
                                                     mIndex++;
                                                 }
                                                 
                                             }
                                             
                                             
                                             RLMResults<RepairerHistoryList *> *mRepairerAccount = [RepairerHistoryList allObjects];
                                             arrayWithOrders = (NSArray*)mRepairerAccount;
                                             [self.mTableView reloadData];
                                             
                                             
                                             
                                             [self recenterMapToPlacemark:CLLocationCoordinate2DMake([[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"lat"] doubleValue], [[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"lng"] doubleValue]) withName:[NSString stringWithFormat:@"%@ %@",[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"customer_name"],[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"customer_surname"]]];
                                             
                                             [self.mMapView reloadInputViews];
                                             
                                             
                                         } failure:^(NSError *error, NSInteger responseCode) {
                                             
                                         }];
                                         
                                         //                                     myTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self
                                         //                                                                              selector:@selector(sendUpdateLocationRequest) userInfo:nil repeats:YES];
                                         // [self performSelectorInBackground:@selector(updateLocation) withObject:nil];
                                         
                                         
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     } failure:^(NSError *error, NSInteger responseCode) {
                                         NSLog(@"error = %@",error.description);
                                     }];
                                     
                                     
                                 }else{
                                     [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
                                 }

                                 
                             }];
    
    UIAlertAction* decline = [UIAlertAction
                              actionWithTitle:@"DECLINE"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  

                                  NSLog(@"Decline!!");
                                  self.mNotOrderLbl.hidden = NO;
                                  self.mCustomerNameLbl.hidden = YES;
                                  self.mPhoneNumberBtn.hidden = YES;
                                  self.mDoneBtn.hidden = YES;
                                  [self.view layoutIfNeeded];
                                  
                                [alert dismissViewControllerAnimated:YES completion:nil];
                              }];
    
    [alert addAction:accept];
    [alert addAction:decline];
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(MKAnnotationView*)mapView:(MKMapView*)mapView viewForAnnotation:(MapAnotation <MKAnnotation>*)annotation
{
    UIView * pinView;
    DXAnnotationView * annotationViewRepaier;
   // NSLog(@"RepaierAnnotation Coordinate Lat = %f   Long = %f",RepaierAnnotation.coordinate.latitude,RepaierAnnotation.coordinate.longitude);
    pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pointer.png"]];
    [pinView setFrame:CGRectMake(0, 0, 40, 40)];
    UIView * cloudView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 80)];
    UIImageView * bgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cloudView.frame.size.width, cloudView.frame.size.height)];
    bgImageView.image = [UIImage imageNamed:@"cursor"];
    [cloudView addSubview:bgImageView];
    
    UIImageView * clockImage = [[UIImageView alloc] initWithFrame:CGRectMake(8, 17, 20, 20)];
    clockImage.image = [UIImage imageNamed:@"jam"];
    clockImage.backgroundColor = [UIColor clearColor];
    [cloudView addSubview:clockImage];
    // [cloudView addSubview:lbl];
    cloudView.backgroundColor = [UIColor clearColor];
    annotationViewRepaier = (DXAnnotationView*)[self.mMapView dequeueReusableAnnotationViewWithIdentifier:@"Varpet"];
    
    if (!annotationViewRepaier) {
        annotationViewRepaier = [[DXAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"Varpet" pinView:pinView calloutView:nil settings:[DXAnnotationSettings defaultSettings]];
        
    }
    
    [annotationViewRepaier insertTitleTolabel:annotation.title];
    [annotationViewRepaier setEnabled:NO];
    [annotationViewRepaier hideCalloutView];
    annotationViewRepaier.canShowCallout = NO;
 
    return annotationViewRepaier;
    
}
#pragma mark --Actions

- (IBAction)logOutAction:(id)sender {
    
    if([self checkInternetConnection]){
        RLMResults<RepairerAccount *> *mRepairerAccount = [RepairerAccount allObjects];
        // RepairerAccount * repairerOldAccount = [mRepairerAccount firstObject];
        NSString * apiKey = [[mRepairerAccount firstObject] valueForKey:@"ApiKey"];
        LogOutRequest * logOutRequest = [[LogOutRequest alloc] initWithApiKey:apiKey];
        
        //    UserAccount * userAccount = [[UserAccount alloc] initWithUserName:@"" andToken:appObj.gDeviceToken andApiKey:@"" andRole:0 andIsUserLogined:NO];
        //    [userAccount createOrUpdateCustomerAccountInStorage];
        
        RLMRealm *realm = [RLMRealm defaultRealm];
        RLMResults<UserAccount *> *userAccounts = [UserAccount allObjects];
        UserAccount *userAccount = [userAccounts firstObject];
        
        //    [realm beginWriteTransaction];
        //    userAccount.usernName = @"";
        //    userAccount.role = 0;
        //    userAccount.isUserLogined = NO;
        //    [realm commitWriteTransaction];
        
        [realm beginWriteTransaction];
        [realm deleteObject:userAccount];
        [realm commitWriteTransaction];
        
        
        //    mCount = mImages.count+1;
        //    [realm transactionWithBlock:^{
        //        mImages.count = mCount;
        //    }];
        //    [mImages createOrUpdateInStorage];
        
        [logOutRequest postWithSuccess:^(NSDictionary *response) {
            NSLog(@"Log out response: = %@",response);
            // CustomerAccount * customerAccount = [[CustomerAccount alloc] initWithOldAccount:customerOldAccount andIsLoginid:NO];
            //[CustomerAccount isU];
            //[[mCustomerAccount firstObject] createOrUpdateCustomerAccountInStorage];
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLogin"];
            
            [self dismissViewControllerAnimated:YES completion:nil];
        } failure:^(NSError *error, NSInteger responseCode) {
            //[self showAlertWithMessage:@"Error" andText:[NSString stringWithFormat:@"Error N:%ld",(long)responseCode]];
            NSLog(@"Error = %@",error.description);
            
        }];

    }else{
       [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
    }
    
    

 
}
- (IBAction)CallAction:(id)sender {
    NSString *phoneNumber = appObj.customerPhoneNumberFromNot;
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"Phone number = %@",phoneNumber);

    NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:phoneNumber]];
    NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:phoneNumber]];
    
    if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
        [UIApplication.sharedApplication openURL:phoneUrl];
    } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
        [UIApplication.sharedApplication openURL:phoneFallbackUrl];
    } else {
        // Show an error message: Your device can not do phone calls.
    }

}

- (IBAction)doneAction:(id)sender {
    
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Info!"
                                  message:@"Are you sure that you want to done this order?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* accept = [UIAlertAction
                             actionWithTitle:@"Done"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 if([self checkInternetConnection]){
                                     DoneOrderRequest * doneOrderRequest = [[DoneOrderRequest alloc] initWithApiKey:repairerAccount.ApiKey andUserId:repairerAccount.UserId andOrderId:[NSString stringWithFormat:@"%@",[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"id"]]];
                                     
                                     [doneOrderRequest postWithSuccess:^(NSDictionary *response) {
                                         NSLog(@"response = %@",response);
                                         [self getRepaierHistory];
                                         
                                         self.mNotOrderLbl.hidden = NO;
                                         self.mCustomerNameLbl.hidden = YES;
                                         self.mPhoneNumberBtn.hidden = YES;
                                         self.mDoneBtn.hidden = YES;
                                         [self.view layoutIfNeeded];
                                         
                                         [self.mMapView removeAnnotations:[self.mMapView annotations]];
                                         [self.mMapView reloadInputViews];
                                         
                                         [self dismissViewControllerAnimated:YES completion:nil];
                                     } failure:^(NSError *error, NSInteger responseCode) {
                                         NSLog(@"error = %@",error.description);
                                         
                                     }];;
                                     

                                 }else{
                                     [self showAlertWithMessage:@"Internet connection is required" andTitle:@""];
                                 }
                                 

                                 
                             }];
    
    UIAlertAction* decline = [UIAlertAction
                              actionWithTitle:@"Cancel"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                [alert dismissViewControllerAnimated:YES completion:nil];
                              }];
    
    [alert addAction:accept];
    [alert addAction:decline];
    
    
    [self presentViewController:alert animated:YES completion:nil];

    
    //=============================
    
    
}

- (void)recenterMapToPlacemark:(CLLocationCoordinate2D)locations withName:(NSString*)name{
    
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    
    span.latitudeDelta = 0.02;
    span.longitudeDelta = 0.02;
    
    region.span = span;
    region.center = locations;
    MapAnotation *  CustomerAnnotation = [[MapAnotation alloc] init];
    CustomerAnnotation.coordinate = locations;
    
  
    CustomerAnnotation.title = name;
    
    
    [self.mMapView addAnnotation:CustomerAnnotation];
    [self.mMapView setRegion:region];
//    mLatitude = [NSString stringWithFormat:@"%f",locations.latitude];
//    mLongitute = [NSString stringWithFormat:@"%f",locations.longitude];
    
}
#pragma mark --MKMap

- (void)addPlacemarkAnnotationToMap:(CLPlacemark *)placemark addressString:(NSString *)address {
    [self.mMapView removeAnnotation:selectedPlaceAnnotation];
    //[selectedPlaceAnnotation release];
    
    selectedPlaceAnnotation = [[MKPointAnnotation alloc] init];
    selectedPlaceAnnotation.coordinate = placemark.location.coordinate;
    selectedPlaceAnnotation.title = address;
    [self.mMapView addAnnotation:selectedPlaceAnnotation];
}

-(void)initSPGooglePlacesAutocompleteQuery {
    _query = [SPGooglePlacesAutocompleteQuery query];
    //_query.input = @"Armenia  gyumri Te";
    //_query.radius = 1000.0;
    _query.language = @"en";
    _query.types = SPPlaceTypeGeocode;
    // _query.location = CLLocationCoordinate2DMake(37.76999, -122.44696);
    
}

//-(void)changeMapToSelectedDropDownAddress:(NSInteger)index {
//    SPGooglePlacesAutocompletePlace *place = [_searchResultPlaces objectAtIndex:index];
//    [place resolveToPlacemark:^(CLPlacemark *placemark, NSString *addressString, NSError *error) {
//        if (error) {
//            SPPresentAlertViewWithErrorAndTitle(error, @"Could not map selected Place");
//        } else if (placemark) {
//            if ([self.mMapView annotations].count > 0) {
//                [self.mMapView removeAnnotation:CustomerAnnotation];
//            }
//            
//            [self recenterMapToPlacemark:placemark.location.coordinate];
//            [self geocoderMethod:placemark.location.coordinate.latitude longitude: placemark.location.coordinate.longitude];
//            
//            //        [self addPlacemarkAnnotationToMap:placemark addressString:addressString];
//            //        [self recenterMapToPlacemark:placemark];
//            //        [self dismissSearchControllerWhileStayingActive];
//            //        [self.searchDisplayController.searchResultsTableView deselectRowAtIndexPath:indexPath animated:NO];
//        }
//    }];
//}
//- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSInteger)indexPath {
//    return [_searchResultPlaces objectAtIndex:indexPath];
//}

/*
-(MKAnnotationView*)mapView:(MKMapView*)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString * const kPinAnnotationIdentifier = @"PinIdentifier";
    if(annotation == self.mMapView.userLocation)
    {
        return nil;
    }
    
    MapAnotation * myAnnotation  = (MapAnotation*)annotation;
    MKAnnotationView *newAnnotation = (MKAnnotationView*)[self.mMapView dequeueReusableAnnotationViewWithIdentifier:kPinAnnotationIdentifier];
    
    if(!newAnnotation){
        newAnnotation = [[MKAnnotationView alloc] initWithAnnotation:myAnnotation reuseIdentifier:@"userloc"];
    }
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 20, 20);
    [btn setBackgroundImage:[UIImage imageNamed:@"timer"] forState:UIControlStateNormal];
    newAnnotation.leftCalloutAccessoryView = btn;//[UIButton buttonWithType:UIButtonTypeCustom];
    // NSDictionary *dict=[alertInfoArray objectAtIndex:myAnnotation.ann_tag];
    newAnnotation.detailCalloutAccessoryView.backgroundColor = [UIColor redColor];
    UIView *anView=[[UIView alloc] init];
    anView.backgroundColor=[UIColor clearColor];
    
    UIImageView *bgImg=[[UIImageView alloc] init];
    bgImg.image=[UIImage imageNamed:@""];
    bgImg.backgroundColor=[UIColor clearColor];
    
    UIImageView *imgView=[[UIImageView alloc] init];
    imgView.tag=myAnnotation.ann_tag;
    
    UILabel *lblName=[[UILabel alloc] init];
    lblName.font=[UIFont systemFontOfSize:12];
    lblName.textAlignment=UITextAlignmentCenter;
    lblName.textColor=[UIColor redColor];
    lblName.backgroundColor=[UIColor clearColor];
    lblName.text=@"TEXT YOU WANT";
    
    newAnnotation.frame=CGRectMake(0, 0, 70, 70);
    anView.frame=CGRectMake(0, 0, 70, 70);
    bgImg.frame=CGRectMake(0, 0, 70, 70);
    //bgImg.image=[UIImage imageNamed:@"man.png"];
    
    imgView.frame=CGRectMake(8,25,35,35);
        imgView.image=[UIImage imageNamed:@"pin.png"];
        newAnnotation.canShowCallout=YES;
        
   
    lblName.frame=CGRectMake(5,79,60,10);
    
    
    //[anView addSubview:lblName];
    [anView addSubview:bgImg];
    [anView addSubview:imgView];
    [newAnnotation addSubview:anView];
    
    
    [newAnnotation setEnabled:YES];
    
    return newAnnotation;
}
*/

//============ TableView Delegates Metods =====================

#pragma mark - Table View
- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    //if (section>0) return YES;
    if([[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"product_details"] count] > 1 ) {
        return YES;
    }
    
    return NO;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrayWithValidOrderList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self tableView:tableView canCollapseSection:section])
    {
        if ([expandedSections containsIndex:section])
        {
            NSLog(@"expandedSections!!!!! count = %ld",[[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"product_details"] count] - 1);
            return [[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"product_details"] count] ; // return rows when expanded
        }
        
        return 1; // only top row showing
    }
    
    // Return the number of rows in the section.
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    CustomOrdersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CustomOrdersTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
//    if ([self tableView:tableView canCollapseSection:indexPath.section])
//    {
//        if (!indexPath.row)
//        {
//            NSInteger index1 = indexPath.row;
//            NSLog(@" indexPath.row = %ld",(long)index1);
//            
//            NSInteger index2 = indexPath.section;
//            NSLog(@" indexPath.section = %ld",(long)index2);
//            // first row
//            cell.mDescription.text = [[[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"product_details"] objectAtIndex:0] valueForKey:@"description"];
//            cell.mNameLbl.text = [[[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"product_details"] objectAtIndex:0] valueForKey:@"name"]; // only top row showing
//            [cell.mDeviceImg setImage:[UIImage imageWithData:
//                                       [NSData dataWithContentsOfURL:
//                                        [NSURL URLWithString:[[[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"product_details"] objectAtIndex:0] valueForKey:@"avatar"]]]]];
//            
//            if ([expandedSections containsIndex:indexPath.section])
//            {
//                cell.accessoryView = [ALCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:ALCustomColoredAccessoryTypeUp];
//            }
//            else
//            {
//                cell.accessoryView = [ALCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:ALCustomColoredAccessoryTypeDown];
//            }
//        }
//        else
//        {
//            
//            NSInteger index = indexPath.row;
//            NSLog(@"%ld",(long)index);
//            
//            NSInteger index3 = indexPath.section;
//            NSLog(@" indexPath.section = %ld",(long)index3);
//            
//            // all other rows
//            cell.mDescription.text = [[[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"product_details"] objectAtIndex:indexPath.row] valueForKey:@"description"];
//            cell.mNameLbl.text = [[[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"product_details"] objectAtIndex:indexPath.row] valueForKey:@"name"]; // only top row showing
//            [cell.mDeviceImg setImage:[UIImage imageWithData:
//                                       [NSData dataWithContentsOfURL:
//                                        [NSURL URLWithString:[[[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"product_details"] objectAtIndex:indexPath.row] valueForKey:@"avatar"]]]]];
//            cell.accessoryView = nil;
//            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        }
//    }
//    else
//    {
//        cell.mDescription.text = [[[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"product_details"] objectAtIndex:indexPath.row] valueForKey:@"description"];
//        cell.mNameLbl.text = [[[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"product_details"] objectAtIndex:indexPath.row] valueForKey:@"name"]; // only top row showing
//        [cell.mDeviceImg setImage:[UIImage imageWithData:
//                                   [NSData dataWithContentsOfURL:
//                                    [NSURL URLWithString:[[[[arrayWithValidOrderList objectAtIndex:0] valueForKey:@"product_details"] objectAtIndex:indexPath.row] valueForKey:@"avatar"]]]]];
//        cell.accessoryView = nil;
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//
//        
//    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            // only first row toggles exapand/collapse
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            NSInteger section = indexPath.section;
            BOOL currentlyExpanded = [expandedSections containsIndex:section];
            NSInteger rows;
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            if (currentlyExpanded)
            {
                rows = [self tableView:tableView numberOfRowsInSection:section];
                [expandedSections removeIndex:section];
                
            }
            else
            {
                [expandedSections addIndex:section];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }
            
            for (int i=1; i<rows; i++)
            {
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                               inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }
            
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            if (currentlyExpanded)
            {
                [tableView deleteRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
                
                cell.accessoryView = [ALCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:ALCustomColoredAccessoryTypeDown];
                
            }
            else
            {
                [tableView insertRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
                cell.accessoryView =  [ALCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:ALCustomColoredAccessoryTypeUp];
                
            }
        }
        else {
            NSLog(@"Selected Section is %ld and subrow is %ld ",(long)indexPath.section ,(long)indexPath.row);
            
        }
        
    }
    else{
        //        DetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
        //        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
-(BOOL)checkInternetConnection{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        mIsConnected = NO;
        NSLog(@"There IS NO internet connection");
    } else {
        NSLog(@"There IS internet connection");
        mIsConnected = YES;
    }
    return mIsConnected;
}
-(void)showAlertWithMessage:(NSString*)message andTitle:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}


//=================================

@end
