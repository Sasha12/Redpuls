//
//  PaymentCardsListViewController.h
//  RedPulsePro
//
//  Created by MacMini on 12/13/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentCardsListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *mTableView;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *mVisualView;

- (IBAction)backAction:(id)sender;
@end
