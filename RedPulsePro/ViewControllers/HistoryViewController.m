//
//  HistoryViewController.m
//  RedPulsePro
//
//  Created by MacMini on 10/23/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import "HistoryViewController.h"
#import "AppDelegate.h"
#import "UserAccount.h"
#import "CustomerHistoryList.h"
#import "RepairerHistoryList.h"
#import <Realm.h>
#import "ALCustomColoredAccessory.h"
#import "CustomOrdersTableViewCell.h"
#import "ProductDetails.h"
#import "MainViewController.h"
#import "GetOrderViewRequest.h"
#import "CustomerAccount.h"
#import <ProgressHUD.h>

@interface HistoryViewController () <UITableViewDelegate,UITableViewDataSource,UITableViewDataSourcePrefetching>{
    
    NSMutableArray * historyArray;
    NSMutableArray * detailsArray;
    NSInteger count;
    
}

@property()UserAccount * userAccount;
@property()CustomerHistoryList * customerHistoryList;
@property()RepairerHistoryList * repairerHistoryList;
@property()AppDelegate * appObj;

@property() CustomerAccount * customerAccount;
@property() NSMutableArray * mCustomerHistoryListArray;

@end

@implementation HistoryViewController

-(void)viewWillAppear:(BOOL)animated{
    
    count = 0;
   // [self.mTableView reloadData];
    self.navigationController.navigationBarHidden = YES;
    
    //self.mVisualEffectView.hidden = NO;
     //self.mVisualEffectView.hidden = YES;
    [ProgressHUD dismiss];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Order History"];
    detailsArray = [[NSMutableArray alloc] init];
    //========================
    [ProgressHUD dismiss];
    //self.mVisualEffectView.hidden = YES;
    if (!expandedSections)
    {
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
    self.appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    if (self.appObj.historyCount < 1) {
        self.mTableView.hidden = YES;
    }else {
        self.mTableView.hidden = NO;
        self.mTableView.delegate = self;
        self.mTableView.dataSource = self;
    }
    
    


    RLMResults<UserAccount*>* mUserAccount = [UserAccount allObjects];
    _userAccount = [mUserAccount firstObject];
   
   
    
}

-(void)viewDidAppear:(BOOL)animated{
    
//    if (_userAccount.role == 20) {
//        NSLog(@"User");
//        RLMResults<CustomerHistoryList*>* mCustomerHistoryList = [CustomerHistoryList allObjects];
//        historyArray = (NSMutableArray*)mCustomerHistoryList;
//    }else{
//        NSLog(@"Varpet");
//        RLMResults<RepairerHistoryList*>* mRepairerHistoryList = [RepairerHistoryList allObjects];
//        historyArray = (NSMutableArray*)mRepairerHistoryList;
//    }
    
    self.appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [self.mTableView reloadData];
   }


//============ TableView Delegates Metods =====================

#pragma mark - Table View
- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
//    //if (section>0) return YES;
//    if ([[[historyArray objectAtIndex:section] valueForKey:@"ProductDetails"] count] > 1) {
//        return YES;
//    }
    return NO;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if ([self tableView:tableView canCollapseSection:section])
//    {
////        if ([expandedSections containsIndex:section])
////        {
////            NSLog(@"expandedSections!!!!!");
////            return [[[historyArray objectAtIndex:section] valueForKey:@"ProductDetails"] count]; // return rows when expanded
////        }
//        
//        return 1; // only top row showing
//    }
//    
//    // Return the number of rows in the section.
    return self.appObj.historyCount;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    count++;
    CustomOrdersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CustomOrdersTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSString* primKey = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
    RLMResults<CustomerHistoryList*>* mCustomerHistoryList= [[CustomerHistoryList objectsWhere:@"primKey == %@",primKey] firstObject];
    
    
    
    cell.mDateLbl.text = [[mCustomerHistoryList  valueForKey:@"UpdatedDate"] stringByReplacingOccurrencesOfString:@"-" withString:@"/"];
    cell.mNameLbl.text = [NSString stringWithFormat:@"%@ %@",[mCustomerHistoryList  valueForKey:@"RepairerName"],[mCustomerHistoryList  valueForKey:@"RepairerSurname"]];
    cell.mPriceLbl.text = [NSString stringWithFormat:@"€ %.2f",[[mCustomerHistoryList  valueForKey:@"ServicePrice"] floatValue]];
    cell.mPriceLbl.layer.cornerRadius = 6;
    cell.mPriceLbl.layer.masksToBounds = YES;
    [cell.mCustomerImage setImage:[UIImage imageWithData:[mCustomerHistoryList valueForKey:@"imageData"]]];
    cell.mOrderNameLbl.text = [NSString stringWithFormat:@"%@ %@",[mCustomerHistoryList  valueForKey:@"ProductName"],[mCustomerHistoryList  valueForKey:@"ServiceName"]];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        cell.mCustomerImage.layer.cornerRadius = 30;
    }else {
        cell.mCustomerImage.layer.cornerRadius = 45;
    }
    
    cell.mCustomerImage.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.mCustomerImage.layer.borderWidth = 2;
    cell.mCustomerImage.layer.masksToBounds = YES;
    if (count == 10 && self.appObj.historyCount >= 10) {
        self.navigationController.navigationBarHidden = NO;
        //self.mVisualEffectView.hidden = YES;
        [ProgressHUD dismiss];
        self.mTableView.hidden = NO;
    }else {
        self.navigationController.navigationBarHidden = NO;
        //self.mVisualEffectView.hidden = YES;
        [ProgressHUD dismiss];
        self.mTableView.hidden = NO;
    }
    if (count == 6) {
        [ProgressHUD dismiss];
    }
    return cell;
}
-(void)getOrderViewDetailsWithIndex:(NSInteger)index {
    
    RLMResults<CustomerAccount*>* mCustomerAccount = [CustomerAccount allObjects];
    _customerAccount = [mCustomerAccount firstObject];
    RLMResults<CustomerHistoryList*>* mCustomerHistoryList = [CustomerHistoryList allObjects];
    _mCustomerHistoryListArray = (NSMutableArray*)mCustomerHistoryList;
    
    RLMResults<UserAccount*>* mUserAccount = [UserAccount allObjects];
    _userAccount = [mUserAccount firstObject];
    AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    appObj.repOrderId = [[_mCustomerHistoryListArray objectAtIndex:appObj.selectedOrderIndexForDetails] valueForKey:@"OrderId"];
    
    GetOrderViewRequest * mGetOrderViewRequest = [[GetOrderViewRequest alloc] initWithApiKey:_customerAccount.ApiKey withOrderId:appObj.repOrderId];
    
    [mGetOrderViewRequest postWithSuccess:^(NSDictionary *response) {
        NSLog(@"mGetOrderViewRequest Respons = %@",response);
        
        NSError * error;
        NSData* data = [(NSString *)response dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        NSDictionary * orderDetailsDictionary = [dictionary objectForKey:@"message"];
        appObj.DictionaryForOrderDetails = [orderDetailsDictionary valueForKey:@"order_details"];
        appObj.repLat = [[[orderDetailsDictionary valueForKey:@"address_coordinates"] valueForKey:@"lat"] doubleValue];
        appObj.repLong = [[[orderDetailsDictionary valueForKey:@"address_coordinates"] valueForKey:@"lng"] doubleValue];
        appObj.repName = [NSString stringWithFormat:@"%@ %@",[[orderDetailsDictionary valueForKey:@"repairer_data"] valueForKey:@"name"],[[orderDetailsDictionary  valueForKey:@"repairer_data"] valueForKey:@"surname"]];
        appObj.repImage = [UIImage imageWithData:
                           [NSData dataWithContentsOfURL:
                            [NSURL URLWithString:[[orderDetailsDictionary valueForKey:@"repairer_data"] valueForKey:@"image"]]]];

         [self performSegueWithIdentifier:@"orderView" sender:nil];
    } failure:^(NSError *error, NSInteger responseCode) {
        NSLog(@"mGetOrderViewRequest ERROR = %@",error.description);
    }];
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate * appObj = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appObj.selectedOrderIndexForDetails = (NSInteger)indexPath.row;
    if(_userAccount.role == 20){
        [ProgressHUD show:@"Please wait..."];
        //self.mVisualEffectView.hidden = NO;
        [self getOrderViewDetailsWithIndex:(NSInteger)indexPath.row];
    }
    
    
//    if ([self tableView:tableView canCollapseSection:indexPath.section])
//    {
//        if (!indexPath.row)
//        {
//            // only first row toggles exapand/collapse
//            [tableView deselectRowAtIndexPath:indexPath animated:YES];
//            
//            NSInteger section = indexPath.section;
//            BOOL currentlyExpanded = [expandedSections containsIndex:section];
//            NSInteger rows;
//            
//            NSMutableArray *tmpArray = [NSMutableArray array];
//            
//            if (currentlyExpanded)
//            {
//                rows = [self tableView:tableView numberOfRowsInSection:section];
//                [expandedSections removeIndex:section];
//                
//            }
//            else
//            {
//                [expandedSections addIndex:section];
//                rows = [self tableView:tableView numberOfRowsInSection:section];
//            }
//            
//            for (int i=1; i<rows; i++)
//            {
//                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
//                                                               inSection:section];
//                [tmpArray addObject:tmpIndexPath];
//            }
//            
//            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//            
//            if (currentlyExpanded)
//            {
//                [tableView deleteRowsAtIndexPaths:tmpArray
//                                 withRowAnimation:UITableViewRowAnimationTop];
//                
//                cell.accessoryView = [ALCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:ALCustomColoredAccessoryTypeDown];
//                
//            }
//            else
//            {
//                [tableView insertRowsAtIndexPaths:tmpArray
//                                 withRowAnimation:UITableViewRowAnimationTop];
//                cell.accessoryView =  [ALCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:ALCustomColoredAccessoryTypeUp];
//                
//            }
//        }
//        else {
//            NSLog(@"Selected Section is %ld and subrow is %ld ",(long)indexPath.section ,(long)indexPath.row);
//            
//        }
//        
//    }
//    else{
//        //        DetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
//        //        [self.navigationController pushViewController:controller animated:YES];
//        
//    }
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


//=================================
- (IBAction)backButtonPress:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
   // [self goToCustomerPage];
}

-(void)goToCustomerPage {
    // Override point for customization after application launch.
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainIdentifire"];
    mainViewController.rootViewController = navigationController;
    [mainViewController setupWithPresentationStyle:LGSideMenuPresentationStyleSlideAbove type:3];
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    
    window.rootViewController = mainViewController;
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
