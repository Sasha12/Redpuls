//
//  RepairerViewController.h
//  LGSideMenuControllerDemo
//
//  Created by MacMini on 9/6/16.
//  Copyright © 2016 Grigory Lutkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>



@interface RepairerViewController : UIViewController <UITableViewDelegate,UITableViewDataSource> {
    MKPointAnnotation *selectedPlaceAnnotation;
     NSMutableIndexSet *expandedSections;
}


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mActivityView;
@property (weak, nonatomic) IBOutlet MKMapView *mMapView;
@property (weak, nonatomic) IBOutlet UITableView *mTableView;
@property (weak, nonatomic) IBOutlet UIView *mButtomView;
@property (weak, nonatomic) IBOutlet UILabel *mCustomerNameLbl;
@property (weak, nonatomic) IBOutlet UIButton *mPhoneNumberBtn;
@property (weak, nonatomic) IBOutlet UIButton *mDoneBtn;
@property (weak, nonatomic) IBOutlet UILabel *mNotOrderLbl;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *mVisualView;

- (IBAction)CallAction:(id)sender;
- (IBAction)doneAction:(id)sender;



@end
