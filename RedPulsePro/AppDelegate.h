//
//  AppDelegate.h
//  LGSideMenuControllerDemo
//
//  Created by Grigory Lutkov on 28.07.15.
//  Copyright (c) 2015 Grigory Lutkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <UserNotifications/UserNotifications.h>

extern NSString * const ShowNotification;
extern NSString * const ShowAcceptNotification;
extern NSString * const ShowDoneNotification;


@interface AppDelegate : UIResponder <UIApplicationDelegate,AVAudioPlayerDelegate,UNUserNotificationCenterDelegate> {
    AVAudioPlayer *audioPlayer;
}

@property (strong, nonatomic) UIWindow *window;
@property (assign, nonatomic) BOOL isBackFromMap;

@property (assign, nonatomic) NSInteger selectedRow;
@property (strong, nonatomic) NSArray * interventionsList;
@property (strong, nonatomic) NSString * firstName;
@property (strong, nonatomic) NSString * lastName;
@property (strong, nonatomic) NSString * userImagePath;

@property (strong, nonatomic) NSArray * serviceList;
@property (strong, nonatomic) NSString * userRol;

@property (assign, nonatomic) NSInteger selectedServiceIndex;
@property (assign, nonatomic) NSInteger selectedBrandIndex;


@property (strong, nonatomic) NSString * gDeviceToken;

@property (strong, nonatomic) NSString * BASE_URL;
@property (assign, nonatomic) BOOL gIsLogined;
@property (assign, nonatomic) NSInteger selectedOrderIndex;

@property (strong, nonatomic) NSArray * notifierOrderId;
@property (assign, nonatomic) NSInteger selectedOrderID;
@property (strong, nonatomic) NSString * selectedServiceName;
@property (strong, nonatomic) NSString * selectedItemId;
@property (strong, nonatomic) NSString * selectedItemType;
@property (strong, nonatomic) NSString * problemTextField;

@property () NSInteger historyCount;

@property (strong, nonatomic) NSTimer* myTimer;;
@property NSString * alertMessage;

@property() NSDictionary * AcceptNotData;

@property() NSString * customerNameFromNot;
@property() NSString * customerPhoneNumberFromNot;

@property ()  UIAlertView *alertView;
@property (strong) AVPlayer* mPlayer;
@property()NSString* selectedPlace;
@property()NSInteger selectedIndex;
@property() NSString* alertText;

@property() NSMutableDictionary* DictionaryForOrderDetails;
@property() double repLat;
@property() double repLong;
@property() NSString* repName;
@property() UIImage* repImage;
@property() NSString* repOrderId;

@property() NSInteger selectedCardCellIndex;
@property() NSInteger selectedOrderIndexForDetails;

@property() BOOL isaddCardPage;
-(void)playFile:(NSString *)file volume:(float)avol;
-(void)setVolume:(float)vol;
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag;
@end
