//
//  OrderListTableViewCell.h
//  RedPulsePro
//
//  Created by MacMini on 10/18/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mItemNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *mPriceLbl;
@property (weak, nonatomic) IBOutlet UIImageView *mChackbox;

@end
