//
//  CustomDeviceTypeCollectionCell.h
//  RedPulsePro
//
//  Created by MacMini on 10/17/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomDeviceTypeCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mLineImageView;
@property (weak, nonatomic) IBOutlet UIImageView *mCircleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *mAjGic;

@property (weak, nonatomic) IBOutlet UILabel *mDeviceName;
@end
