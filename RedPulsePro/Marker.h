//
//  Marker.h
//  RedPulsePro
//
//  Created by MacMini on 12/16/16.
//  Copyright © 2016 MacMini. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

@interface Marker : GMSMarker

- (void) setPosition : (id) posValue;
@end
