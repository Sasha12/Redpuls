//
//  DropDown.h
//  WiFIScan
//
//  Created by Armen on 6/20/14.
//  Copyright (c) 2014 Armen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@class DropDownView;

@protocol NIDropDownDelegate

- (void) dropDownDelegateMethod: (DropDownView *) sender;

@end

@interface DropDownView : UIView <UITableViewDelegate,UITableViewDataSource>
{
    NSString *animationDirection;
    UIImageView *imgView;
}

@property (nonatomic, weak) id <NIDropDownDelegate> delegate;
@property (nonatomic, strong) NSString *animationDirection;
-(void)reloadData:(NSMutableArray*)data;
-(void)hideDropDown:(UIButton *)b;
- (id)showDropDown:(UIButton *)b
            height:(CGFloat *)height
             array:(NSArray *)arr
         direction:(NSString *)direction;

@end
